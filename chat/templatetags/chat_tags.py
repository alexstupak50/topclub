# -*- coding: utf-8 -*-
from django import template
from django.conf import settings

from core.helpers import site_settings

from chat.helpers import r

register = template.Library()


@register.inclusion_tag('chat/head.html', takes_context=True)
def chat_head(context):
    return dict()

@register.inclusion_tag('chat/chat.html', takes_context=True)
def chat(context):
    history_list = list()
    user = context['request'].user

    if user.is_authenticated():
        for line in r.lrange('chat_history', 0, 10):
            line = line.decode('utf-8', 'replace')
            name, time, message, avatar = line.split(u'<::>')
            hour, minute, second = time.split(u':')
            hour = int(hour) + settings.TIME_ZONE_DELTA
            if hour > 23:
                hour = '%02d' % (hour - 24)

            try:
                if name == user.get_short_name():
                    name = u'Я'
            except AttributeError:
                pass
            history_list.append((
                name, '%02d:%02d:%02d' % (int(hour), int(minute), int(second)),
                message, avatar))
        history_list = reversed(history_list)

    return {
        'history': history_list,
        'user': user,
        'tz_delta': settings.TIME_ZONE_DELTA,
        'max_length': site_settings.CHAT_MAX_LENGTH
    }
