# -*- coding: utf-8 -*-
import tornado.web
import tornado.ioloop
from sockjs.tornado.router import SockJSRouter
from django.core.management.base import NoArgsCommand,LabelCommand
from chat.tornado_server import Connection


class Command(NoArgsCommand):
    def handle_noargs(self, **options):
        router = SockJSRouter(Connection, '/chat')
        app = tornado.web.Application(router.urls)
        app.listen(8989)
        tornado.ioloop.IOLoop.instance().start()


# class Command(LabelCommand):
#     def handle_label(self,*label, **options):
#         router = SockJSRouter(Connection, '/chat')
#         app = tornado.web.Application(router.urls)
#         app.listen(label[0])
#         tornado.ioloop.IOLoop.instance().start()
