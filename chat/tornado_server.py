# -*- coding: utf-8 -*-

import tornado.gen
import tornadoredis

import redis
import json as simplejson
import django
from sockjs.tornado import SockJSConnection

from django.utils.importlib import import_module
from django.conf import settings
from django.utils import timezone

from chat.helpers import REDIS_HOST, REDIS_PORT, REDIS_PASSWORD, REDIS_DB, r


_engine = import_module(settings.SESSION_ENGINE)


def get_session(session_key):
    return _engine.SessionStore(session_key)


def get_user(session):
    class Dummy(object):
        pass

    django_request = Dummy()
    django_request.session = session

    return django.contrib.auth.get_user(django_request)

unjson = simplejson.loads
json = simplejson.dumps


class Connection(SockJSConnection):
    def __init__(self, *args, **kwargs):
        super(Connection, self).__init__(*args, **kwargs)
        self.redis_client = None
        self.listen_redis()
        self.is_jabber_client_start = False


    @tornado.gen.engine
    def listen_redis(self):
        self.redis_client = tornadoredis.Client(
            host=REDIS_HOST,
            port=REDIS_PORT,
            password=REDIS_PASSWORD,
            selected_db=REDIS_DB
        )
        self.redis_client.connect()
        self.redis = r
        self.redis_publish = self.redis.publish

        yield tornado.gen.Task(self.redis_client.subscribe, ['chat'])
        self.redis_client.listen(self.on_redis_queue)

    def send(self, *args, **kwargs):
        return super(Connection, self).send({
            'type': kwargs['msg_type'],
            'data': kwargs['message'],
        })

    def on_open(self, info):
        self.django_session = get_session(info.get_cookie('sessionid').value)
        self.sid = info.get_cookie('sessionid').value
        self.user = get_user(self.django_session)

    def on_message(self, msg):
        msg_type, data = msg.split(',', 1)
        return getattr(self, 'on_%s' % msg_type)(data)

    def on_send(self, message):
        self.redis_publish('chat', json({
            'message': message,
            'username': self.user.get_short_name(),
            'avatar': self.user.get_chat_avatar()
        }))
        self.redis.lpush('chat_history', u'%s<::>%s<::>%s<::>%s' % (
            self.user.get_short_name(), str(timezone.now().time())[:-7],
            message, self.user.get_chat_avatar()))

    def on_redis_queue(self, message):
        if message.kind == 'message':
            message_body = unjson(message.body)
            self.on_chat_receive(message_body)

    def on_chat_receive(self, message):
        if message.get('username', '') != self.user.get_short_name():
            self.send(msg_type='send', message=message)

    def on_close(self):
        self.redis_client.unsubscribe(['chat'])
        self.redis_client.disconnect()
