# -*- encoding: utf-8 -*-

from django.utils import timezone
from celery import shared_task

from core.helpers import site_settings
from chat.helpers import r


@shared_task
def clean_chat():
    last_clean = r.get('last_clean')

    if not last_clean:
        r.delete('chat_history')
        r.set('last_clean', timezone.now().strftime('%d-%m-%Y %H:%M:%S'))
        return

    x = timezone.datetime.strptime(last_clean, '%d-%m-%Y %H:%M:%S')
    last_clean = timezone.datetime(
        x.year, x.month, x.day, x.hour, x.minute, x.second, tzinfo=timezone.utc)
    second = (timezone.now() - last_clean).total_seconds()
    if second >= 60 * site_settings.CHAT_DELETE_DELTA:
        r.delete('chat_history')
        r.set('last_clean', timezone.now().strftime('%d-%m-%Y %H:%M:%S'))
        return
