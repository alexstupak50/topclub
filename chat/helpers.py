# -*- encoding: utf-8 -*-

import redis
from django.conf import settings

REDIS_HOST = getattr(settings, 'CHAT_REDIS_HOST', 'localhost')
REDIS_PORT = getattr(settings, 'CHAT_REDIS_PORT', 6379)
REDIS_PASSWORD = getattr(settings, 'CHAT_REDIS_PASSWORD', None)
REDIS_DB = getattr(settings, 'CHAT_REDIS_DB', '12')

r = redis.StrictRedis(
    host=REDIS_HOST,
    port=REDIS_PORT,
    db=REDIS_DB,
    password=REDIS_PASSWORD
)