#-*- coding:utf-8 -*-
"""
Django settings for minibaraholka project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
from django.utils.translation import ugettext_lazy as _
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '!n*t=1oc53t9b%=kjb6!2qg#lpdn9w3*eve1^@586s#+on+l1v'
SITE_ID = 1


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = ['*']


# Application definition
TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.request",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    "social.apps.django_app.context_processors.backends",
    "social.apps.django_app.context_processors.login_redirect",
    "registration.context_processors.social",
    "core.context_processors.site_settings",
    "core.context_processors.appname",
    "core.context_processors.test_server",
    "catalog.context_processors.count_advert",
    "event.context_processors.count_event",
)

INSTALLED_APPS = (
    # django apps
    'grappelli',
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.sitemaps',
    # 'django.contrib.gis',

    # our apps
    'core',
    'registration',
    'cabinet',
    'catalog',
    'comments',
    'search',
    'chat',
    'event',

    # batteries
    # 'haystack',
    'smart_selects',
    'djcelery',
    'djapian',
    'pagination',
    'south',
    'sorl.thumbnail',
    'captcha',
    'ckeditor',
    'solo',
    'django_geoip',
    'pymorphy',
    'social.apps.django_app.default',
    'widget_tweaks',
    'clear_cache',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django_geoip.middleware.LocationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'pagination.middleware.PaginationMiddleware',
    'core.middleware.TimezoneMiddleware',
    # 'django_geoip.middleware.LocationMiddleware',
)

ROOT_URLCONF = 'infoportal.urls'

WSGI_APPLICATION = 'infoportal.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
#     }
# }

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'infoportal',
        'USER': 'root',
        'PASSWORD': 'portal',
        'HOST': '',
        'PORT': '',
        'TEST_MIRROR': 'default',
        'OPTIONS': {'init_command': 'SET storage_engine=INNODB;'}

    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'ru-Ru'
LANGUAGES = (
    ('ru', _(u'Русский')),
)
TIME_ZONE = 'Europe/Minsk'
TIME_ZONE_DELTA = 3
USE_I18N = True
USE_L10N = True
USE_TZ = True


# Celery
BROKER_URL = 'redis://localhost:6379/10'

import djcelery
from celery.schedules import crontab
djcelery.setup_loader()

from datetime import timedelta

CELERYBEAT_SCHEDULE = {
    "runs-clean_chat": {
        "task": "chat.tasks.clean_chat",
        "schedule": timedelta(seconds=60),
    },
    "runs-clean_events": {
        "task": "event.tasks.clean_events",
        "schedule": crontab(minute=10, hour=0),
    },
    "runs-clean_messages": {
        "task": "cabinet.tasks.clean_messages",
        "schedule": crontab(minute=20, hour=0),
    },
    'every-day-clean-event-and-advert': {
        'task': 'core.tasks.clean_blank',
        'schedule': crontab(minute=0, hour=0),
    },
}


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'collected_static')

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, "templates"),
)

# User model
AUTH_USER_MODEL = 'cabinet.UserProfile'
AUTHENTICATION_BACKENDS = (
    'social.backends.vk.VKOAuth2',
    'social.backends.facebook.FacebookOAuth2',
    'registration.backends.backends.Username_and_EmailAuthBackend',
)
LOGIN_REDIRECT_URL = '/'
LOGIN_URL = '/'
LOGOUT_URL = '/logout/'
SOCIAL_AUTH_VK_OAUTH2_KEY = '4593677'
SOCIAL_AUTH_VK_OAUTH2_SECRET = 'xUlqZhNlmURn7v2oSiez'
SOCIAL_AUTH_STRATEGY = 'social.strategies.django_strategy.DjangoStrategy'
SOCIAL_AUTH_STORAGE = 'social.apps.django_app.default.models.DjangoStorage'
SOCIAL_AUTH_PIPELINE = (
    'social.pipeline.social_auth.social_details',
    'social.pipeline.social_auth.social_uid',
    'social.pipeline.social_auth.auth_allowed',

    'social.pipeline.social_auth.social_user',
    'social.pipeline.social_auth.associate_by_email',
    'social.pipeline.partial.save_status_to_session',
    'registration.pipeline.require_username_and_email',
    'social.pipeline.user.create_user',
    'registration.pipeline.save_profile_picture',
    'social.pipeline.social_auth.associate_user',
    'social.pipeline.social_auth.load_extra_data',
    'social.pipeline.user.user_details',
)

EXTRA_DATA = ['photo_200', '']
SOCIAL_AUTH_PROVIDERS = [
    {'id': p[0], 'name': p[1], 'position': {'width': p[2][0], 'height': p[2][1],}}
    for p in (
        ('vk-oauth2', u'Авторизироваться через ВКонтакте', (0, 0)),
    )
]

UPPER_MAX = 3

#facebook settings
SOCIAL_AUTH_FACEBOOK_KEY = os.environ.get('SOCIAL_AUTH_FACEBOOK_KEY', '856583157746831')
SOCIAL_AUTH_FACEBOOK_SECRET = os.environ.get('SOCIAL_AUTH_FACEBOOK_SECRET', '5873e6b2e2047917c942658eb454ad8d')
SOCIAL_AUTH_FACEBOOK_SCOPE = ['email']
SOCIAL_AUTH_FACEBOOK_PROFILE_EXTRA_PARAMS = {'locale': 'ru_RU'}
SOCIAL_AUTH_NEW_USER_REDIRECT_URL = '/registration/social/'
# Grappelli settings
GRAPPELLI_ADMIN_TITLE = u"Администрирование - Infoportal"

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'django.contrib.staticfiles.finders.FileSystemFinder',
)

# ckeditor settings
CKEDITOR_UPLOAD_PATH = "uploads/"

# mail
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'linfoportal@gmail.com'
EMAIL_HOST_PASSWORD = 'lpdn9w31oc53t9b'
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = 'linfoportal@gmail.com'
# DEFAULT_EMAIL_FROM = 'info@mail.topclub.by'


# cache
CACHES = {
    # 'default': {
    #     'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
    #     'LOCATION': '127.0.0.1:11211',
    # },
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    },
}
# session caching, more info at https://docs.djangoproject.com/en/1.6/topics/http/sessions/#using-cached-sessions
SESSION_ENGINE = 'django.contrib.sessions.backends.cached_db'
CABINET_MENU_CACHE_KEY = 'cabinet_menu_{0}'
MAIN_MENU_CACHE_KEY = 'main_menu'
GEO_MENU_CACHE_KEY = 'geo_menu'
FILTER_CACHE_KEY = 'price_delta_{0}'
PAGINATOR_COUNT_KEY = 'paginator_count_{0}'
COURSE_CACHE_KEY = 'course_data_{0}'
LATEST_CACHE_KEY = 'adv_latest'

# (1, u'BYR')
# (2, u'RUB')
# (3, u'у.е')
# (4, u'EUR')
MAIN_CURRENCY = 3

# django-solo
SOLO_CACHE = 'default'
SOLO_CACHE_TIMEOUT = 24*60*60

# sorl
THUMBNAIL_FORMAT = 'PNG'

# image upload
# 0.5MB - 524288
# 2.5MB - 2621440
# 5MB - 5242880
# 10MB - 10485760
# 20MB - 20971520
# 50MB - 5242880
# 100MB 104857600
# 250MB - 214958080
# 500MB - 429916160
UPLOAD_IMAGE_MAX_SIZE = 5242880
UPLOAD_IMAGE_CONTENT_TYPES = ['image', ]
UPLOAD_IMAGE_MAX_COUNT = 8

# yandex & geoip
YANDEX_API_URL = 'http://geocode-maps.yandex.ru/1.x/'

GEOIP_LOCATION_MODEL = 'core.models.LocCity'
IPGEOBASE_ALLOWED_COUNTRIES = ['BY']

GM_LOCAL_FILE_LOCATION = os.path.join(BASE_DIR, 'local/v0.4-csv.zip')
GEO_MAPPING_SOURCE_URL = 'http://citieslist.ru/v0.4-csv.zip'
GEO_MAPPING_ALLOWED_COUNTRIES = ['belarus',]

GEO_MAPPING_CITIES_FILENAME = '_cities.csv'
GEO_MAPPING_REGIONS_FILENAME = '_regions.csv'
GEO_MAPPING_COUNTRIES_FILENAME = '_countries.csv'

GEO_MAPPING_FILE_ENCODING = 'utf-8'
GEO_MAPPING_FIELDS_DELIMITER = ';'

GEO_MAPPING_COUNTRY_FIELDS = ['country_id', 'title_ru', 'title_ua', 'title_be', 'title_en', 'title_es', 'title_pt', 'title_de', 'title_fr', 'title_it', 'title_pl', 'title_ja', 'title_lt', 'title_lv', 'title_cz']
GEO_MAPPING_REGION_FIELDS = ['region_id', 'country_id', 'title_ru', 'title_ua', 'title_be', 'title_en', 'title_es', 'title_pt', 'title_de', 'title_fr', 'title_it', 'title_pl', 'title_ja', 'title_lt', 'title_lv', 'title_cz']
GEO_MAPPING_CITIES_FIELDS = ['city_id', 'country_id', 'important', 'region_id', 'title_ru', 'area_ru', 'region_ru', 'title_ua', 'area_ua', 'region_ua', 'title_be', 'area_be', 'region_be', 'title_en', 'area_en', 'region_en', 'title_es', 'area_es', 'region_es', 'title_pt', 'area_pt', 'region_pt', 'title_de', 'area_de', 'region_de', 'title_fr', 'area_fr', 'region_fr', 'title_it', 'area_it', 'region_it', 'title_pl', 'area_pl', 'region_pl', 'title_ja', 'area_ja', 'region_ja', 'title_lt', 'area_lt', 'region_lt', 'title_lv', 'area_lv', 'region_lv', 'title_cz', 'area_cz', 'region_cz']

GEO_MAPPING_DEFAULT_CITY = 'Минск'

# pymorphy
PYMORPHY_DICTS = {
    'ru': {
        'dir': os.path.join(BASE_DIR, 'infoportal/files'),
        'backend': 'cdb',
        'use_cache': True,
        'default': True,
    },
}

# crypto
from Crypto.Cipher import AES

CRYPTO_SECRET = 'hb8qwtjcr7T9SZ85GMNKw8QerLZqFlTk'
CRYPTO_BLOCK_SIZE = 32
CRYPTO_PADDING_CHAR = '{'

# search
# HAYSTACK_CONNECTIONS = {
#     'default': {
#         'ENGINE': 'xapian_backend.XapianEngine',
#         'PATH': os.path.join(BASE_DIR, 'xapian_index')
#     },
# }

DJAPIAN_DATABASE_PATH = os.path.join(BASE_DIR, 'djapian_spaces')


# logging
DEFAULT_LOGGER_NAME = 'catalog'
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s \n'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'file': {
            # 'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename':  os.path.join(BASE_DIR, 'logs/django.log'),
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'catalog': {
            'handlers': ['file'],
            'propagate': True,
            'level': 'DEBUG',
        },
        'django': {
            'handlers': ['file'],
            'propagate': True,
            'level': 'DEBUG',
        },
    }
}

# pwd: 1oc53t9b
# pwd: lpdn9w3
# pwd: lpdn9w31oc53t9b

try:
    from local_settings import *
except ImportError:
    pass
