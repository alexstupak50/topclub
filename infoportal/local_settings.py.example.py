# -*- encoding: utf-8 -*-

import os
import settings

DEBUG = True
TEMPLATE_DEBUG = DEBUG

settings.MIDDLEWARE_CLASSES += (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

settings.INSTALLED_APPS += (
    'debug_toolbar',
)

INTERNAL_IPS = ('127.0.0.1',)

# DEBUG_TOOLBAR_CONFIG = {
#     'INTERCEPT_REDIRECTS': False,
# }

# For testing purposes
DATABASE_SUPPORTS_TRANSACTIONS = True

# # Disable caching on local development environment
# CACHE_BACKEND = 'dummy://'

# sorl
THUMBNAIL_DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(settings.BASE_DIR, 'db.sqlite3'),
    }
}

EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'sandbox.kybervision@gmail.com'
EMAIL_HOST_PASSWORD = 'hortonCVT'
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = 'sandbox.kybervision@gmail.com'


# geoip LOCAL_IP_ALTERNATIVE
LOCAL_IP_ALTERNATIVE = '46.219.81.21'

# ignore some apps in local development
local_ignore = (
)

if local_ignore:
    INSTALLED_APPS = ()
    for item in settings.INSTALLED_APPS:
        if item not in local_ignore:
            INSTALLED_APPS += item,