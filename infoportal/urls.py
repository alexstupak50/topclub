from django.conf.urls import patterns, include, url
from django.conf.urls.static import static

from django.contrib import admin
from django.conf import settings
from event.views import EventListView
# from django_geoip.views import set_location

admin.autodiscover()

urlpatterns = patterns('',
    # home
    url(r'^$', EventListView.as_view(), name='home'),

    # admin
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/', include(admin.site.urls)),

    # registration
    url(r'^', include('registration.urls')),

    # for menu order
    # url(r'^pagesorter/orderedmove_order/(?P<direction>up|down)/(?P<model_type_id>\d+)/(?P<model_id>\d+)/$',
    #      'core.views.admin_move_ordered_model_order',
    #      name="admin_move_order"),

    # other apps
    url(r'^comment/', include('comments.urls', namespace='comments')),
    url(r'^cabinet/', include('cabinet.urls', namespace='cabinet')),
    url(r'^', include('event.urls', 'event')),
    url(r'^ckeditor/', include('ckeditor.urls')),
    # url(r'^article/', include('core.urls', namespace='core')),
    url(r'^captcha/', include('captcha.urls')),
    url(r'', include('social.apps.django_app.urls', namespace='social')),

    # geo
    url(r'^geo/regions/$', 'core.views.get_regions', name='get_regions'),
    url(r'^geo/region-(?P<region_pk>\d+)/$', 'core.views.get_cities', name='get_cities'),
    url(r'^geo/city/autocomplete/$', 'core.views.city_autocomplete', name='city_autocomplete'),
    # url(r'^geoip/', include('django_geoip.urls')),
    # url(r'^geoip/setlocation/', 'core.views.set_location', name='geoip_change_location'),
    url(r'^chaining/', include('smart_selects.urls')),
    url(r'^', include('search.urls')),
    url(r'^', include('catalog.urls', app_name="catalog", namespace='catalog')),
    url(r'^', include('event.urls')),
)

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
