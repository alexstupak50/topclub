from django.db.models import signals

from catalog.signals import delete_cabinet_menu_cache
from event.models import Comment, Like


def update_event_comments_count(sender, **kwargs):
    kwargs.get('instance').event.update_comments_count()
    delete_cabinet_menu_cache(kwargs.get('instance').user_id)


def update_event_likes_count(sender, **kwargs):
    kwargs.get('instance').event.update_likes_count()

signals.post_save.connect(update_event_likes_count, sender=Like)
signals.post_delete.connect(update_event_likes_count, sender=Like)
signals.post_save.connect(update_event_comments_count, sender=Comment)
signals.post_delete.connect(update_event_comments_count, sender=Comment)
