# -*- coding: utf-8 -*-

import json
import urllib2
import hashlib
from django.utils import timezone
from django.shortcuts import redirect, get_object_or_404, render_to_response, \
    RequestContext
from django.core.urlresolvers import reverse_lazy
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.views.generic.base import View
from django.views.generic import ListView, DetailView, FormView, TemplateView
from django.utils.translation import ugettext_lazy as _
from django.core.servers.basehttp import FileWrapper
from django.http import StreamingHttpResponse
from django.contrib import auth, messages
from django.utils.safestring import mark_safe
from django.utils.encoding import force_text
from django.views.decorators.http import condition
from django.core.exceptions import PermissionDenied
from dateutils import relativedelta
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator, EmptyPage
from django.db.models import Q
from django.conf import settings
from django.template import Context, Template, loader
from sorl.thumbnail import get_thumbnail
from django.template.defaultfilters import filesizeformat

from event.mixins import LoginRequiredMixin
from event.forms import EventAddForm, EventFilterForm, EventEditForm, \
    CommentAddForm, EventUploadForm, EventUploadUrlForm
from event.models import Event, EventCategory, Comment
from .forms import EventImageForm
from django.core.files import File
import os
import urllib


class EventListView(ListView):
    model = Event
    template_name = 'event/list.html'
    context_object_name = "events"
    form_class = EventFilterForm

    def get_queryset(self):
        filters = self.request.GET.copy()
        pagination = filters.get('pangination', 20)

        if not filters:
            self.queryset = self.model.objects.get_actual().order_by('-datetime')
        else:
            print(filters)
            self.queryset = self.filter(filters)

        try:
            page = int(self.request.GET.get('page', 1))
            self.queryset = Paginator(self.queryset, pagination).page(page)
        except ValueError:
            self.queryset = Paginator(self.queryset, pagination).page(1)
        except EmptyPage:
            pass

        return self.queryset

    def get_context_data(self, **kwargs):
        ctx = super(EventListView, self).get_context_data(**kwargs)
        ctx['form'] = self.form_class(self.request.GET)
        return ctx

    def filter(self, filters):
        q = Q()
        func_filters = ['category', 'date', 'query', 'addresses']
        for name in func_filters:
            q = getattr(self, '_filter_%s' % name)(filters, q)
        queryset = self.model.objects.get_actual()
        return queryset.filter(q).order_by('-created')

    def _filter_query(self, filters, q):
        if filters.get('query'):
            q = q & (Q(title__icontains=filters['query']) |
                     Q(keywords__icontains=filters['query']))
        return q

    def _filter_category(self, filters, q):
        if filters.get('category'):
            q = q & Q(category__id=filters['category'])
        return q

    def _filter_date(self, filters, q):
        try:
            date_start = timezone.datetime.strptime(filters.get('date_start'),
                                                    '/%m/%d/%Y') if \
                filters.get('date_start') else None
        except ValueError:
            date_start = None

        try:
            date_end = timezone.datetime.strptime(filters.get('date_end'),
                                                  '/%m/%d/%Y') if \
                filters.get('date_end') else None
        except ValueError:
            date_end = None

        if date_start and date_end:
            return q & Q(datetime__range=(date_start, date_end))

        if date_start:
            return q & Q(datetime__gte=date_start)

        if date_end:
            return q & Q(datetime__lte=date_end)

        return q

    def _filter_addresses(self, filters, q):
        _q = Q()
        if filters.get('region'):
            _q = _q & Q(region__id=int(filters.get('region')))

        if filters.get('city'):
            _q = _q & Q(city__id=int(filters.get('city')))

        return _q & q


class EventCreateView(FormView):
    template_name = 'event/create.html'
    form_class = EventAddForm

    def get_obj(self):
        data = dict()
        if self.request.user.is_authenticated():
            data['user'] = self.request.user
        else:
            data['session_key'] = self.request.session.session_key

        if self.request.session.get('event_id'):
            try:
                return Event.base_manager.get(
                    id=self.request.session['event_id'], **data)
            except Event.DoesNotExist:
                pass

        obj = Event.objects.create(**data)
        self.request.session['event_id'] = obj.id
        self.request.session.save()
        return obj

    def get_context_data(self, **kwargs):
        ctx = super(EventCreateView, self).get_context_data(**kwargs)
        ctx['obj'] = obj = self.get_obj()
        ctx['images'] = obj.eventimage_set.all()
        ctx['img_form'] = EventImageForm(advert=self.get_obj())
        return ctx

    def get_form_kwargs(self):
        kwargs = super(EventCreateView, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        kwargs['files'] = self.request.FILES or None
        kwargs['instance'] = self.get_obj()
        if self.request.user.is_authenticated():
            kwargs['initial'] = {
                'name': self.request.user.first_name or '',
                'email': self.request.user.email,
                'phone': self.request.user.phone or ''
            }
        return kwargs

    def form_valid(self, form):
        obj = form.save()
        del self.request.session['event_id']
        self.request.session.save()
        if not self.request.user.is_authenticated():
            obj.session_key = self.request.session.session_key
            obj.edit_key = hashlib.sha256(obj.session_key).hexdigest()
            obj.save()
        else:
            if obj.phone and not self.request.user.phone:
                self.request.user.phone = obj.phone
                self.request.user.save()
        if self.request.user.is_authenticated():
            messages.success(self.request, _(u'Ивент успешно создан'))
        else:
            ar = (self.request.META['HTTP_HOST'],
                  reverse_lazy('event:edit_for_key', kwargs={
                      'pk': obj.pk, 'key': obj.edit_key}))
            messages.success(
                self.request,
                mark_safe(u'Ивент успешно создан<br>Ссылка для редактирования:'
                          u'<br><input id="edit-link" type="text" size="20 " '
                          u'value="http://%s%s" onClick="this.select();">' % ar)
            )

        return HttpResponseRedirect(reverse_lazy('event:detail',
                                                 kwargs={'pk': obj.pk}))


class EventUploadView(FormView):
    form_class = EventUploadForm

    def get_form_kwargs(self):
        kwargs = super(EventUploadView, self).get_form_kwargs()
        data = dict()
        if self.request.user.is_authenticated():
            data['user'] = self.request.user
        else:
            data['session_key'] = self.request.session.session_key

        try:
            kwargs['instance'] = Event.base_manager.get(
                id=self.kwargs.get('pk'), **data)
        except Event.DoesNotExist:
            raise Http404
        kwargs['field_name'] = self.kwargs.get('field')
        return kwargs

    def form_invalid(self, form):
        data = {
            'error': form[self.kwargs.get('field')].errors[0]
        }
        return HttpResponse(json.dumps(data), content_type='application/json')

    def form_valid(self, form):
        obj = form.save()
        data = {'success': True}

        if self.kwargs.get('field') == 'photo':
            thumbnail = get_thumbnail(
                obj.photo, '75x75', crop='center', quality=99)
            data['im_url'] = obj.photo.url
            data['thumbnail_url'] = thumbnail.url

        if self.kwargs.get('field') == 'audio':
            data['url'] = obj.audio.url
            data['name'] = obj.get_audio_name()
            data['size'] = obj.get_audio_size()

        return HttpResponse(json.dumps(data), content_type='application/json')


class EventUploadDeleteView(View):

    def get(self, request, *args, **kwargs):
        data = dict()
        if self.request.user.is_authenticated():
            data['user'] = self.request.user
        else:
            data['session_key'] = self.request.session.session_key

        try:
            obj = Event.base_manager.get(id=self.kwargs.get('pk'), **data)
        except Event.DoesNotExist:
            raise Http404
        setattr(obj, self.kwargs.get('field'), None)
        obj.save()
        return HttpResponse('ok', 'text/plain')


def EventUploadUrlView(request,pk=None, field=''):
    if request.method == 'POST' and request.is_ajax():
        form = EventUploadUrlForm(request_POST=request.POST,field=field)
        if form.is_valid():
            cleaned_data = form.cleaned_data
            data = {'success': True}
            event_instance = Event.base_manager.get(id=pk)

            result = urllib.urlretrieve(cleaned_data[field]) 
            getattr(event_instance,field).save( os.path.basename(cleaned_data[field]),
                                                File(open(result[0])) )
            event_instance.save()

            thumbnail = get_thumbnail(getattr(event_instance,field), 
                                      '75x75', crop='center', quality=99)
            data['im_url'] = getattr(event_instance,field).url
            data['thumbnail_url'] = thumbnail.url

            return HttpResponse(json.dumps(data), content_type='application/json')

        else:
            data = {'error': form[field].errors[0]}
            return HttpResponse(json.dumps(data), content_type='application/json')

    else:
        form = EventUploadUrlForm(field=field)


class EventDetailView(DetailView):
    model = Event
    template_name = 'event/detail.html'
    context_object_name = 'event'

    def get_context_data(self, **kwargs):
        ctx = super(EventDetailView, self).get_context_data(**kwargs)
        ctx['comment_form'] = CommentAddForm()
        ctx['is_cat_edit'] = False
        ctx['is_can_up'] = False
        ctx['is_can_see_key'] = False
        if self.request.user.is_authenticated() and \
                self.request.user == ctx['event'].user:
            ctx['is_cat_edit'] = True
            ctx['is_can_up'] = True

        if self.request.session.session_key == ctx['event'].session_key:
            ctx['is_can_see_key'] = True

        return ctx


class EventEditView(FormView):
    template_name = 'event/edit.html'
    form_class = EventEditForm

    def get_object(self):
        obj = get_object_or_404(Event, pk=self.kwargs.get('pk'))
        if obj.user == self.request.user:
            return obj
        if self.kwargs.get('key') and self.kwargs.get('key') == obj.edit_key:
            return obj

        raise Http404

    def get_context_data(self, **kwargs):
        ctx = super(EventEditView, self).get_context_data(**kwargs)
        ctx['obj'] = obj = self.get_object()
        ctx['images'] = obj.eventimage_set.all()
        ctx['img_form'] = EventImageForm(advert=obj)
        return ctx

    def get_form_kwargs(self):
        kwargs = super(EventEditView, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        kwargs['files'] = self.request.FILES or None
        kwargs['instance'] = self.get_object()
        return kwargs

    def form_valid(self, form):
        obj = form.save()
        if not self.request.user.is_authenticated():
            obj.session_key = self.request.session.session_key
            obj.save()
        else:
            if obj.phone and not self.request.user.phone:
                self.request.user.phone = obj.phone
            if obj.name and not self.request.user.first_name:
                self.request.user.first_name = obj.name

            self.request.user.save()

        messages.success(self.request, _(u'Ивент успешно изменен'))
        return HttpResponseRedirect(reverse_lazy('event:detail',
                                                 kwargs={'pk': obj.pk}))


class EventUpView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        event = get_object_or_404(Event, pk=kwargs.get('pk'),
                                  user=self.request.user)
        if event.upper_count >= settings.UPPER_MAX:
            messages.error(self.request, _(
                u'Вы уже испотльзовали все поднятия для этого иветнта'))
        else:
            event.ordering = timezone.now()
            event.upper_count += 1
            event.save()
            messages.success(self.request, _(u'Иветн поднят в списке'))
        return HttpResponseRedirect(reverse_lazy('event:detail',
                                                 kwargs={'pk': event.pk}))


class CommitAddView(LoginRequiredMixin, FormView):
    form_class = CommentAddForm
    template_name = 'event/comment_add.html'

    def get_form_kwargs(self):
        kwargs = super(CommitAddView, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        kwargs['event'] = get_object_or_404(Event, pk=self.kwargs.get('pk'))
        if self.request.GET.get('parent'):
            try:
                kwargs['parent'] = Comment.objects.get(
                    event=kwargs['event'], pk=self.request.GET.get('parent'))
            except (Comment.DoesNotExist, ValueError):
                kwargs['parent'] = None

        return kwargs

    def form_valid(self, form):
        comment = form.save()
        if comment.parent:
            template = 'event/comment_child.html'
            ctx = {
                'event': comment.event,
                'comment': comment.parent,
                'children': comment,
                'request': self.request
            }
        else:
            template = 'event/comments.html'
            ctx = {
                'event': comment.event,
                'comments': [comment],
                'request': self.request
            }

        data = {
            'valid': True,
            'html': loader.render_to_string(template, ctx)
        }
        return HttpResponse(json.dumps(data), content_type="application/json")


class CommentPageView(LoginRequiredMixin, View):
    template_name = 'event/comments.html'

    def get(self, *args, **kwargs):
        event = get_object_or_404(Event, pk=kwargs.get('pk'))
        try:
            page = int(self.request.GET.get('page'))
        except (ValueError, KeyError):
            raise Http404

        data = {
            'event': event,
            'comments': event.get_comments_by_page(page),
            'page': page,
            'next_page': page + 1
        }

        return render_to_response(self.template_name, data,
                                  context_instance=RequestContext(self.request))


class EventDeleteView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        obj = get_object_or_404(Event, pk=self.kwargs.get('pk'))
        if obj.user == self.request.user:
            obj.delete()
            messages.success(self.request, _(u'Ивент успешно удален'))
        else:
            messages.error(self.request, _(u'У вас нет прав на это действие'))
        return HttpResponseRedirect(self.request.META.get('HTTP_REFERER', '/'))
