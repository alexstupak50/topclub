# -*- coding: utf-8 -*-
from django.core.cache import cache

from .models import Event


def count_event(request):
    cache_key = 'count_event'
    cache_time = 60*60
    c = cache.get(cache_key)
    if c is None:
        c = Event.objects.all().count()
        cache.set(cache_key, c, cache_time)
    return {
        'count_event': c
    }