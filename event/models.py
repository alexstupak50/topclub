# -*- encoding: utf-8 -*-
import math
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.template.defaultfilters import filesizeformat
from django.utils import timezone
from dateutils import relativedelta
from core.helpers import site_settings
from django.core.urlresolvers import reverse
from sorl.thumbnail import get_thumbnail

from event.fields import ContentTypeRestrictedFileField
from event.mixins import FileKirilicMixin
from core.models import LocCity, LocRegion
from core.utils import mat_validator


class EventCategory(models.Model):
    title = models.CharField(_(u'название'), max_length=255)
    icon_name = models.CharField(
        _(u'иконка'), max_length=255, null=True, blank=True,
        help_text=_(u'название иконок брать тут http://fortawesome.github.io/'
                    u'Font-Awesome/icons/'))
    hidden = models.BooleanField(u'скрыть', default=False,
                                 help_text=u'Будет скрыто, если отмечено')

    class Meta:
        verbose_name = _(u'Группа ивентов')
        verbose_name_plural = _(u'Группы ивентов')

    def __unicode__(self):
        return unicode(self.title)


class EventManager(models.Manager):

    def get_queryset(self):
        return super(EventManager, self).get_queryset()\
            .filter(title__isnull=False)

    def get_archive(self):
        """
            Ивенты в которых истех срок годности + значение с настроек
        """
        x_date = timezone.now() - relativedelta(days=site_settings.EVENT_REG_ON_FEED)
        return self.filter(datetime__lt=x_date)

    def get_actual(self):
        """
            Ивенты которые еще не попали в архив
        """
        x_date = timezone.now() - relativedelta(days=site_settings.EVENT_REG_ON_FEED)
        return self.exclude(datetime__lt=x_date)
        # return self.all()


class Event(FileKirilicMixin, models.Model):
    # file_filed = ['photo', 'audio','photo_from_url_1','photo_from_url_2','photo_from_url_3','photo_from_url_4']

    user = models.ForeignKey(
        'cabinet.UserProfile', verbose_name=_(u'пользователь'),
        null=True, blank=True, related_name='events')
    session_key = models.CharField(
        _(u'Ключ сессии автора'), max_length=80, null=True, blank=True)
    edit_key = models.CharField(
        _(u'Ключ редактирования'), max_length=80, null=True, blank=True)
    title = models.CharField(_(u'заголовок'), max_length=200, null=True)
    category = models.ForeignKey(
        'EventCategory', verbose_name=_(u'категория'), null=True)
    description = models.TextField(_(u'описание'))
    name = models.CharField(_(u'имя'), max_length=60, null=True, blank=True)
    email = models.EmailField(_(u'email'), null=True, blank=True)
    skype = models.CharField(_(u'skype'), max_length=40, null=True, blank=True)
    phone = models.CharField(_(u'телефон'), max_length=20,
                             null=True, blank=True)
    address = models.CharField(_(u'адрес'), max_length=255,
                               null=True, blank=True)
    keywords = models.CharField(
        u'ключевые слова', max_length=254, blank=True, null=True,
        help_text=u'Через запятую.')
    datetime = models.DateTimeField(_(u'начало'), null=True)
    cover = models.ForeignKey(
        'EventImage', verbose_name=u'обложка', related_name='event_covered',
        on_delete=models.SET_NULL, null=True, blank=True)
    audio = ContentTypeRestrictedFileField(
        _(u'аудио файл'), upload_to='events/audio/', null=True, blank=True,
        content_types=('audio/mpeg3', 'audio/x-mpeg-3', 'audio/mp3',
                       'audio/mpeg'), max_length=200)
    created = models.DateTimeField(_(u'создано'), default=timezone.now)
    ordering = models.DateTimeField(_(u'дата сортировки'), default=timezone.now)
    upper_count = models.PositiveIntegerField(
        _(u'количество поднятий'), default=0)

    # координаты
    region = models.ForeignKey(
        LocRegion, verbose_name=u'регион', null=True, blank=True)
    city = models.ForeignKey(
        LocCity, verbose_name=u'город', null=True, blank=True)

    comments_count = models.PositiveIntegerField(
        u'кол-во комментариев', default=0)
    likes_count = models.PositiveIntegerField(
        u'кол-во позитивных лайков', default=0)
    dislike_count = models.PositiveIntegerField(
        u'кол-во негативных лайков', default=0)
    objects_all = models.Manager()
    objects = EventManager()
    base_manager = models.Manager()

    class Meta:
        verbose_name = _(u'Ивент')
        verbose_name_plural = _(u'Ивенты')
        ordering = ('-ordering',)

    def __unicode__(self):
        return unicode(self.title)

    def get_cover_image(self):
        if self.cover and self.cover.image:
            return self.cover.image
        else:
            first = self.eventimage_set.first()
            return first.image if first else None

    def get_comments(self):
        return self.comments.filter(parent__isnull=True)

    def get_event_page_count(self):
        return math.ceil(
            self.comments.filter(parent__isnull=True).count() / 10.)

    def get_comments_first_page(self):
        return self.comments.filter(parent__isnull=True)[:10]

    def get_comments_by_page(self, page):
        return self.comments.filter(
            parent__isnull=True)[(page - 1) * 10:page * 10]

    def get_audio_name(self):
        if self.audio:
            return self.audio.name.split('/')[-1]
        return ''

    def get_audio_size(self):
        if self.audio:
            return filesizeformat(self.audio.size)
        return ''

    @property
    def is_actual(self):
        return True if self.datetime > timezone.now() else False

    def update_comments_count(self):
        self.comments_count = self.comments.count()
        self.save()

    def update_likes_count(self):
        self.likes_count = self.likes.filter(type=1).count()
        self.dislike_count = self.likes.filter(type=2).count()
        self.save()


class EventImage(models.Model):
    image = models.ImageField(u'Изображение', upload_to='events/photo/')
    advert = models.ForeignKey(Event, verbose_name=u'Ивент')

    class Meta:
        verbose_name = u'Изображение'
        verbose_name_plural = u'Изображения'

    def __unicode__(self):
        return u'Изображение'

    def get_delete_url(self):
        return reverse(
            'event:img_control', args=(self.advert_id, self.id, 'delete'))

    def get_cover_url(self):
        return reverse(
            'event:img_control', args=(self.advert_id, self.id, 'cover'))

    def get_thumbnail(self, size='75x75', crop='center', quality=99):
        img = self.image
        return unicode(get_thumbnail(img, size, crop=crop, quality=quality).url)

    def get_thumbnail_html(self):
        html = '<img src="{0}" alt="img"/>'
        return html.format(self.get_thumbnail())


class Comment(models.Model):
    event = models.ForeignKey(
        'Event', verbose_name=_(u'ивент'), related_name='comments')
    parent = models.ForeignKey(
        'self', related_name='children', verbose_name=_(u'предок'),
        null=True, blank=True)
    user = models.ForeignKey(
        'cabinet.UserProfile', verbose_name=_(u'Пользователь'),
        related_name='event_comments')
    message = models.TextField(_(u'Коментарий'), validators=[mat_validator])
    created = models.DateTimeField(_(u'Создано'), default=timezone.now)

    class Meta:
        verbose_name = _(u'Коментарий')
        verbose_name_plural = _(u'Коментарии')
        ordering = ('-created',)

    def __unicode__(self):
        return unicode(self.user)

    def get_children(self):
        return self.children.all().order_by('created')


TYPE_LIKE = (
    (0, None),
    (1, 'like'),
    (2, 'dislike')
)


class Like(models.Model):
    event = models.ForeignKey(
        'Event', verbose_name=_(u'ивент'), related_name='likes')
    user = models.ForeignKey(
        'cabinet.UserProfile', verbose_name=_(u'Пользователь'),
        related_name='event_likes')
    type = models.SmallIntegerField(_('Тип'), choices=TYPE_LIKE)

    class Meta:
        verbose_name = _(u'лайк')
        verbose_name_plural = _(u'лайки')

    def __unicode__(self):
        return u'like'
