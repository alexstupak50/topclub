from django import template
from django.db.models import Count

from event.models import EventCategory
from event.forms import EventFilterForm

register = template.Library()


@register.assignment_tag
def event_category_tag():
    return EventCategory.objects.annotate(count_events=Count('event')).order_by('-count_events')


@register.assignment_tag(takes_context=True)
def event_filter_form(context):
    return EventFilterForm(data=context['request'].GET or None)