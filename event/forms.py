# -*- encoding: utf-8 -*-

import re
from dateutil import tz
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from smart_selects.widgets import ChainedSelect

from event.models import Event, EventCategory, Comment, EventImage
from core.matfilter import matfilter
from core.models import LocCity, LocRegion
from catalog.forms import LocationFormMixin, ImageFormBase


class EventAddForm(LocationFormMixin):
    date = forms.DateField(label=_(u'дата'))
    time = forms.CharField(label=_(u'время'), min_length=5, max_length=5)

    class Meta:
        model = Event
        fields = ('title', 'category', 'description', 'keywords', 'address',
                  'name', 'email', 'skype', 'phone', 'lat', 'lng')

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(EventAddForm, self).__init__(*args, **kwargs)
        self.fields['date'].input_formats = ('%d/%m/%Y',)
        self.fields['address'].widget = forms.HiddenInput()
        self.fields['keywords'].widget.attrs.update(
            placeholder=u'вечеринка, встреча')
        self.fields['phone'].widget.attrs.update(
            placeholder=u'+375 (xx) xxxxxxx')
        self.fields['description'].widget.attrs.update({'id': 'tiny'})

    def clean_title(self):
        value = self.cleaned_data['title']
        if value and matfilter(value):
            raise forms.ValidationError(
                _(u'Пожалуйста удалите бранные слова.'))
        return value

    def clean_description(self):
        value = self.cleaned_data['description']
        if value and matfilter(value):
            raise forms.ValidationError(
                _(u'Пожалуйста удалите бранные слова.'))
        return value

    def clean_time(self):
        value = self.cleaned_data['time']
        r = re.compile('[012][\d]:[0123456][\d]')
        if not r.search(value):
            raise forms.ValidationError(_(u'Неправильный формат времени'))
        return value

    def save(self, commit=True):
        data = self.cleaned_data
        obj = super(EventAddForm, self).save(commit=False)
        if self.user.is_authenticated():
            obj.user = self.user

        hour = int(data['time'][0:2])
        minute = int(data['time'][3:5])
        obj.datetime = timezone.datetime(
            year=data['date'].year, month=data['date'].month,
            day=data['date'].day, hour=hour, minute=minute)
        obj.created = timezone.now()
        obj.ordering = timezone.now()

        if commit:
            obj.save()
        return obj


class EventImageForm(ImageFormBase):
    max_count = 4
    patt_url = 'event:img_add'

    class Meta:
        model = EventImage
        exclude = ('advert',)


class EventUploadForm(forms.ModelForm):

    class Meta:
        model = Event
        fields = ('audio', )

    def __init__(self, *args, **kwargs):
        self.field_name = kwargs.pop('field_name')
        super(EventUploadForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            if field != self.field_name:
                del field


# динамическое создание класса с одним атрибутом-полем, функция возвращает экземпляр этого класса
def EventUploadUrlForm(request_POST=None,field=''):
    EventUploadUrlForm= type('EventUploadUrlForm', (forms.Form,),
                             { field:forms.URLField(max_length=500,
                                                    required=False) }
                            )
    if request_POST:
        return EventUploadUrlForm(request_POST)
    else:
        return EventUploadUrlForm()


class EventFilterForm(forms.Form):
    category = forms.ModelChoiceField(label=_(u'категория'),
                                      queryset=EventCategory.objects.none(),
                                      required=False)
    date_start = forms.DateField(_(u'дата начала'), required=False)
    date_end = forms.DateField(_(u'дата окончания'), required=False)
    region = forms.ModelChoiceField(
        label=_(u'Регион'), queryset=LocRegion.objects.all(), empty_label=u"Регион", required=False)
    city = forms.ModelChoiceField(queryset=LocCity.objects.all(), empty_label=u"Город", required=False,
                                  widget=ChainedSelect(
                                      app_name='core',
                                      model_name='LocCity',
                                      chain_field='region',
                                      model_field='region',
                                      auto_choose=False,
                                      show_all=False,)
                                  )

    def __init__(self, *args, **kwargs):
        super(EventFilterForm, self).__init__(*args, **kwargs)
        self.fields['category'].queryset = EventCategory.objects.all()
        self.fields['category'].empty_label = _(u'Все категории')
        self.fields['date_start'].input_formats = ('%d/%m/%Y',)
        self.fields['date_end'].input_formats = ('%d/%m/%Y',)
        self.fields['region'].widget.attrs['class'] = 'clear'
        self.fields['city'].widget.queryset = LocCity.objects.all()
        if not self.data.get('region') or self.data.get('region') == '':
            self.fields['city'].widget.attrs['disabled'] = 'true'


class EventEditForm(LocationFormMixin):
    date = forms.DateField(label=_(u'дата'))
    time = forms.CharField(label=_(u'время'), min_length=5, max_length=5)

    class Meta:
        model = Event
        fields = ('title', 'category', 'description', 'keywords', 'address',
                  'name', 'email', 'skype', 'phone', 'lat', 'lng')

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(EventEditForm, self).__init__(*args, **kwargs)
        self.fields['date'].input_formats = ('%d/%m/%Y',)
        self.fields['address'].widget = forms.HiddenInput()
        self.fields['date'].initial = self.instance.datetime.strftime(
            '%d/%m/%Y')
        self.fields['time'].initial = self.instance.datetime.strftime(
            '%H:%M')
        self.fields['keywords'].widget.attrs.update(
            placeholder=u'вечеринка, встреча')
        self.fields['phone'].widget.attrs.update(
            placeholder=u'+375 (xx) xxxxxxx')
        self.fields['description'].widget.attrs.update({'id': 'tiny'})

        if not self.user.is_authenticated():
            fields = ('title', 'description', 'address', 'name', 'email',
                      'skype', 'phone', 'lat', 'lng', )
            for field_name in self.fields.keys():
                if field_name not in fields:
                    del self.fields[field_name]

    def clean_title(self):
        value = self.cleaned_data['title']
        if value and matfilter(value):
            raise forms.ValidationError(
                _(u'Пожалуйста удалите бранные слова.'))
        return value

    def clean_description(self):
        value = self.cleaned_data['description']
        if value and matfilter(value):
            raise forms.ValidationError(
                _(u'Пожалуйста удалите бранные слова.'))
        return value

    def clean_time(self):
        value = self.cleaned_data['time']
        r = re.compile('[012][\d]:[0123456][\d]')
        if not r.search(value):
            raise forms.ValidationError(_(u'Неправильный формат времени'))
        return value

    def save(self, commit=True):
        data = self.cleaned_data
        obj = super(EventEditForm, self).save(commit=False)
        if self.user.is_authenticated():
            obj.user = self.user
            hour = int(data['time'][0:2])
            minute = int(data['time'][3:5])
            obj.datetime = timezone.datetime(
                year=data['date'].year, month=data['date'].month,
                day=data['date'].day, hour=hour, minute=minute)

        if commit:
            obj.save()

        return obj


class CommentAddForm(forms.ModelForm):

    class Meta:
        model = Comment
        fields = ('message',)

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        self.event = kwargs.pop('event', None)
        self.parent = kwargs.pop('parent', None)
        super(CommentAddForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        obj = super(CommentAddForm, self).save(commit=False)
        obj.user = self.user
        obj.event = self.event
        obj.parent = self.parent
        if commit:
            obj.save()

        return obj
