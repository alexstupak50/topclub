# -*- encoding: utf-8 -*-

from django.utils import timezone
from celery import shared_task
from dateutils import relativedelta
from core.helpers import site_settings


@shared_task
def clean_events():
    from event.models import Event
    x_date = timezone.now() - relativedelta(
        days=site_settings.EVENT_REG_DELETE_DELTA)
    Event.objects.filter(created__lt=x_date, user__isnull=False).delete()

    x_date = timezone.now() - relativedelta(
        days=site_settings.EVENT_GUEST_DELETE_DELTA)
    Event.objects.filter(created__lt=x_date, user__isnull=True).delete()
    Event.base_manager.filter(created__lt=x_date, title_isnull=True).delete()
