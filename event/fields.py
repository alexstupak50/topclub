# -*- encoding: utf-8 -*-

from django.db.models import FileField
from django.forms import forms
from django.template.defaultfilters import filesizeformat
from django.utils.translation import ugettext_lazy as _


class ContentTypeRestrictedFileField(FileField):

    def __init__(self, *args, **kwargs):
        self.content_types = kwargs.pop('content_types', list())
        self.max_upload_size = kwargs.pop('max_upload_size', '10485760')
        super(ContentTypeRestrictedFileField, self).__init__(*args, **kwargs)

    def clean(self, *args, **kwargs):
        data = super(ContentTypeRestrictedFileField, self).clean(
            *args, **kwargs)
        file = data.file

        try:
            content_type = file.content_type
            if content_type in self.content_types:
                if file._size > self.max_upload_size:
                    raise forms.ValidationError(
                        _(u'Максимальный размер загружаемых файлов %s.') % (
                            filesizeformat(self.max_upload_size),))
            else:
                raise forms.ValidationError(_(u'Формат не поддерживаеться.'))
        except AttributeError:
            pass

        return data


from south.modelsinspector import add_introspection_rules
add_introspection_rules([], ["^event\.fields\.ContentTypeRestrictedFileField"])