# -*- encoding: utf-8 -*-

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.core.files.uploadedfile import SimpleUploadedFile
from datetime import date, datetime
from hashlib import md5
import random


class FileKirilicMixin(object):
    file_filed = None

    def save(self, *args,**kwargs):
        if self.file_filed:
            for name in self.file_filed:
                reupload_image(getattr(self, name))
        return super(FileKirilicMixin, self).save(*args, **kwargs)


def i_image(instance, filename):
    return image_path(instance, filename, 'i')


def image_path(instance, filename, dirname):
    ext = filename.rsplit('.')[-1].lower()
    hash = md5(str(random.random()) + '%s' % datetime.now())
    new_filename = '%s.%s' % (hash.hexdigest()[3:10], ext)
    return u'%s/%s/%d/%d/%s' % (dirname, date.today().year, date.today().month,
                                date.today().day, new_filename)


def reupload_image(field):
    if not hasattr(field.name, 'lower'):
        return
    try:
        str(unicode(field.name.lower()))
    except (UnicodeDecodeError, UnicodeEncodeError):
        img_name = i_image(field, field.name)
        img = SimpleUploadedFile(img_name, field.read())
        field.save(img_name, img, save=True)


class LoginRequiredMixin(object):

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(*args, **kwargs)