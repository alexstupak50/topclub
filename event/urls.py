# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

from event.views import EventListView, EventCreateView, EventDetailView, \
    EventEditView, EventUpView, CommitAddView, CommentPageView, \
    EventUploadView, EventUploadUrlView, EventUploadDeleteView, EventDeleteView
from core.views import event_make_like
from .models import Event

urlpatterns = patterns('',
    url(r'^$', EventListView.as_view(), name='list'),
    url(r'^events/add/$', EventCreateView.as_view(), name='create'),
    url(r'^events/(?P<pk>\d+)/$', EventDetailView.as_view(), name='detail'),
    url(r'^events/(?P<pk>\d+)/edit/$', EventEditView.as_view(), name='edit'),
    url(r'^events/(?P<pk>\d+)/edit/(?P<key>\w+)', EventEditView.as_view(),
        name='edit_for_key'),
    url(r'^events/(?P<pk>\d+)/up/$', EventUpView.as_view(), name='up'),
    url(r'^events/(?P<pk>\d+)/comment/add/$', CommitAddView.as_view(),
        name='comment_add'),
    url(r'^events/(?P<pk>\d+)/comment/$', CommentPageView.as_view(),
        name='comment_page'),

    url(r'^events/upload/(?P<pk>\d+)/(?P<field>\w+)/$',
        EventUploadView.as_view(), name='upload'),

    url(r'^events/upload_from_url/(?P<pk>\d+)/(?P<field>\w+)/$',
        EventUploadUrlView, name='upload_from_url'),

    url(r'^events/upload/del/(?P<pk>\d+)/(?P<field>\w+)/$',
        EventUploadDeleteView.as_view(), name='upload_del'),
    url(r'^events/delete/(?P<pk>\d+)/$',
        EventDeleteView.as_view(), name='delete'),

    url(r'^events/(?P<pk>\d+)/(?P<kind>["like"|"dislike"]+)/$', event_make_like, name='like'),
    # url(r'^events/(?P<pk>\d+)//$', event_make_dislike, name='dislike'),

    url(r'^events/img/(?P<pk>\d+)/$', 'catalog.views.img_add',
        {'model': Event}, name='img_add'),
    url(r'^events/img_url/(?P<pk>\d+)/$', 'catalog.views.img_url_add',
        {'model': Event}, name='img_url_add'),
    url(r'^events/img/(?P<pk>\d+)-(?P<image_pk>\d+)/(?P<action>delete|cover)/$',
        'catalog.views.img_control', {'model': Event}, name='img_control'),
)
