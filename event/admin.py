# -*- encoding: utf-8 -*-

from django.contrib import admin

from event.models import Event, EventCategory


class EventCategoryAdmin(admin.ModelAdmin):
    list_display = ('title', 'hidden')
    list_filter = ('hidden',)
    search_fields = ('title',)

admin.site.register(EventCategory, EventCategoryAdmin)


class EventAdmin(admin.ModelAdmin):
    list_display = ('title', 'category', 'user', 'datetime', 'created')
    list_filter = ('category',)
    search_fields = ('title', 'description')

    def queryset(self, request):
        user = request.user
        filters = {}
        if not user.is_superuser:
            filters['region'] = user.admin_region
        return super(EventAdmin, self).queryset(request).filter(title__isnull=False).filter(**filters)


admin.site.register(Event, EventAdmin)