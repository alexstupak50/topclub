# -*- coding:utf-8 -*-
from django import forms
from django.core.urlresolvers import reverse
from django.core.cache import cache
from django.template.defaultfilters import filesizeformat
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.core.exceptions import ValidationError
from django.db import connection
from smart_selects.widgets import ChainedSelect

from catalog.signals import AdvertPublished
from catalog.models import Advert, AdvertImage, Category, Catalog
from core.utils import get_location_data, cprint
from core.models import LocCity, LocRegion
from itertools import chain
import logging
logger = logging.getLogger(__name__)

from pymorphy.django_conf import default_morph as morph


def cache_categories():
    cache_key = Advert.cached_categories_cache_key
    cached_categories = cache.get(cache_key)
    if cached_categories is None:
        catalogs = Catalog.objects.filter(hidden=False).values(
            'id', 'title', 'slug')
        catalogs_set, catagories = set(x['id'] for x in catalogs), {}
        keys = []
        for category in Category.objects\
                .filter(catalog_id__in=catalogs_set, hidden=False)\
                .values('id', 'title', 'slug', 'catalog_id'):
            catagories.setdefault(category['catalog_id'], []).append(category)
            keys.append(category['id'])
        choices = list()
        for catalog in catalogs:
            if catalog['id'] in catagories:
                c = [tuple((x['id'], x['title'])) for x in catagories[catalog['id']]]
                choices.append((catalog['title'], tuple(c)), )
        # 60 sec * 60 min * 24 hours * 7 days
        cache_time = 60*60*24*7
        cached_categories = {'categories': tuple(choices), 'keys': keys}
        cache.set(cache_key, cached_categories, int(cache_time))
    return cached_categories


class LocationFormMixin(forms.ModelForm):
    '''
    Mixin with implemented lat and lng validation.
    Need to include 'lat' and 'lng' into your form fields defenition.
    '''
    _loc_error_messages = {
        'country_mismatch': _(u"Вы поставили маркер в {0}. К сожалению мы не \
                              работаем в данной стране."),
        'invalid_point': _(u'Выбранная точка не относится ни к какому\
                           населенному пункту либо находится на шоссе.'),
        }

    lat = forms.FloatField(widget=forms.HiddenInput(), error_messages={'required': _(u'Отметьте местопроведение иветна')})
    lng = forms.FloatField(widget=forms.HiddenInput(), error_messages={'required': _(u' ')})

    def __init__(self, *args, **kwargs):
        if 'location' in kwargs:
            self.location = kwargs.pop('location')
        super(LocationFormMixin, self).__init__(*args, **kwargs)


    def save(self, commit=True, *args, **kwargs):
        obj = super(LocationFormMixin, self).save(*args, commit=False, **kwargs)
        if hasattr(self, 'city_id'):
            obj.city_id = self.city_id
        if hasattr(self, 'region_id'):
            obj.region_id = self.region_id
        if commit:
            obj.save()
        return obj

    @staticmethod
    def cached_cities():
        cache_key = Advert.popular_cities_cache_key
        cached_cities = cache.get(cache_key)
        if cached_cities is None:
            # 60 sec * 60 min * 24 hours * 7 days
            cache_time = 60*60*24*7
            cached_cities = [x.lower() for x in LocCity.objects.filter(
                important=True).values_list('title_ru', flat=True)]
            cache.set(cache_key, cached_cities, int(cache_time))
        return cached_cities

    def clean_lng(self):
        lat, lng = self.cleaned_data.get('lat'), self.cleaned_data.get('lng')
        print lat, lng
        if all([lat, lng]):
            loc_data = get_location_data(logger, lat, lng)
            countries = getattr(settings, 'IPGEOBASE_ALLOWED_COUNTRIES', 'BY')
            if loc_data['CountryNameCode'] in countries:
                # Выбранная точка не относится ни к какому населенному пункту
                # либо находится на шоссе
                # The selected point does not apply to any locality
                # or is located on highway
                if 'AdministrativeArea' not in loc_data:
                    raise forms.ValidationError(
                        self._loc_error_messages['invalid_point'])

                # Sometimes when the city is important, yandex api returns
                # city name instead of region
                if loc_data['AdministrativeArea']['AdministrativeAreaName'].lower() in self.cached_cities():
                    try:
                        city = LocCity.objects.get(title_ru__iexact=loc_data['AdministrativeArea']['AdministrativeAreaName'])
                        self.city_id = city.id
                        self.region_id = city.region_id
                    except LocCity.DoesNotExist:
                        pass

                # Trying to get location by city name
                if 'Locality' in loc_data['AdministrativeArea'] and\
                   not hasattr(self, 'city_id'):
                    try:
                        city = LocCity.objects.get(title_ru__iexact=loc_data['AdministrativeArea']['Locality']['LocalityName'])
                        self.city_id = city.id
                        self.region_id = city.region_id
                    except (LocCity.DoesNotExist, KeyError):
                        pass

                # Trying to get location by region
                if not hasattr(self, 'city_id'):
                    try:
                        region = LocRegion.objects.get(title_ru__iexact=loc_data['AdministrativeArea']['AdministrativeAreaName'])
                        self.region_id = region.id
                        self.city_id = None
                    except LocRegion.DoesNotExist:
                        pass
            else:
                raise forms.ValidationError(
                    self._loc_error_messages['country_mismatch'].format(morph.inflect_ru(loc_data['CountryName'].upper(), u'пр').capitalize())
                    )
        elif hasattr(self, 'location') and self.location:
            self.city_id = self.location.pk
            self.region_id = self.location.region_id
        return lng

    def clean(self):
        # clear lat and lng in form data if something is wrong with it
        lat, lng = self.cleaned_data.get('lat'), self.cleaned_data.get('lng')
        if any([lat and not lng, not lat and lng]) or \
           (all([lat, lng]) and not any([hasattr(self, 'city_id'), hasattr(self, 'region_id')])):
            try:
                del self.data['lat']
                del self.cleaned_data['lat']
            except KeyError:
                pass
            try:
                del self.data['lng']
                del self.cleaned_data['lng']
            except KeyError:
                pass
        return self.cleaned_data


class AddAdvertForm(LocationFormMixin):
    draft_allow_fields = ['contacts', 'currency', 'description', 'keywords',
                          'phone', 'price', 'title', 'type']
    category_id = forms.TypedChoiceField(label=u'Категория',
                                         required=True,
                                         coerce=int,
                                         empty_value=0)
    _error_messages = {
        'draft_denied': _(u'Чтобы сохранить заполните хотя бы несколько полей.')
        }

    class Meta:
        model = Advert
        fields = ('category_id', 'title', 'type', 'price', 'currency',
                  'auction', 'keywords', 'description', 'lat', 'lng',
                  'phone', 'contacts')

    def __init__(self, *args, **kwargs):
        self._draft = False
        super(AddAdvertForm, self).__init__(*args, **kwargs)
        # caching
        self.cached_categories = cache_categories()
        # category choices and initial
        self.fields['category_id'].choices = (0, _(u'Выберите категорию...')),
        self.fields['category_id'].choices += self.cached_categories['categories']
        self.fields['category_id'].initial = kwargs['instance'].category_id
        # other
        self.fields['description'].widget.attrs.update({'id': 'tiny'})
        self.fields['phone'].widget.attrs.update(
            {'placeholder': '+375 (xx) xxxxxxx'})
        self.fields['phone'].required = True
        self.fields['keywords'].widget.attrs.update({'placeholder': _(u'красный, дерево')})

    @property
    def draft(self):
        return self._draft

    @draft.setter
    def draft(self, value):
        ''' Set form fields to not required when draft is set to True '''
        self._draft = value
        if self._draft:
            for field in self.fields:
                self.fields[field].required = False

    def save(self, commit=True, *args, **kwargs):
        obj = super(AddAdvertForm, self).save(*args, commit=False, **kwargs)
        published = False
        # публикация
        if not self.draft and not self.instance.public:
            obj.public_date = timezone.now()
            obj.public = True
            published = True
        obj.category_id, obj.blank = self.cleaned_data.get('category_id'), False
        if commit:
            obj.save()
        if published:
            AdvertPublished.send(sender=Advert, instance=obj)
        return obj

    def check_images_exist(self):
        if AdvertImage.objects.filter(advert_id=self.instance.id).exists():
            return True
        return False

    def clean_category_id(self):
        category = self.cleaned_data['category_id']
        # черновик и поле пустое - не проверяем
        if all([not self.fields['category_id'].required, not category]):
            return
        if not category:
            raise forms.ValidationError(
                self.fields['category_id'].error_messages['required'])
        return category

    def clean(self):
        super(AddAdvertForm, self).clean()
        # Если черновик, то проверяем заполнены ли минимальные данные,
        #                                                 либо есть изображения
        if all([self.instance.blank, self.draft]) and \
           not (any([x in self.draft_allow_fields for x in self.changed_data]) or self.check_images_exist()):
            raise forms.ValidationError(self._error_messages['draft_denied'])

        return self.cleaned_data


class AdvertImageUrlForm(forms.Form):

    img_from_url = forms.URLField(max_length=500, required=False, label=_(u'Фото по url'),
                                  help_text=_(u'Вставьте ссылку и нажмите Enter'))


class ImageFormBase(forms.ModelForm):
    max_count = 8
    patt_url = 'catalog:img_add'

    def __init__(self, *args, **kwargs):
        post_url = None
        if 'advert' in kwargs:
            post_url = reverse(self.patt_url,
                               args=(kwargs.pop('advert').id,))
        super(ImageFormBase, self).__init__(*args, **kwargs)
        self.fields['image'].widget.attrs.update(
            {'multiple': 'multiple',
             'data-post-url': post_url,
             'accept': 'image/jpeg,image/png,image/gif',
             'max': getattr(settings, 'UPLOAD_IMAGE_MAX_COUNT', self.max_count)})

    def clean_image(self):
        image = self.cleaned_data['image']
        image_type = image.content_type.split('/')[0]
        types = getattr(settings, 'UPLOAD_IMAGE_CONTENT_TYPES', ['image'])
        if image_type in types:
            max_size = getattr(settings, 'UPLOAD_IMAGE_MAX_SIZE', 524288)
            if image._size > max_size:
                raise forms.ValidationError(
                    u'Допутимый размер изображения до %s.\
                    Текущий размер %s' % (
                        filesizeformat(max_size),
                        filesizeformat(image._size))
                    )
        else:
            raise forms.ValidationError(
                u'Неправильный тип файла.\
                Поддерживаемы типы: image/jpeg, image/png, image/gif.')
        return image

    def save(self, advert_id=None, commit=True, *args, **kwargs):
        obj = super(ImageFormBase, self).save(*args, commit=False, **kwargs)
        # cursor = connection.cursor()
        # cursor.execute("SELECT COUNT(id) FROM catalog_advertimage WHERE advert_id=%s", [advert_id])
        # cursor.close()

        if advert_id and \
           self.Meta.model.objects.filter(advert_id=advert_id).count() >= self.max_count:
            return False
        obj.advert_id = advert_id
        if commit:
            obj.save()
        return obj


class AdvertImageForm(ImageFormBase):

    class Meta:
        model = AdvertImage
        exclude = ('advert',)
        widgets = {
            'num': forms.HiddenInput(),
        }


class CategoryFilterForm(forms.Form):
    _all = (0, _(u'Все')),

    advtype = forms.TypedChoiceField(label=_(u'Тип обьявлений'),
                                  required=True,
                                  coerce=int,
                                  initial=0,
                                  empty_value=0,
                                  choices=_all + Advert.type_choices)
    price_min = forms.IntegerField(label=_(u'Цена от'),
                                   required=False)
    price_max = forms.IntegerField(label=_(u'до'),
                                   required=False)
    keywords = forms.CharField(label=_(u'Ключевые слова'),
                               required=False)

    region = forms.ModelChoiceField(
        label=_(u'Регион'), queryset=LocRegion.objects.all(), empty_label=u"Регион", required=False)
    city = forms.ModelChoiceField(queryset=LocCity.objects.all(), empty_label=u"Город", required=False,
                                  widget=ChainedSelect(
                                      app_name='core',
                                      model_name='LocCity',
                                      chain_field='region',
                                      model_field='region',
                                      auto_choose=False,
                                      show_all=False,)
                                  )

    def __init__(self, *args, **kwargs):
        super(CategoryFilterForm, self).__init__(*args, **kwargs)
        self.fields['keywords'].widget.attrs.update({
            'placeholder': _(u'через запятую'),
            })
        self.fields['city'].widget.queryset = LocCity.objects.all()
        if not self.data.get('region') or self.data.get('region') == '':
            self.fields['city'].widget.attrs['disabled'] = 'true'


class CatalogAdminForm(forms.ModelForm):

    class Meta:
        model = Catalog

    def clean_is_popular(self):
        Catalog.check_pupular_valid()
        return super(CatalogAdminForm, self).clean_is_pupular()
