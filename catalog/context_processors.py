# -*- coding: utf-8 -*-
from django.core.cache import cache

from .models import Advert


def count_advert(request):
    cache_key = 'count_adverts'
    cache_time = 60*60
    c = cache.get(cache_key)
    if c is None:
        c = Advert.objects_active.all().count()
        cache.set(cache_key, c, cache_time)
    return {
        'count_advert': c
    }