# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Catalog'
        db.create_table(u'catalog_catalog', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('hidden', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=254)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=100)),
            ('show_on_top', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'catalog', ['Catalog'])

        # Adding model 'Category'
        db.create_table(u'catalog_category', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('hidden', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=254)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=100)),
            ('show_on_top', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('catalog', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalog.Catalog'])),
        ))
        db.send_create_signal(u'catalog', ['Category'])

        # Adding model 'Advert'
        db.create_table(u'catalog_advert', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('hidden', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=254)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('changed_last', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, auto_now_add=True, blank=True)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalog.Category'])),
            ('type', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=1)),
            ('price', self.gf('django.db.models.fields.IntegerField')(max_length=10)),
            ('currency', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=1)),
            ('auction', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('description', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'catalog', ['Advert'])

        # Adding model 'AdvertImage'
        db.create_table(u'catalog_advertimage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('advert', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalog.Advert'])),
        ))
        db.send_create_signal(u'catalog', ['AdvertImage'])


    def backwards(self, orm):
        # Deleting model 'Catalog'
        db.delete_table(u'catalog_catalog')

        # Deleting model 'Category'
        db.delete_table(u'catalog_category')

        # Deleting model 'Advert'
        db.delete_table(u'catalog_advert')

        # Deleting model 'AdvertImage'
        db.delete_table(u'catalog_advertimage')


    models = {
        u'catalog.advert': {
            'Meta': {'ordering': "('-created',)", 'object_name': 'Advert'},
            'auction': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.Category']"}),
            'changed_last': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'currency': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.IntegerField', [], {'max_length': '10'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'type': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'})
        },
        u'catalog.advertimage': {
            'Meta': {'object_name': 'AdvertImage'},
            'advert': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.Advert']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'})
        },
        u'catalog.catalog': {
            'Meta': {'object_name': 'Catalog'},
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'show_on_top': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '100'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '254'})
        },
        u'catalog.category': {
            'Meta': {'object_name': 'Category'},
            'catalog': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.Catalog']"}),
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'show_on_top': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '100'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '254'})
        }
    }

    complete_apps = ['catalog']