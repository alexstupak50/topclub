# -*- coding: utf-8 -*-
import logging
import os
from random import sample, choice, randint
from shutil import copyfile
from django.conf import settings

from cabinet.models import UserProfile as User
from catalog.models import (Category,
                            Advert,
                            AdvertImage,
                            )


class FakeFill(object):
    """
    Backend to fill db with fake adverts.

    """

    def __init__(self, logger=None, amount=100):
        self.BASE_DIR = getattr(settings, 'BASE_DIR')
        self.MEDIA_DIR = getattr(settings, 'MEDIA_ROOT')
        self.images_put_path = 'fake_images'
        self.images_src_path = os.path.join(self.BASE_DIR, 'local/fake_images')
        self.images_formats = ['jpeg', 'jpg', 'png']
        self.amount = amount
        self.bulk_threshold = 500 if amount > 500 else amount
        self.images = []
        self.logger = logger or logging.getLogger(__name__)

    def clear_database(self):
        """ Removes all adverts stored in database.
            Useful for development, never use on production.
        """
        self.logger.info('Removing obsolete adverts from database...')
        while Advert.objects.exists():
            ids = Advert.objects.values_list('pk', flat=True)[:750]
            Advert.objects.filter(pk__in=ids).delete()
        while AdvertImage.objects.exists():
            ids = AdvertImage.objects.values_list('pk', flat=True)[:750]
            AdvertImage.objects.filter(pk__in=ids).delete()

    def sync_database(self):
        self.logger.info('Looking for images in {0} of adverts... \n'.format(self.images_src_path))
        self.get_avialable_images()
        self.logger.info('Found {0} of images... \n'.format(len(self.images)))
        self.logger.info('Creating {0} of adverts... \n'.format(self.amount))
        self.create_adverts()
        self.logger.info('--- done\n')
        self.logger.info('Creating and assign images... \n')
        self.create_images()
        self.logger.info('Created...')
        self.logger.info(' ... make assign... ')
        self.make_covers()
        self.logger.info('All done.')

    def get_type(self):
        return choice(Advert.type_choices)[0]

    def get_description(self):
        if choice([True, False]):
            return text[:randint(20, 300)]
        return ''

    def get_user(self):
        if not hasattr(self, 'user'):
            self.user = User.objects.all().first().id
        return self.user

    def get_phone(self):
        return u'+375 (23) 2323232'

    def get_currency(self):
        return choice(Advert.currency_choices)[0]

    def get_price(self):
        return randint(1, 100000)

    def get_title(self):
        return choice(titles)

    def get_category(self):
        if not hasattr(self, 'categories'):
            self.categories = Category.objects.filter(hidden=False
                ).values_list('id', flat=True)
        return choice(self.categories)

    def get_images(self):
        if not self.images: return
        if not hasattr(self, 'img_max'):
            if 1 <= len(self.images) <= 8:
                self.img_max = len(self.images)
            elif len(self.images) > 8:
                self.img_max = 8
        return sample(self.images, randint(1, self.img_max))

    def get_avialable_images(self):
        """
        Find all images in the given image src dir and copy them to
        project media dir.
        """
        for dirname, dirnames, filenames in os.walk(self.images_src_path):
            for filename in filenames:
                _format = filename.split('.')[-1].lower()
                if _format in self.images_formats:
                    copyfile(os.path.join(self.images_src_path, filename),
                             os.path.join(self.MEDIA_DIR, self.images_put_path, filename))
                    self.images.append(os.path.join(self.images_put_path, filename))

    def get_avdert_data(self):
        return Advert(
            blank=False,
            public=True,
            title=self.get_title(),
            category_id=self.get_category(),
            type=self.get_type(),
            price=self.get_price(),
            currency=self.get_currency(),
            description=self.get_description(),
            phone=self.get_phone(),
            user_id=self.get_user()
            )

    def create_adverts(self):
        _amount = 0
        create_list = []
        while _amount < self.amount:
            create_list.append(self.get_avdert_data())
            if len(create_list) > self.bulk_threshold:
                _amount += len(create_list)
                Advert.objects.bulk_create(create_list)
                del create_list[:]
        Advert.objects.bulk_create(create_list)

    def create_images(self):
        images_list = []
        for advert in Advert.objects.filter(blank=False, public=True, hidden=False).values_list('id', flat=True):
            images_files = self.get_images()
            for image in images_files:
                images_list.append(AdvertImage(image=image, advert_id=advert))
            if len(images_list) > 500:
                AdvertImage.objects.bulk_create(images_list)
                del images_list[:]
        AdvertImage.objects.bulk_create(images_list)

    def make_covers(self):
        for advert in Advert.objects.filter(blank=False, public=True, hidden=False):
            advert.cover_id = choice(AdvertImage.objects.filter(advert_id=advert.id).values_list('id', flat=True))
            advert.save()

text = u"\
Lorem Ipsum - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. \
Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI \
века. В то время некий безымянный печатник создал большую коллекцию размеров и \
форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не \
только успешно пережил без заметных изменений пять веков, но и перешагнул в \
электронный дизайн. Его популяризации в новое время послужили публикация листов \
Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, \
программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых \
используется Lorem Ipsum.\
"
titles = [
u"Шкаф", u"Стол", u"Стул", u"Кресло", u"Диван", u"Кровать",

u"Монитор", u"Мышка", u"Клавиатура", u"Ноутбук", u"Компьютер", u"Винчестер",
u"Сервер",

u"Дом", u"Квартира", u"Офис", u"Помещение", u"Комната",

u"Машина", u"Велосипед", u"Мопед", u"Мотороллер", u"Грузовик", u"Автобус",
u"Пикап",

u"Штаны", u"Джинсы", u"Футболка", u"Белье", u"Рубашка", u"Кепка", u"Шапка",
u"Окно", u"Двери", u"Стекло",
u"Бумага", u'Ручка', u"Карандаш", u"Резинка стирательная",
]
