from django.db.models import signals
from django.dispatch import receiver, Signal

from django.core.cache import cache
from django.conf import settings

from catalog.models import (Catalog,
                            Category,
                            Advert,
                            AdvertImage)
from comments.models import Comment
from core.utils import (delete_image_on_delete,
                        delete_image_on_change,
                        GetCourse,
                        )


AdvertPublished = Signal(providing_args=["instance"])


def delete_cabinet_menu_cache(user_id):
    '''
    Clear cached counts for user's cabinet menu.
    '''
    key = getattr(settings, 'CABINET_MENU_CACHE_KEY')
    cache.delete(key.format(user_id))


def update_comments_count(sender, **kwargs):
    try:
        kwargs.get('instance').content_object.update_comments_count()
        delete_cabinet_menu_cache(kwargs.get('instance').user_id)
    except AttributeError:
        pass


def update_latest_block(**kwargs):
    cache_key = getattr(settings, 'LATEST_CACHE_KEY')
    # 60 sec * 60 min * 24 hours
    cache_time = 60*60*24
    c = cache.get(cache_key)
    if c:
        del c[:1]
        instance = kwargs.get('instance')
        c.append({'pk': instance.pk,
                  'title': instance.title,
                  'price': instance.price,
                  'currency': instance.currency,
                  'type': instance.type,
                  'cover__image': instance.cover.image if instance.cover else None})
        cache.set(cache_key, c if c else None, int(cache_time))


signals.post_save.connect(update_comments_count, sender=Comment)
signals.post_delete.connect(update_comments_count, sender=Comment)
#signals.post_delete.connect(delete_image_on_delete, sender=AdvertImage)
#signals.post_save.connect(delete_image_on_change, sender=AdvertImage)
AdvertPublished.connect(update_latest_block, sender=Advert)


# @receiver(signals.post_delete, sender=AdvertImage)
# def auto_del_advert_image_on_delete(sender, instance, **kwargs):
#     delete_image_on_delete(sender, instance, **kwargs)


# @receiver(signals.post_save, sender=AdvertImage)
# def auto_del_advert_image_on_change(sender, instance, **kwargs):
#     delete_image_on_change(sender, instance, **kwargs)


@receiver(signals.post_delete, sender=Advert)
def advert_post_delete(sender, instance, **kwargs):
    delete_cabinet_menu_cache(instance.user_id)


@receiver(signals.pre_save, sender=Advert)
def advert_post_save(sender, instance, **kwargs):
    # set default cover image
    if not instance.cover_id and AdvertImage.objects.filter(advert_id=instance.pk).exists():
        instance.cover = AdvertImage.objects.filter(advert_id=instance.pk).first()
    if not instance.blank and instance.public:
        instance.price_s = GetCourse().convert(instance.currency, instance.price)
    # clear menu chache
    delete_cabinet_menu_cache(instance.user_id)


def clear_cache():
    '''
    We are caching category choices for advert add from.
    Aslo we are caching main menu.
    So, we need to renew it when something hase changed in category or catalog data.

    You must call me when something hase changed in category or catalog data.
    '''
    # delete add form choices cache
    cache.delete(Advert.cached_categories_cache_key)
    # delete main menu cache
    cache.delete(getattr(settings, 'MAIN_MENU_CACHE_KEY'))


@receiver(signals.post_delete, sender=Category)
def category_post_save(sender, instance, **kwargs):
    clear_cache()


@receiver(signals.post_save, sender=Category)
def category_post_save(sender, instance, **kwargs):
    clear_cache()


@receiver(signals.post_delete, sender=Catalog)
def catalog_post_save(sender, instance, **kwargs):
    clear_cache()


@receiver(signals.post_save, sender=Catalog)
def catalog_post_save(sender, instance, **kwargs):
    clear_cache()
