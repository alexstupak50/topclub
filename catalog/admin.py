# -*- coding:utf-8 -*-
from django import forms
from django.db import models
from django.contrib import admin
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _
from django.utils.html import format_html

from catalog.models import Catalog, Category, Advert, AdvertImage, Currency
from core.utils import ForeignKeyLinksMetaclass
from sorl.thumbnail import get_thumbnail
from sorl.thumbnail.admin.current import AdminImageWidget
from .forms import CatalogAdminForm


class AdvertImageInline(admin.TabularInline):
    classes = ('grp-collapse grp-closed',)
    model = AdvertImage
    extra = 0
    max_num = 8
    formfield_overrides = {
        models.ImageField: {'widget': AdminImageWidget},
    }


class AdvertAdminForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(AdvertAdminForm, self).__init__(*args, **kwargs)
        self.fields['cover'].queryset = self.fields['cover'].queryset.filter(
            advert_id=self.instance.pk)


class AdvertAdmin(admin.ModelAdmin):
    __metaclass__ = ForeignKeyLinksMetaclass

    # list_display
    list_per_page = 20
    list_display = ('public', 'type', 'title_and_thumbs', 'link_to_user', 'created')
    list_display_links = ('title_and_thumbs',)
    list_display_titles = {'link_to_user': _(u'Пользователь')}
    list_select_related = ('user',)
    list_filter = ('public', 'hidden', 'type', 'suspicious', 'actual')
    date_hierarchy = 'created'
    change_list_template = 'admin/custom_change_list.html'

    # detail
    inlines = [AdvertImageInline, ]
    form = AdvertAdminForm
    readonly_fields = ('public', 'uuid', 'created', 'changed_last',
                       'session_id', 'public_date', 'actual', 'price_s')
    fieldsets = (
        (None,
            {'fields': (('public', 'suspicious'),
                        ('actual', 'hidden'),
                        ('type', 'auction'),
                        ('price_s', 'price', 'currency'), )}),
        (_(u'Данные'),
            {'fields': ('title',
                        ('category', 'cover'),
                        'keywords', 'description')}),

        (_(u'Контакты'), {
            'classes': ('grp-collapse grp-closed',),
            'fields': ('user', ('phone', 'contacts'), )}),

        (_(u'Гео'), {
            'classes': ('grp-collapse grp-closed',),
            'fields': (('region', 'city'), ('lat', 'lng'), )}),

        (u'Системная', {
            'classes': ('grp-collapse grp-closed',),
            'fields': ('public_date',
                       ('created', 'changed_last'),
                       ('uuid', 'session_id'), )})
    )
    raw_id_fields = ('region', 'city', 'user')
    autocomplete_lookup_fields = {
        'fk': ['region', 'city', 'user'],
    }

    class Media:
        css = {'all': ('css/jquery.fancybox.css',
                       'css/jquery.fancybox-thumbs.css')}
        js = ('js/jquery-1.10.1.min.js', 'js/jquery.mousewheel-3.0.6.pack.js',
              'js/jquery.fancybox.pack.js', 'js/jquery.fancybox-thumbs.js')

    def title_and_thumbs(self, obj):
        if obj.title:
            body = obj.title + '<br>'
        else:
            body = _(u'<Заголовок отсутствует>') + '<br>'
        if obj.advertimage_set.all():
            for img in obj.advertimage_set.all():
                if img.image:
                    thumbnail = get_thumbnail(img.image, '75x75', crop='center',
                                              quality=99)
                    body += '</a><a\
                            class="fancybox-thumbs"\
                            data-fancybox-group="{2}"\
                            href="{0}">\
                            <img src="{1}" alt="img">'.format(
                            img.image.url, thumbnail.url, obj.id)
        return body
    title_and_thumbs.short_description = _(u'превью')
    title_and_thumbs.allow_tags = True

    def queryset(self, request):
        user = request.user
        filters = {}
        if not user.is_superuser:
            filters['region'] = user.admin_region
        return super(AdvertAdmin, self).queryset(request).filter(
            blank=False
            ).filter(**filters).prefetch_related('advertimage_set')


class CategoryAdmin(admin.ModelAdmin):
    __metaclass__ = ForeignKeyLinksMetaclass

    list_display = ('hidden', 'title', 'slug',
                    'link_to_catalog')
    list_display_titles = {'link_to_catalog': _(u'Каталог')}
    list_display_links = ('title',)
    list_select_related = ('catalog',)
    list_filter = ('catalog', 'hidden')
    list_editable = ('hidden',)
    prepopulated_fields = {"slug": ("title",)}


class CatalogAdmin(admin.ModelAdmin):
    list_display = ('hidden', 'title', 'slug', 'ordering', 'is_popular')
    list_display_links = ('title',)
    list_filter = ('is_popular', 'hidden')
    list_editable = ('hidden', 'ordering')
    prepopulated_fields = {"slug": ("title",)}
    # form = CatalogAdminForm


class CurrencyAdmin(admin.ModelAdmin):
    list_display = ('kind', 'coefficient', 'is_main')
    list_editable = ('is_main',)


admin.site.register(Catalog, CatalogAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Advert, AdvertAdmin)
admin.site.register(Currency, CurrencyAdmin)
