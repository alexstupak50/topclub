import datetime
from haystack import indexes
from catalog.models import Advert


class AdvertIndex(indexes.SearchIndex, indexes.Indexable):
    # text = indexes.CharField(document=True, use_template=True)
    # title = indexes.CharField(model_attr='user')
    # pub_date = indexes.DateTimeField(model_attr='pub_date')
    text = indexes.CharField(document=True, use_template=True)
    title = indexes.CharField(model_attr='title')
    keywords = indexes.CharField(model_attr='keywords')

    def get_model(self):
        return Advert

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
    def index_queryset(self, using=None):
        return self.get_model().objects.filter(hidden=False, public=True)
