# -*- coding:utf-8 -*-
from django.db import models
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes import generic
from django.core.exceptions import ValidationError
from django.shortcuts import get_object_or_404
from core.utils import path, mat_validator
from core.models import LocCity, LocRegion
from cabinet.models import UserProfile as User
from comments.models import Comment
from django.core.cache import cache

from shortuuidfield import ShortUUIDField
from sorl.thumbnail import get_thumbnail


class PreBase(models.Model):
    hidden = models.BooleanField(u'скрыть', default=False, 
                                 help_text=u'Будет скрыто, если отмечено')
    title = models.CharField(u'название', max_length=50, validators=[mat_validator])

    class Meta:
        abstract = True

    def __unicode__(self):
        return self.title


class Base(models.Model):
    created = models.DateTimeField(u'дата создания', auto_now_add=True)
    changed_last = models.DateTimeField(u'последнее изменение', auto_now=True,
                                        auto_now_add=True)

    class Meta:
        abstract = True


class CatMixin(models.Model):
    slug = models.SlugField(
        u'ЧПУ', max_length=100, unique=True, db_index=True,
        help_text=u'Подробнее <a href="http://ru.wikipedia.org/wiki/%D0%A7%D0%9'
                  u'F%D0%A3_%28%D0%98%D0%BD%D1%82%D0%B5%D1%80%D0%BD%D0%B5%D1%82'
                  u'%29" target="_blank">http://ru.wikipedia.org/wiki/%D0%A7%D0'
                  u'%9F%D0%A3_%28%D0%98%D0%BD%D1%82%D0%B5%D1%80%D0%BD%D0%B5%D1%'
                  u'82%29</a>.')
    # show_on_top = models.BooleanField(
    #     u'в топе', default=False,
    #     help_text=u'Будет в топе главной/каталога, если отмечено.')

    class Meta:
        abstract = True


def validator_is_popular(val):
    if Catalog.objects.filter(is_popular=True).count() > Catalog.MAX_COUNT_POPULAR:
        raise ValidationError(u'Уже выбрано %s главных раздела' % str(Catalog.MAX_COUNT_POPULAR))


class Catalog(PreBase, CatMixin):
    icon = models.ImageField(u'Изображение', upload_to='category/', blank=True, null=True)
    ordering = models.IntegerField(_(u'Порядок вывода'), default=1)
    is_popular = models.BooleanField(u'Популярный раздел', default=False, validators=[validator_is_popular])

    MAX_COUNT_POPULAR = 4

    class Meta:
        verbose_name = u'каталог'
        verbose_name_plural = u'каталоги'
        ordering = ('ordering',)

    def get_absolute_url(self):
        return reverse('catalog:detail', args=(self.slug,))


class Category(PreBase, CatMixin):
    catalog = models.ForeignKey(Catalog, verbose_name=u'каталог')

    class Meta:
        verbose_name = u'категория'
        verbose_name_plural = u'категории'

    def get_absolute_url(self):
        return reverse('catalog:category_detail', args=(self.slug,))


class Currency(models.Model):
    kind = models.CharField(u'Валюта', max_length=30)
    is_main = models.BooleanField(u'Основная валюта', default=False)
    coefficient = models.DecimalField(u'Коефициент', max_digits=20, decimal_places=12, null=True)

    class Meta:
        verbose_name = u'Валюта'
        verbose_name_plural = u'Валюты'

    def __unicode__(self):
        return self.kind

    def save(self, *args, **kwargs):
        if self.is_main and Currency.objects.filter(is_main=True).count() > 1:
            raise ValidationError(u'Уже выбрана основная валюта')
        super(Currency, self).save(*args, **kwargs)


class AdvetManager(models.Manager):
    def get_queryset(self):
        return super(AdvetManager, self).get_queryset().filter(blank=False)


class Advert(PreBase, Base):

    # currency_choices = (
    #     (3, u'у.е.'),
    #     (1, u'BYR'),
    #     (2, u'RUB'),
    #     (4, u'EUR'),
    # )
    type_choices = (
        (1, u'Продам'),
        (2, u'Куплю'),
        (3, u'Аренда'),
        (4, u'Услуга'),
        (5, u'Обмен'),
    )
    popular_cities_cache_key = 'add-form-popular-cities'
    cached_categories_cache_key = 'add-form-categories'

    blank = models.BooleanField(u'заготовка', default=True, db_index=True)
    public = models.BooleanField(u'публичное', default=False, db_index=True)
    actual = models.BooleanField(u'актуально', default=True)
    suspicious = models.BooleanField(u'подозрительное', default=False)
    uuid = ShortUUIDField(u'uuid', editable=False)

    category = models.ForeignKey(
        Category, verbose_name=u'категория', null=True)
    type = models.PositiveSmallIntegerField(
        u'тип', choices=type_choices, null=True)
    price_s = models.DecimalField(
        u'цена USD', max_digits=20, decimal_places=2, null=True)
    price = models.DecimalField(
        u'цена', max_digits=20, decimal_places=2, null=True)
    currency = models.ForeignKey(Currency, verbose_name=u'валюта', blank=True, null=True)
    auction = models.BooleanField(u'торг', default=False)
    description = models.TextField(u'описание', blank=True, validators=[mat_validator])

    # координаты
    lat = models.FloatField(
        verbose_name=u'Широта', null=True, blank=True,
        help_text=u'Десятичное число. Например: 50.254444')
    lng = models.FloatField(
        verbose_name=u'Долгота', null=True, blank=True,
        help_text=u'Десятичное число. Например: 28.657778')
    region = models.ForeignKey(
        LocRegion, verbose_name=u'регион', null=True, blank=True)
    city = models.ForeignKey(
        LocCity, verbose_name=u'город', null=True, blank=True)

    # идентификация
    user = models.ForeignKey(
        User, verbose_name=u'пользователь', null=True, blank=True)
    session_id = models.CharField(
        u'идентификатор', max_length=254, blank=True,
        help_text=u'только для неавторизированных')
    phone = models.CharField(
        u"Телефон", default='', max_length=20, null=True, blank=True)
    contacts = models.CharField(
        u'skype/email/прочее', default='', max_length=50, null=True, blank=True)

    # обложка
    cover = models.ForeignKey(
        'AdvertImage', verbose_name=u'обложка', related_name='advert_covered',
        on_delete=models.SET_NULL, null=True, blank=True)
    keywords = models.CharField(
        u'ключевые слова', max_length=254, blank=True,
        help_text=u'Через запятую.')

    # даты
    public_date = models.DateTimeField(u'публикация', null=True)
    comments = generic.GenericRelation(Comment)
    comments_count = models.PositiveIntegerField(
        u'кол-во комментариев', default=0)

    objects = models.Manager()
    objects_active = AdvetManager()

    class Meta:
        ordering = ('-created',)
        verbose_name = u'объявление'
        verbose_name_plural = u'объявления'

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('catalog:advert_detail', args=(self.pk,))

    def type_display(self):
        return self.get_type_display()

    def update_comments_count(self):
        self.comments_count = self.comments.filter(hidden=False).count()
        self.save()

    def get_price(self):
        if self.price and self.currency:
            main_currency = get_object_or_404(Currency, is_main=True)
            if self.currency != main_currency:
                return '%0.3f(%s)' % (self.price / self.currency.coefficient, main_currency.kind)
            return '%s(%s)' % (self.price, self.currency.kind)
        return

    def get_cover_image(self):
        if self.cover and self.cover.image:
            return self.cover.image
        else:
            first = self.advertimage_set.first()
            return first.image if first else None


def advertimage_path(*args):
    """
    Returns a file path where to save a photo/image
    Default args are: instance, filename
    Also u can provide kwarg 'path' to path. Only string.
    """
    return path(*args, path='advert')


class AdvertImage(models.Model):
    image = models.ImageField(u'изображение', upload_to=advertimage_path, )
    advert = models.ForeignKey(Advert, verbose_name=u'объявление')

    class Meta:
        verbose_name = u'изображение'
        verbose_name_plural = u'изображения'

    def __unicode__(self):
        return _(u'изображение')

    def get_delete_url(self):
        return reverse(
            'catalog:img_control', args=(self.advert_id, self.id, 'delete'))

    def get_cover_url(self):
        return reverse(
            'catalog:img_control', args=(self.advert_id, self.id, 'cover'))

    def get_thumbnail(self, size='75x75', crop='center', quality=99):
        img = self.image
        return unicode(get_thumbnail(img, size, crop=crop, quality=quality).url)

    def get_thumbnail_html(self):
        html = '<img src="{0}" alt="img"/>'
        return html.format(self.get_thumbnail())

    # @property
    # def is_cover(self):
    #     if self.advert.cover_id == self.id:
    #         return True
    #     return False
