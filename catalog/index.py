#-*- coding:utf-8 -*-
from djapian import space, Indexer, CompositeIndexer

from catalog.models import Advert


class AdvertIndexer(Indexer):
    fields = ['text']
    tags = [
        ('title', 'title'),
        ('description', 'description'),
        ('keywords', 'keywords'),
    ]
    trigger = lambda indexer, obj: obj.public and not obj.hidden

space.add_index(Advert, AdvertIndexer, attach_as='indexer')

complete_indexer = CompositeIndexer(Advert.indexer)
