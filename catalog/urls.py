from django.conf.urls import patterns, url
from .models import Advert

urlpatterns = patterns('catalog.views',
    url(r'^market/$', 'flea_market', name='market'),
    url(r'^view/(?P<pk>\d+)/$', 'advert_detail', name='advert_detail'),
    url(r'^edit/(?P<pk>\d+)/$', 'advert_edit', name='advert_edit'),
    url(r'^delete/(?P<pk>\d+)/$', 'advert_delete', name='advert_delete'),
    url(r'^notactual/(?P<pk>\d+)/$', 'advert_notactual', name='advert_notactual'),
    url(r'^add/new/$', 'advert_new', name='new'),
    
    url(r'^add/(?P<pk>\d+)/$', 'advert_add', name='add'),

    url(r'^img/(?P<pk>\d+)/$', 'img_add', {'model': Advert}, name='img_add'),
    url(r'^img_url/(?P<pk>\d+)/$', 'img_url_add', {'model': Advert}, name='img_url_add'),
    url(r'^img/(?P<pk>\d+)-(?P<image_pk>\d+)/(?P<action>delete|cover)/$',
        'img_control', {'model': Advert}, name='img_control'),

    url(r'^(?P<slug>[\w-]+)/$', 'catalog_detail', name='detail'),
    url(r'^category/(?P<slug>[\w-]+)/$', 'category_detail', name='category_detail'),

)
