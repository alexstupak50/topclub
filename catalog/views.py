# -*- coding:utf-8 -*-
import json
import os
import urllib
import operator
from random import sample
from decimal import Decimal
from django.db.models import Q, Max, Min
from django.core.cache import cache
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.decorators.http import require_POST, require_GET
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from sorl.thumbnail import get_thumbnail
from django.core.files import File
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.utils.http import is_safe_url
from django import http

from catalog.forms import AddAdvertForm, AdvertImageForm, AdvertImageUrlForm, CategoryFilterForm
from catalog.models import Catalog, Category, Advert, AdvertImage
from django.http import HttpResponseRedirect, HttpResponseBadRequest, HttpResponse, HttpResponseNotFound
from core.utils import cprint, paginate_object
from core.models import LocCity, LocRegion
from event.forms import EventImageForm
from event.models import EventImage


def flea_market(request, redirect_field_name=REDIRECT_FIELD_NAME):
    """
    Returns main page, with catalogs and each adverts displayed.

    First get or create catalog dict with full advert list in cache.
    On each request retreiving adverts from cache and set 4 random adverts to
    each catalog.

    As result we get random adverts on each request.
    """

    auth = False
    if (redirect_field_name in request.POST or
            redirect_field_name in request.GET):
        next_url = request.POST.get(redirect_field_name,
                                    request.GET.get(redirect_field_name))
        # Security check -- don't allow redirection to a different host.
        if not is_safe_url(url=next_url, host=request.get_host()):
            next_url = request.path

        if request.user.is_authenticated():
            return HttpResponseRedirect(next_url)
        else:
            auth = next_url

    cache_key = 'main_page'
    # 60 sec * 60 min
    cache_time = 60*60
    # c = cache.get(cache_key)
    #TODO uncomment this on production
    c = None
    # caching
    if c is None:
        catalogs = Catalog.objects.filter(
            hidden=False,
            is_popular=True
            ).values('pk', 'title', 'slug')

        categories_set = set(Category.objects.filter(
            hidden=False,
            catalog_id__in=set(c['pk'] for c in catalogs)
            ).values_list('pk', flat=True))
        adverts = Advert.objects.filter(
            blank=False,
            hidden=False,
            public=True,
            actual=True,
            category_id__in=categories_set
            ).values('pk', 'title', 'type', 'price', 'currency',
                     'cover__image', 'category__catalog_id',)
        related_dict = {}
        for advert in adverts:
            related_dict.setdefault(advert['category__catalog_id'],
                                    []).append(advert)
        c = {'catalogs': catalogs,
             'related_dict': related_dict}
        cache.set(cache_key, c, int(cache_time))

    # set 4 random adverts to catalog with help of 'sample'
    for cat in c['catalogs']:
        try:
            len_rel_obj = len(c['related_dict'][cat['pk']])
            count = len_rel_obj if len_rel_obj < 4 else 4

            cat.update({
                'adverts': sample(c['related_dict'][cat['pk']], count),
                'count': len(c['related_dict'][cat['pk']]),
                # 'advertimages': Advert.objects.get(id=c['related_dict'][cat['pk']]).advertimage_set.all()
            })
        except (KeyError, ValueError):
            cat.update({'adverts': None, 'count': None})
    for item in c['catalogs']:
        if item['adverts']:
            for x in item['adverts']:
                x['advertimages'] = Advert.objects.get(id=x['pk']).advertimage_set.all()
    cache_key = 'pupular_catalogs'
    pupular_catalogs = cache.get(cache_key)
    if pupular_catalogs is None:
        pupular_catalogs = Catalog.objects.filter(is_popular=True)
        cache.set(cache_key, pupular_catalogs, int(cache_time))
    return render(request, 'base.html', {'auth': auth,
                                         'catalogs': c['catalogs'],
                                         'pupular_catalogs': pupular_catalogs
                                         })


def get_adverts(params={}):
    """
    Returns list of advert dicts
    """
    adverts = Advert.objects.filter(
        blank=False,
        hidden=False,
        actual=True,
        **params
        )
    return adverts


def base_filter(request, slug, category_list):
    filter_params = {'category_id__in': tuple(category_list), 'public': True}
    price_filter = {}
    form = CategoryFilterForm(request.GET or None)
    keywords, page = [], request.GET.get('page')
    # filter form validation & params update
    if form.is_valid() and form.has_changed():
        if 'advtype' in form.changed_data:
            filter_params.update({'type': form.cleaned_data['advtype']})
        # if 'date' in form.changed_data:
        #     filter_params.update({type: form.cleaned_data['type']})

        # use 'range' if min and max values given
        if 'price_min' in form.changed_data and \
           'price_max' in form.changed_data:
            price_filter.update(
                {'price_s__range': (form.cleaned_data['price_min'],
                                  form.cleaned_data['price_max'])
                 })
        else:
            if 'price_min' in form.changed_data:
                price_filter.update(
                    {'price_s__gte': form.cleaned_data['price_min']})
            if 'price_max' in form.changed_data:
                price_filter.update(
                    {'price_s__lte': form.cleaned_data['price_max']})

        if 'region' in form.changed_data:
            filter_params.update({'region': form.cleaned_data['region']})

        if 'city' in form.changed_data:
            filter_params.update({'city': form.cleaned_data['city']})

        if 'keywords' in form.changed_data:
            # filter_params.update({type: })
            keywords = [x.strip() for x in form.cleaned_data['keywords'].split(',')[:3]]
            # cprint(keywords)
            qs = reduce(operator.or_, (Q(keywords__icontains=kw) for kw in keywords))

    # apply filter params
    adverts = get_adverts(params=filter_params)

    # apply keyword params
    if 'qs' in locals():
        adverts = adverts.filter(qs)

    # caching min and max price values
    counted_hash = hash(
        frozenset(
            filter_params.items()
            ).union(keywords, slug)
        )
    cache_key = getattr(settings, 'FILTER_CACHE_KEY').format(counted_hash)
    price_delta = cache.get(cache_key)
    if price_delta is None:
        # 60 sec * 30 min
        cache_time = 60*30
        price_delta = adverts.aggregate(min=Min('price_s'),
                                        max=Max('price_s'))
        if price_delta['min'] and price_delta['max']:
            price_delta['min'] = round(price_delta['min'])
            price_delta['max'] = round(price_delta['max'] + Decimal(0.5))
            cache.set(cache_key, price_delta, int(cache_time))

    # when min and max counted, we can apply price filter
    # and use select_related() and set order
    adverts = adverts.filter(**price_filter).select_related(
        'cover', 'city', 'region'
        ).order_by('-public_date')

    # generate cache hash for paginator's count()
    counted_hash = hash(
        frozenset(
            price_filter.items() + filter_params.items()
            ).union(keywords, slug)
        )

    # paginate
    adverts = paginate_object(adverts, 20, cache_key=counted_hash, page=page)

    return form, adverts, price_delta


@require_GET
def catalog_detail(request, slug):
    """
    Страницы детального каталога, теперь имеет точно такой же функционал как детальной категории
    что бы не переписывать шаблон, просто передаем каталог вместо категории и добавльяем несколько проверок.

    """
    catalog = get_object_or_404(Catalog, slug=slug)
    form, adverts, price_delta = base_filter(
        request, slug, Category.objects.filter(catalog=catalog).values_list('id', flat=True))
    return render(request, "category.html", {'form': form,
                                             'adverts': adverts,
                                             'price_delta': price_delta,
                                             'catalog': catalog})


@require_GET
def category_detail(request, slug):
    """
    Renders list of adverts on category page
    """
    # initial
    category = get_object_or_404(Category.objects.select_related('catalog'),
                                 slug=slug)
    form, adverts, price_delta = base_filter(request, slug, (category.id, ))

    return render(request, "category.html", {'form': form,
                                             'adverts': adverts,
                                             'category': category,
                                             'price_delta': price_delta})


@require_GET
def advert_detail(request, pk, comment_form=None):
    """
    Renders advert details page
    """
    advert = get_object_or_404(
        Advert.objects.select_related(
            'user', 'category', 'category__catalog'
            ).prefetch_related(
            'advertimage_set'),
        pk=pk,
        blank=False,
        hidden=False,
        public=True,
        actual=True)
    return render(request, 'ad.html', {'advert': advert,
                                       'comment_form': comment_form})


def get_adv_params(request, pk):
    '''
    Getting filter params for Advert in cases logined user or not
    '''
    adv_params = {'pk': pk, 'hidden': False, 'actual': True}
    if request.user.is_authenticated():
        adv_params.update({'user': request.user})
    else:
        adv_params.update({'session_id': request.session.session_key})
    return adv_params


def img_check_rights(request, pk, model):
    '''
    Checking rights for image delete/making cover
    '''
    try:
        if 'events' in request.get_full_path():
            return model.objects_all.get(
                pk=get_adv_params(request, pk).get('pk'), user=get_adv_params(request, pk).get('user'))
        else:
            return model.objects.get(**get_adv_params(request, pk))
    except model.DoesNotExist:
        return None


def advert_add(request, pk, blank=True, public=False):
    '''
    Add advert view
    '''
    advert = get_object_or_404(Advert,
                               blank=blank,
                               public=public,
                               **get_adv_params(request, pk))
    img_form = AdvertImageForm(advert=advert)
    img_form_from_url = AdvertImageUrlForm()
    images = AdvertImage.objects.filter(advert=advert)

    # if request.method == "GET":
    #     form = AddAdvertForm(instance=advert)
    form = AddAdvertForm(request.POST or None,
                         instance=advert,
                         location=request.location)

    if request.method == "POST":
        # Авторизованный/неавторизованный редактирует опубликованное
        if '_save' in request.POST and advert.public:
            if form.is_valid():
                obj = form.save()
                messages.success(request, _(u'Запись успешно обновлена.'))
                return HttpResponseRedirect(
                    reverse('catalog:advert_detail', args=(obj.id,)))

        # Авторизованный сохраняет/редактирует черновик
        elif all(['_draft' in request.POST,
                  advert.public is False,
                  request.user.is_authenticated()]):
            # form = AddAdvertForm(request.POST, instance=advert, draft=True)
            form.draft = True
            if form.is_valid():
                form.save()
                messages.success(request, _(u'Черновик успешно сохранен.'))
                return HttpResponseRedirect(reverse('cabinet:drafts'))

        # Кто-то публикует
        elif '_public' in request.POST:
            # form = AddAdvertForm(request.POST, instance=advert)
            if form.is_valid():
                obj = form.save()
                messages.success(request, _(u'Запись успешно опубликована.'))
                return HttpResponseRedirect(
                    reverse('catalog:advert_detail', args=(obj.id,)))
        else:
            return HttpResponseBadRequest()

    return render(request, 'add.html', {'advert': advert,
                                        'form': form,
                                        'img_form': img_form,
                                        'images': images,
                                        'cover_pk': advert.cover_id})


@login_required
def advert_edit(request, pk):
    '''
    Edit advert view
    '''
    return advert_add(request, pk, blank=False, public=True)


@login_required
def advert_delete(request, pk):
    """
    delete advert view
    """
    obj = get_object_or_404(Advert, pk=pk)
    if obj.user == request.user:
        AdvertImage.objects.filter(advert=obj).delete()
        obj.delete()
        # obj.save()
        messages.success(request, _(u'Объявление успешно удалено'))
    else:
        messages.error(request, _(u'У вас нет прав на это действие'))
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


def advert_new(request):
    '''
    Creating new advert or returns the existing
    '''
    if not request.session.session_key or not request.session.exists(request.session.session_key):
        request.session.create()

    params = {'hidden': False, 'public': False, 'blank': True}
    if request.user.is_authenticated():
        params.update({'user': request.user})
        if Advert.objects.filter(**params).exists():
            return HttpResponseRedirect(
                reverse(
                    'catalog:add',
                    args=(Advert.objects.get(**params).pk,)
                    ))
        params.update({'phone': request.user.phone})
    else:
        params.update({'session_id': request.session.session_key})
        try:
            return HttpResponseRedirect(
                reverse(
                    'catalog:add',
                    args=(Advert.objects.get(**params).pk, )
                    ))
        except Advert.DoesNotExist:
            pass
    return HttpResponseRedirect(
        reverse(
            'catalog:add',
            args=(Advert.objects.create(**params).pk, )
            ))


@require_POST
def img_add(request, pk, model):
    """
    Upload images placed in request.FILES
    and attaches them to given advert pk
    """
    advert = img_check_rights(request, pk, model)
    if not advert:
        message = u'Объявление не существует или неправильная сессия.'
        return HttpResponseBadRequest(
            json.dumps({'status': 'error', 'message': message}))
    status, answer = upload_images(request, advert.id)
    if status:
        return HttpResponse(
            json.dumps({'status': 'success', 'answer': answer}))
    else:
        return HttpResponse(
            json.dumps({'status': 'error', 'answer': answer}))


@require_POST
def img_url_add(request, pk, model):
    """
    Upload images from url
    and attaches them to given advert pk
    """
    advert = img_check_rights(request, pk, model)
    if not advert:
        message = _(u'Объявление не существует или неправильная сессия.')
        return HttpResponseBadRequest(
            json.dumps({'status': 'error', 'message': message}))
    status, answer = upload_url_images(request, advert.id)
    if status:
        return HttpResponse(
            json.dumps({'status': 'success', 'answer': answer}))
    else:
        return HttpResponse(
            json.dumps({'status': 'error', 'answer': answer}))


def upload_url_images(request, advert_id):
    """
    Takes in a request object and returns a list with information about
    image: url, thumbnail_url, delete_url, cover_url.

    Attaches images to the given advert.
    """

    model = EventImage if 'events' in request.get_full_path() else AdvertImage
    if request.method == 'POST':
        form_url = AdvertImageUrlForm(request.POST)
        answer_error = (False, form_url.errors.get('img_from_url'))
        if form_url.is_valid():
            cleaned_data = form_url.cleaned_data
            if not cleaned_data['img_from_url'].endswith(('jpeg', 'png', 'gif', 'jpg')):
                return answer_error
            img = model(advert_id=advert_id)
            img.save()
            try:
                result = urllib.urlretrieve(cleaned_data['img_from_url'])
                img.image.save(os.path.basename(cleaned_data['img_from_url']),
                               File(open(result[0])))
                img.save()

                thumbnail = get_thumbnail(
                    img.image, '75x75', crop='center', quality=99)
                answer = (True, {'url': img.image.url,
                                 'thumbnail_url': thumbnail.url,
                                 'delete_url': img.get_delete_url(),
                                 'cover_url': img.get_cover_url()})
            except IOError:
                return answer_error
        else:
            answer = (False, form_url.errors.get('img_from_url'))

        return answer


@require_GET
def img_control(request, pk, image_pk, action, model):
    """
    Delete or mark as cover with image and advert given primary keys.
    """
    message = _(u'Запрошенное изображение не найдено.')
    advert = img_check_rights(request, pk, model)
    img_model = EventImage if 'events' in request.get_full_path() else AdvertImage
    if not advert:
        return HttpResponseNotFound(
            json.dumps({'status': 'error', 'message': message}))

    try:
        if action == 'delete':
            img_model.objects.filter(pk=image_pk, advert=advert).delete()
        elif action == 'cover':
            advert.cover_id = img_model.objects.get(
                pk=image_pk, advert=advert).pk
            advert.save()
    except img_model.DoesNotExist:
        return HttpResponseNotFound(
            json.dumps({'status': 'error', 'message': message}))

    return HttpResponse(json.dumps({'status': 'success'}))


def upload_images(request, advert_id):
    """
    Takes in a request object and returns a list with information about
    image: url, thumbnail_url, delete_url, cover_url.

    Attaches images to the given advert.
    """
    form = EventImageForm if 'events' in request.get_full_path() else AdvertImageForm
    if request.method == 'POST':
        form = form(request.POST, request.FILES)
        if form.is_valid():
            img = form.save(advert_id=advert_id)
            if not img:
                return (False,
                        '<ul class="errorlist"><li>Максимальное количество \
                        изображений - %s шт.</li></ul>' % form.max_count)


            thumbnail = get_thumbnail(img.image, '75x75', crop='center',
                                      quality=99)
            answer = (True, {'url': img.image.url,
                             'thumbnail_url': thumbnail.url,
                             'delete_url': img.get_delete_url(),
                             'cover_url': img.get_cover_url()})
        else:
            answer = (False, form.errors['image'])
        return answer


def advert_notactual(request, pk):
    '''First chekicng rights
    then changing actual state
    '''
    try:
        adv = Advert.objects.get(
            blank=False,
            public=True,
            **get_adv_params(request, pk))
        adv.actual = False
        adv.save()
        messages.success(request, _(u'Запись успешно обновлена.'))
    except Advert.DoesNotExist:
        messages.error(request, _(u'Ошибка доступа.'))

    return redirect(reverse('cabinet:adverts'))
