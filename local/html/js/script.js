try{
    $.fn.cycle.defaults.autoSelector = '.slideshow';
}
catch(e){
    console.log(e.name);
}

$(document).ready(function(){
    /*Left menu*/
    $('.container aside.left ul.categories > li').mouseover(function(e){
        $(this).children('ul').slideDown(400);
        $(this).mouseleave(function(){
            $(this).children('ul').slideUp(400);
        });
    });

    $('.enter').click(function(e){
        e.preventDefault();
        $('.enter').fancybox({
            autoSize: true
        });
        $('.tabs li').removeClass('active');
        $('.tabs li a[href="#login"]').parent().addClass('active');

        $('.tabs_content > div').removeClass('opened');
        $('#login').addClass('opened');
    });

    $('.reg').click(function(e){
        e.preventDefault();
        $('.reg').fancybox({
            autoSize: true
        });
        $('.tabs li').removeClass('active');
        $('.tabs li a[href="#register"]').parent().addClass('active');

        $('.tabs_content > div').removeClass('opened');
        $('#register').addClass('opened');
    });

    $('.tabs li a').click(function(e){
        e.preventDefault();
        $('.tabs li').removeClass('active');
        $(this).parent().addClass('active');

        $('.tabs_content > div').removeClass('opened');
        $($(this).attr('href')).addClass('opened');
    });

    /*var params = {
        changedEl: "#type, #date_add, #state",
        scrollArrows: false
    }
    cuSel(params);
*/
    $("#slider-range" ).slider({
      range: true,
      min: 0,
      max: 500,
      values: [ 75, 300 ],
      slide: function( event, ui ) {
        $(".min_price").val(ui.values[0]);
        $(".max_price").val(ui.values[1]);
      }
    });

    $('.min_price').on("change",function(){
        var value = $('.min_price').val();
        $("#slider-range").slider( "option", "values", [value, $('.max_price').val()] );
    });

    $('.max_price').on("change",function(){
        var value = $('.max_price').val();
        $("#slider-range").slider( "option", "values", [$('.min_price').val(),value] );
    });

    $('.ad_main_img > a').fancybox();

    $('.ad_main_img .small_img a').click(function(e){
        e.preventDefault();
        var src = $(this).attr('href');
        $('.ad_main_img > a').addClass('visible');
        $('.ad_main_img > a[href="'+src+'"]').removeClass('visible');
    });

    $('.seller p.phone a').click(function(event) {
        event.preventDefault();
        $('.seller p.phone a').toggleClass('vis');
    });

    $('.seller p.mail a').click(function(event) {
        event.preventDefault();
        $('.seller p.mail a').toggleClass('vis');
    });

    $('.user_info .user_img a').fancybox();
    tinymce.init({
        selector:'#tiny',
        content_css : "/static/css/tiny_fix.css"
    });

});
