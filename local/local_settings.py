import os
from settings import *

DEBUG = True
TEMPLATE_DEBUG = DEBUG

MIDDLEWARE_CLASSES += (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

INSTALLED_APPS += (
    'debug_toolbar',
)

# DEBUG_TOOLBAR_CONFIG = {
#     'INTERCEPT_REDIRECTS': False,
# }

# For testing purposes
DATABASE_SUPPORTS_TRANSACTIONS = True

# Disable caching on local development environment
CACHE_BACKEND = 'dummy://'

#sorl
THUMBNAIL_DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'sandbox.kybervision@gmail.com'
EMAIL_HOST_PASSWORD = 'hortonCVT'
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = 'sandbox.kybervision@gmail.com'


# geoip LOCAL_IP_ALTERNATIVE
LOCAL_IP_ALTERNATIVE = '46.219.81.21'

# ignore some apps in local development
local_ignore = (
)
INSTALLED_APPS = ()

for item in INSTALLED_APPS:
    if not item in local_ignore:
        INSTALLED_APPS += item,

INTERNAL_IPS = ('127.0.0.1', 'localhost')
SITE_NAME = 'localhost:8000'

LOGGING = dict()
if DEBUG:
    # Show emails in the console during developement.
    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
