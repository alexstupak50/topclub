# -*- coding:utf-8 -*-
from django import forms
from catalog.models import Advert


class SearchForm(forms.Form):
    query = forms.CharField(required=True)
