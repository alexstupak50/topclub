# -*- coding:utf-8 -*-
from django.shortcuts import render
from django.template import RequestContext

from core.utils import paginate_objects
from catalog.index import complete_indexer as indexer
from search.forms import SearchForm


def search(request):

    page = request.GET.get('page')
    results = []
    form = SearchForm(request.GET or None)

    if request.GET and form.is_valid():
        query = ' OR '.join(map(lambda x: x.strip(',').strip(),
                                form.cleaned_data['query'].split(' ')),
                            )
        results = indexer.search(query).prefetch()

    results = precache_foreignkey(paginate_objects(results, 20, page=page),
                                  'cover')

    return render(request, 'search.html', {'results': results, 'form': form})


def precache_foreignkey(items, field):
    '''Return items with cached foreignkey objects '''
    field_id = field + '_id'
    ids, model = list(), None
    for obj in items:
        try:
            ids.append(getattr(obj.instance, field_id))
        except AttributeError:
            pass
        if model is None:
            model = obj.instance._meta.get_field_by_name(field)[0].rel.to
    if model is not None:
        cache = model.objects.in_bulk(ids)
        for obj in items:
            try:
                setattr(obj.instance,
                        field,
                        cache.get(getattr(obj.instance, field_id)))
            except AttributeError:
                pass
    return items
