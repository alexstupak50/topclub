Необходим Python=>2.7
### Зависимости ###
```bash
sudo apt-get purge openjdk*
wget -qO - http://packages.elasticsearch.org/GPG-KEY-elasticsearch | sudo apt-key add -
```

добавть в /etc/apt/sources.list

```bash
deb http://packages.elasticsearch.org/elasticsearch/1.2/debian stable main
```

устанавливаем необходимые пакеты
```bash
sudo add-apt-repository ppa:webupd8team/java

sudo apt-get update
sudo apt-get upgrade

sudo apt-get install python-pip python-dev build-essential git-core python-mysqldb libmysqlclient-dev zlib1g-dev nano g++ uuid-dev uwsgi uwsgi-plugin-python nginx-full redis-server elasticsearch oracle-java8-installer libfreetype6-dev libxml2-dev libxslt1-dev  mysql-server

sudo easy_install -U distribute
sudo pip install virtualenv
sudo pip install supervisor
```

### Создание необходимых дерикторий ###

```bash
mkdir ~/.virtualenvs
```

### Создаем окружение ###
```bash
virtualenv --no-site-packages ~/.virtualenvs/topclub
```
и активтруем его

```bash
source ~/.virtualenvs/topclub/bin/activate
```

### Установка xapian в окружении ###
```bash
export VENV=$VIRTUAL_ENV
mkdir $VENV/packages && cd $VENV/packages

wget http://oligarchy.co.uk/xapian/1.2.15/xapian-core-1.2.15.tar.gz
wget http://oligarchy.co.uk/xapian/1.2.15/xapian-bindings-1.2.15.tar.gz

tar xzvf xapian-core-1.2.15.tar.gz
tar xzvf xapian-bindings-1.2.15.tar.gz

cd $VENV/packages/xapian-core-1.2.15
./configure --prefix=$VENV && make && make install

export LD_LIBRARY_PATH=$VENV/lib

cd $VENV/packages/xapian-bindings-1.2.15
./configure --prefix=$VENV --with-python && make && make install
```

### Копируем проект ###
```bash
cd ~/
git clone git@bitbucket.org:freemansbitbucket/topclub.git

```

### Установка зависимостей проекта ###
```bash
cd ~/topclub
pip install -r requirements.txt
```
создаем папку для логов
```bash
mkdir logs
```
исправляем миграцию django-social-auth
```bash
rm ~/.virtualenvs/topclub/local/lib/python2.7/site-packages/social/apps/django_app/default/migrations/0001_initial.py
python manage.py schemamigration --initial social.apps.django_app.default
```
### Заходим в Mysql
sudo mysql -u root -p

### Создание базы данным mysql ###
```bash
CREATE DATABASE `topclub` CHARACTER SET utf8 COLLATE utf8_general_ci;
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER, LOCK TABLES, CREATE TEMPORARY TABLES ON `topclub`.* TO 'topclub'@'localhost' IDENTIFIED BY 'PASSWORD';
```

### Создание локальных настроек ###
```bash
nano ~/topclub/infoportal/local_settings.py
```
содержимое
```python
# -*- encoding: utf-8 -*-

from settings import *

DEBUG = True
THUMBNAIL_DEBUG = True
ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'topclub',
        'USER': 'topclub',
        'PASSWORD': 'PASSWORD',
        'HOST': '',
        'PORT': '',
        'TEST_MIRROR': 'default',
        'OPTIONS': {'init_command': 'SET storage_engine=INNODB;'}

    }
}


INTERNAL_IPS = ('127.0.0.1', 'localhost')
SITE_NAME = 'localhost:8000'

LOGGING = dict()

if DEBUG:
    # Show emails in the console during developement.
    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
```

### Выполнение команд проекта ###
инстализация дб
```bash
python manage.py syncdb
python manage.py migrate
```
загрузка гео данных
```bash
python manage.py geoip_update1
python manage.py gm_update
```

### Команды запуска проекта ###
http сервер
```bash
python manage.py runserver
```
celery 
```bash
python manage.py celeryd -l INFO -B -E
```
чат 
```bash
python manage.py chat_server

```
### Подгрузка таблиц с таймзонами (timezone tables) в mysql (проверено на Ubuntu)###
Создание базы данным mysql 
```bash
mysql_tzinfo_to_sql /usr/share/zoneinfo | mysql -u root -p mysql
mysql -u root -p -e "flush tables;" mysql
```
