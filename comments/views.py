# -*- coding: utf-8 -*-
import json
from django.utils.translation import ugettext_lazy as _
from django.contrib import messages
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string
# from django.utils import simplejson
from django.views.decorators.http import require_POST
from django.core.urlresolvers import resolve
from django.utils.six.moves.urllib.parse import urlparse
from django.utils.http import is_safe_url

from comments.forms import CommentForm
from comments.models import Comment
from core.utils import ajax_required

from sorl.thumbnail import get_thumbnail


@login_required
def comment_add(request):

    def comment_save(form, addr):
        comment = form.save(commit=False)
        comment.user_id = form.user_id
        comment.content_object = form.target_object
        comment.ip = addr
        comment.save()
        return comment

    def get_next(request, url):
        if not is_safe_url(url=url, host=request.get_host()):
            url = '/'
        return HttpResponseRedirect(url)

    next_param = 'next'
    next_page = None
    if (next_param in request.POST or next_param in request.GET):
        next_page = request.POST.get(next_param, request.GET.get(next_param))

    form = CommentForm(request.user, request.POST)

    if request.method == 'POST' and request.is_ajax():
        if form.is_valid():
            comment = comment_save(form, request.META['REMOTE_ADDR'])
            data = json.dumps({'success': True,
                               'user': comment.get_name(),
                               'date': comment.get_date(),
                               'text': comment.text,
                               'image': get_thumbnail(request.user.avatar,
                                                      '50x50',
                                                      crop='center',
                                                      quality=99).url})
        else:
            data = json.dumps({'form_errors': form.errors})
        return HttpResponse(data, mimetype='application/json')
    elif request.method == 'POST':
        if form.is_valid():
            comment_save(form, request.META['REMOTE_ADDR'])
            return get_next(request, next_page)
        else:
            view, args, kwargs = resolve(urlparse(next_page)[2])
            request.method = 'GET'
            kwargs.update({'request': request,
                           'comment_form': form})
            return view(*args, **kwargs)
    else:
        return get_next(request, next_page)


@login_required
def comment_delete(request, pk):
    """
        delete advert view
    """
    obj = get_object_or_404(Comment, pk=pk)
    if obj.user == request.user:
        obj.delete()
        messages.success(request, _(u'Комментарий успешно удалено'))
    else:
        messages.error(request, _(u'У вас нет прав на это действие'))

    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))