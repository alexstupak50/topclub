# -*- coding: utf-8 -*-
from django.contrib import admin
from core.utils import ForeignKeyLinksMetaclass
from django.utils.translation import ugettext_lazy as _
from comments.models import Comment


class CommentAdmin(admin.ModelAdmin):
    __metaclass__ = ForeignKeyLinksMetaclass

    # list_display
    list_per_page = 40
    list_display = ('__unicode__', 'ip', 'link_to_user',)
    list_display_titles = {'link_to_user': _(u'Пользователь')}
    list_select_related = ('user',)
    list_filter = ('hidden', )
    date_hierarchy = 'created'

    # detail
    readonly_fields = ('ip', 'created', 'user')
    fieldsets = (
        (None,
            {'fields': (('hidden', 'created'),
                        'ip',
                        'user',
                        'text', )}),
    )

admin.site.register(Comment, CommentAdmin)
