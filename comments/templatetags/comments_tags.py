# -*- coding: UTF-8 -*-
import re
from django import template
from django.contrib.contenttypes.models import ContentType
from django.utils.safestring import mark_safe

from comments.forms import CommentForm
from comments.models import Comment
from comments.utils import paginate_objects

register = template.Library()


@register.inclusion_tag('comment_form.html', takes_context=False)
def render_text_comments_form(target_obj, user, _next=None):
    """
    Returns html with a form for a given object

    Usage:

        {% render_text_comments_form entry user %}

    Where 'entry' is an object instance we want to comments on
    """
    return {'form': CommentForm(user, target_object=target_obj),
            'next': _next}


@register.inclusion_tag('comment_form.html', takes_context=False)
def render_exist_comments_form(form, _next=None):
    """
    Returns html with a form for a given object

    Usage:

        {% render_text_comments_form entry user %}

    Where 'entry' is an object instance we want to comments on
    """
    return {'form': form,
            'next': _next}


@register.inclusion_tag('comments_list.html', takes_context=False)
def render_comments_list(target_obj, page):
    """
    Returns html with form for a given object

    Usage:

        {% render_comments_list entry %}

    Where entry is an object instance we want to see comments of
    """
    comments = Comment.objects.filter(
        content_type=ContentType.objects.get_for_model(target_obj).pk,
        object_id=target_obj.pk,
        hidden=False).select_related('user')
    comments = paginate_objects(comments, 10, page=page)
    return {'comments': comments}


@register.inclusion_tag('comments_pagination.html', takes_context=False)
def get_paginator(page_obj, adjacent_pages=4, page_key='page'):
    # обработка пагинации
    current_page = page_obj.number
    number_of_pages = page_obj.paginator.num_pages

    startPage = current_page - adjacent_pages
    if startPage <= 2:
        startPage = 1

    endPage = current_page + adjacent_pages
    if endPage >= number_of_pages - 1:
        endPage = number_of_pages

    page_numbers = [n for n in range(startPage, endPage + 1) if 0 < n <= number_of_pages]

    return {'page_key': page_key,
            'page_obj': page_obj,
            'page_numbers': page_numbers,
            'show_first': startPage > 1,
            'show_last': endPage < number_of_pages
            }
