# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic

from cabinet.models import UserProfile as User
from core.utils import UCrypto, mat_validator


class Comment(models.Model):

    class Meta:
        verbose_name = u'Комментарий'
        verbose_name_plural = u'Комментарии'
        get_latest_by = 'created'

    ip = models.GenericIPAddressField(u'IP address', unpack_ipv4=True, blank=True, null=True)
    created = models.DateTimeField(u'дата создания', auto_now_add=True)
    hidden = models.BooleanField(u'скрыть', default=False, help_text=u'Будет скрыто, если отмечено')

    user = models.ForeignKey(User, verbose_name=u'Пользователь')
    text = models.TextField(u'сообщение', validators=[mat_validator])

    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey('content_type', 'object_id')

    def get_name(self):
        return self.user.get_full_name()

    def get_date(self):
        return self.created.strftime('%d %B %Y %H:%M')

    def __unicode__(self):
        return self.get_date()
