from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'add/$', 'comments.views.comment_add', name='comment_add'),
    url(r'delete/(?P<pk>\d+)/$', 'comments.views.comment_delete', name='comment_delete'),
)
