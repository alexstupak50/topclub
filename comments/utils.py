from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def paginate_objects(obj_query, items_number, page=1):
    # paginate
    obj_pages = Paginator(obj_query, items_number)

    # paginate object
    try:
        result = obj_pages.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        result = obj_pages.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        result = obj_pages.page(obj_pages.num_pages)
    return result
