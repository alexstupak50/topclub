# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.models import ContentType

from comments.models import Comment
from core.utils import UCrypto


class CommentForm(forms.ModelForm):
    key = forms.CharField(widget=forms.HiddenInput)

    class Meta:
        model = Comment
        # widgets = {'key': forms.HiddenInput}
        exclude = ('ip', 'created', 'hidden', 'user', 'content_type', 'object_id')

    _error_messages = {
        'invalid_key': _(u'Неверный ключ записи. Попробуйте еще раз.')
        }

    def __init__(self, user, data=None, target_object=None, initial=None):
        super(CommentForm, self).__init__(data=data, initial=initial)
        self.user_id = user.pk
        if target_object:
            key = UCrypto(
                ContentType.objects.get_for_model(target_object).pk,
                target_object.pk,
                self.user_id,
            ).encode()
            self.fields['key'].initial = key

    def clean_key(self):
        key = UCrypto(self.cleaned_data.get('key')).decode().split(',')
        try:
            if not all([len(key) == 3, self.user_id == int(key[2])]):
                raise KeyError
            ctype = ContentType.objects.get_for_id(int(key[0]))
            target_object = ctype.get_object_for_this_type(id=int(key[1]))
        except (ContentType.DoesNotExist, KeyError):
            raise forms.ValidationError(self._error_messages['invalid_key'])
        self.target_object = target_object
        return key
