function image_del(selector){
    var obj;
    $(selector).on('click', function(e){
        e.preventDefault();
        obj = $(this);
        $.ajax({
            url: obj.attr('href'),
            type: 'GET',
            dataType: 'json',
            success: function(data){
                if(data["status"] == 'success')
                    obj.parent().remove();
            }
        })
        .done(function() {
                    input_status('#id_image', '.preview');
                });
    });
}

function set_main(selector){
    $(selector).click(function(e){
        e.preventDefault();
        $('.preview').removeClass('active');
        var obj = $(this);
        $.ajax({
            url: obj.attr('href'),
            type: 'GET',
            dataType: 'json',
            success: function(data){
                if(data["status"] == 'success')
                    obj.parent().addClass('active');
            }
        });
    });
}

function input_status(input, selector){
    if ($(selector).length >= 8 ) {
        $(input).prop( "disabled", true );
    } else if ($(selector).length < 8) {
        $(input).prop( "disabled", false );
    }
}

$(document).ready(function () {
    var files, upl_img;
    var input = $('#id_image');
    input.on('change', function(event){
        $('.img_errors').html('')
        input.prop( "disabled", true );
        count = $('.preview').length;
        files = event.target.files;
        upl_img = files.length;
        if ( count + upl_img > 8){
            $('#warning').html('<p>Максимальное количество изображений для обьявления - 8 шт.</p>').fancybox().click();
            input.val('');
            input_status('#id_image', '.preview');
        } else {
            var csrf = $('input[name=csrfmiddlewaretoken]').val();
            $('.img_previews').prepend($('<div class="loader"><img src="/static/images/ajax-loader2.gif"></div>'));
        }

        $.each(files, function(key, value){
            //Проверка есть ли входной файл картинкой
            if( !value.type.indexOf('image/') ){
                var data = new FormData();
                data.append('csrfmiddlewaretoken', csrf);
                data.append('image', value);
                $.ajax({
                    url: input.attr('data-post-url'),
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                    processData: false,
                    contentType: false,
                    success: function(data){
                        if(data['status'] == 'success'){
                            input.val('');
                            $('.img_previews').append($('<div class="preview"><img src="' + data['answer'].thumbnail_url + '"><a href="' + data['answer'].delete_url + '" class="preview_del"></a><a href="' + data['answer'].cover_url + '" class="cover_url">Сделать обложкой</a></div>'));
                            image_del('.preview a.preview_del');
                            set_main('.cover_url');
                        } else {
                            input.val('');
                            $('.img_errors').html(data['answer']);
                        }
                    }
                })
                .done(function() {
                    if ( key == upl_img - 1)
                        $('.loader').remove();
                    input_status('#id_image', '.preview');
                });
            }
        });
    });

    input_status('#id_image', '.preview');
    image_del('.preview a.preview_del');
    set_main('.cover_url');
//-----------------------------------
 $('#photo_from_url').change(function(){
        $.ajax({
          type: 'POST',
          dataType: 'json',
          data: $('#photo_from_url').serialize(),
          url: "{% url 'catalog:img_add' pk={{advert.id}} %}",
          beforeSubmit:function(){
            $('.img_previews').prepend($('<div class="loader"><img src="/static/images/ajax-loader2.gif"></div>'));
            },

 success: function(data) {
        if(data['status'] == 'success'){
            $('.img_previews .loader').hide(); //убираем картинку лоадер
            $('.img_previews').append($('<div class="preview"><img src="' + data['answer'].thumbnail_url + '"><a href="' + data['answer'].delete_url + '" class="preview_del"></a><a href="' + data['answer'].cover_url + '" class="cover_url">{% trans 'Сделать обложкой' %}</a></div>'));
           image_del('.preview a.preview_del');
           set_main('.cover_url');
        } else{
            $('.img_errors').html(data['answer']);
            }

},
          complete: function(data){
            $('#photo_from_url').val('');
                }
        });

        return false;

 });
//-----------------------------------
});
