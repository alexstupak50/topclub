$(document).ready(function() {
    $(document.body).on('submit', '#comment-form', function() {
        var form = this;
        var url = form.action;
        if ($(this).attr('data-id')) {
            url = url + '?parent=' + $(this).attr('data-id');
        }
        $(form).ajaxSubmit({
            url: url,
            type: form.method,
            data: $(form).serialize(),
            success: function (data) {
                if (data.valid) {
                    if ($(form).attr('data-id')) {
                        $('#comment-' + $(form).attr('data-id')).append(data.html);
                    } else {
                        $(form).after(data.html);
                    }
                    $('form#comment-form > #id_message').focusout();
                    $('form#comment-form > #id_message').val('');
                    $(form).attr('data-id', '');

                } else {
                    var parent = $(form).parent();
                    $(parent).html(data);
                }
            }
        });
        return false;
    });
    $(document.body).on('click', '.re-message', function() {
        $('form#comment-form').attr('data-id', $(this).attr('data-id'));
        $('form#comment-form > #id_message').focus();
        return false;
    });

    $(document.body).on('click', '#get-messages', function() {
        var el = this;
        $.ajax({
            type: "GET",
            url: $(el).attr('href') + '?page=' + $(this).attr('page'),
            complete: function(res) {
                $('#comments').append(res.responseText);
                $(el).attr('page', $(el).attr('page') + 1);
            }
        });
        return false;
    });
});
