try{
    $.fn.cycle.defaults.autoSelector = '.slideshow';
}
catch(e){
    console.log(e.name);
}

function bbcode_to_html(str){
    $format_search = [ /\[a\](.*?)\[\/a\]/ig, /\[t\](.*?)\[\/t\]/ig ];
    $format_replace = [ '<p class="author">$1</p>', '<p class="quote_text">$1</p>' ];
    for (var i =0; i<$format_search.length; i++)
      str = str.replace($format_search[i], $format_replace[i]);
    return str;
}

function save_cookie(){
    $('.save_cookie').click(function(e){
        e.preventDefault();
        var city_id = $('#id_geo_city').attr('data_id');
        $('.no_city').hide();
        if (!city_id || $('#id_geo_city').val().length == 0){
            $('.no_city').html('Выберите город!');
            $('.no_city').show();
        } else {
            $.cookie('location', '1');
            $.cookie('geoip_location_id', city_id);
            $.fancybox.close();
            $('.region a').html($('#id_geo_city').val());
        }
    });
    $('.cookie_cancel').click(function(e){
        e.preventDefault();
        if(!$.cookie('location'))
            $.cookie('location', 'None');
        $.fancybox.close();
    });
}

function toggle_button(elem, up, down) {
    if (up) {
        $(elem).removeClass('fa-minus-circle');
        $(elem).addClass('fa-plus-circle')
    } else if (down) {
        $(elem).removeClass('fa-plus-circle');
        $(elem).addClass('fa-minus-circle');
    } else if (!down && !up ) {
        $(elem).toggleClass('fa-plus-circle');
        $(elem).toggleClass('fa-minus-circle');
    }
}

$(document).ready(function(){

    if(!$.cookie('location')){
        $.fancybox({
            href: '#region',
            autoSize: true,
            minHeight: 180,
            beforeClose: function(){
                if(!$.cookie('location'))
                    $.cookie('location', 'None');
            }
        });
        save_cookie();
    }

    $('.yes').click(function (event) {
        event.preventDefault();
        $.cookie('location', '1');
        $.fancybox.close();
    });

    $('.no').click(function (event) {
        event.preventDefault();
        $('.geo').removeClass('visible');
        $('.geo_form').addClass('visible');
        save_cookie();
        $.fancybox.update();
    });

    /*Left menu*/

        //$('.container aside.left ul.categories > li').mouseover(function(e){
        //$(this).children('ul').slideDown(400);
        //$(this).mouseleave(function(){
        //    $(this).children('ul').slideUp(400);
        //});
        //
    //


    $('.container .left h3 i').click(function () {
        var target = $('.categories > li > ul');
        if ($(this).hasClass('fa-plus-circle')) {
            target.slideDown(400);
            toggle_button('.categories i', false, true);
        } else {
            target.slideUp(400);
            toggle_button('.categories i', true, false);
        }
        toggle_button(this)
    });

    $('.categories i').click(function () {
        $(this).siblings('ul').slideToggle(400);
        toggle_button(this);
        if ($('.categories ul:visible').length === 0) {
            toggle_button('.container .left h3 i');
        }
        if ($('.categories ul:hidden').length === 0) {
            toggle_button('.container .left h3 i');
        }
    });

    $('.enter').click(function(e){
        e.preventDefault();
        $('.enter').fancybox({
            autoSize: true
        });
        $('.tabs li').removeClass('active');
        $('.tabs li a[href="#login"]').parent().addClass('active');

        $('.tabs_content > div').removeClass('opened');
        $('#login').addClass('opened');
    });

    $('.reg').click(function(e){
        e.preventDefault();
        $('.reg').fancybox({
            autoSize: true
        });
        $('.tabs li').removeClass('active');
        $('.tabs li a[href="#register"]').parent().addClass('active');

        $('.tabs_content > div').removeClass('opened');
        $('#register').addClass('opened');
    });

    $('.tabs li a').click(function(e){
        e.preventDefault();
        $('.tabs li').removeClass('active');
        $(this).parent().addClass('active');

        $('.tabs_content > div').removeClass('opened');
        $($(this).attr('href')).addClass('opened');
    });

    /*var params = {
        changedEl: "#type, #date_add, #state",
        scrollArrows: false
    }
    cuSel(params);
    */

    var min_price = parseInt($("#id_price_min").attr('data-min'));
    var max_price = parseInt($("#id_price_max").attr('data-max'));
    setTimeout(function(){
    $("#slider-range" ).slider({
      range: true,
      min: min_price,
      max: max_price,
      values: [ min_price, max_price ],
      slide: function( event, ui ) {
        $("#id_price_min").val(ui.values[0]);
        $("#id_price_max").val(ui.values[1]);
      }
    });
    },100);

    $('#id_price_min').on("change",function(){
        var value = $('#id_price_min').val();
        $("#slider-range").slider( "option", "values", [value, $('#id_price_max').val()] );
    });

    $('#id_price_max').on("change",function(){
        var value = $('#id_price_max').val();
        $("#slider-range").slider( "option", "values", [$('#id_price_min').val(),value] );
    });

    $('.ad_main_img > a').fancybox();

    $('.ad_main_img .small_img a').click(function(e){
        e.preventDefault();
        var src = $(this).attr('href');
        $('.ad_main_img > a').addClass('visible');
        $('.ad_main_img > a[href="'+src+'"]').removeClass('visible');
    });

    $('.seller p.phone a').click(function(event) {
        event.preventDefault();
        $('.seller p.phone a').toggleClass('vis');
    });

    $('.seller p.mail a').click(function(event) {
        event.preventDefault();
        $('.seller p.mail a').toggleClass('vis');
    });

    $('.user_info .user_img a').fancybox();

    tinymce.init({selector:'#tiny', content_css : "/static/css/tiny_fix.css"});

    var loader = {
        obj: '.ajax-loader',
        show: function () {
          var obj = $(this.obj);
          obj.css({
            height: obj.parent().height(),
            width: obj.parent().width()
          });
          obj.show();
        },
        hide: function () {
          $(this.obj).hide();
        }
    }


    // var loader = $('#ajax_loader');
    // loader.xshow = function() {
    //     loader.css({
    //         height: loader.parent().height(),
    //         width: loader.parent().width()
    //     });
    //     loader.show();
    // }

    $('#popup-registration-form').submit(function(e){
        e.preventDefault();
        if (passcheck(true)) {
            $('.error_msg').remove();
            $('#popup-registration-form p.success').html('');
            loader.show();

            $.ajax({
                url : $(this).attr("action"),
                type: $(this).attr("method"),
                data : $(this).serializeArray(),
                success:function(data) {
                    if (data.success){
                        $('#popup-registration-form p.success').html('Вы успешно зарегистрированы!');
                        setTimeout(function(){window.location = "/";}, 500);
                    }
                    else{
                        $.each(data.form_errors, function(i, val) {
                            if (i == '__all__')
                                $('#popup-registration-form').before('<span class="error_msg">' + val + '</span>');
                            $('#popup-registration-form #id_' + i).after('<span class="error_msg">' + val + '</span>');
                        });
                        if (data.new_cptch_image) {
                            $('#id_captcha_0').val(data.new_cptch_key);
                            $('.captcha').attr("src", data.new_cptch_image);
                        }
                        if (data.form_errors.captcha){
                            $('#popup-registration-form #id_captcha_1').after('<span class="error_msg">' + data.form_errors.captcha + '</span>');
                        }
                    }
                    loader.hide();
                }
            });
        } else {
            pswd_info.show();
        }
    });


    $('#popup-login-form').submit(function(e){
            e.preventDefault();

            $('.error_msg').remove();
            $('#popup-login-form p.success').html('');
            loader.show();

            $.ajax({
                url : $(this).attr("action"),
                type: $(this).attr("method"),
                data : $(this).serializeArray(),
                success:function(data) {
                    if (data.success) {
                        // $('span.login').remove();
                        // $('span.register').html('<article>Здравствуйте ' + data.username + ' <a href="/logout/">Выйти</a></article>');
                        // $('#window_login_form').jqmHide();
                        $('.modal_login, .logout').toggleClass('visible');
                        $('#popup-login-form p.success').html('Вы успешно авторизированны!');

                        setTimeout(function(){
                            $('#popup-login-form p.success').html('');
                            $.fancybox.close();
                            location.reload();
                        }, 500);
                    }
                    else{
                        $.each(data.form_errors, function(i, val) {
                            if (i == '__all__')
                                $('#popup-login-form').before('<span class="error_msg">' + val + '</span>');
                            $('#popup-login-form #id_' + i).after('<span class="error_msg">' + val + '</span>');
                        });
                    }
                    loader.hide();
                }
            });
        });


    //  $('#popup-feedback').submit(function(e){
    //     e.preventDefault();
    //     $('#popup-feedback .error_msg').remove();
    //     $.ajax({
    //         url : $(this).attr("action"),
    //         type: $(this).attr("method"),
    //         data : $(this).serializeArray(),
    //         success:function(data) {
    //             if (data.success) {
    //                 $('.success').html('Ð”Ð°Ð½Ð½Ñ‹Ðµ ÑÐ¾Ñ…Ñ€Ð°Ð½ÐµÐ½Ñ‹');
    //             }
    //             $.each(data.form_errors, function(i, val) {
    //                 $('#popup-feedback #id_' + i).after('<span class="error_msg">' + val + '</span>');
    //             });
    //         }
    //     });
    // });

    $('#popup-reminder-form').submit(function(e){
        e.preventDefault();
        $('.error_msg').remove();
        $('#popup-reminder-form p.success').html('');
        loader.show();

        $.ajax({
            url : $(this).attr("action"),
            type: $(this).attr("method"),
            data : $(this).serializeArray(),
            success:function(data) {
                if (data.success) {
                    $('#popup-reminder-form p.success').html('Письмо с инструкциями выслано успешно!');
                }
                else{
                    $.each(data.form_errors, function(i, val) {
                        if (i == '__all__')
                            $('#popup-reminder-form').before('<span class="error_msg">' + val + '</span>');
                        $('#popup-reminder-form #id_' + i).after('<span class="error_msg">' + val + '</span>');
                    });
                }
                loader.hide();
            }
        });
    });

    var el_length = $('#length')
    var el_number = $('#number')
    // var el_capital = $('#capital')
    var el_letter = $('#letter')
    var el_mismatch = $('#mismatch')
    var pswd_info = $('#pswd_info')
    var password1 = $('#id_password1')
    var password2 = $('#id_password2')

    function passcheck(form) {
        var pswd = password1.val();
        var pswd2 = password2.val();
        //validate the length
        if ( pswd.length < 6 ) {
            el_length.removeClass('valid').addClass('invalid');
            el_length.status = false
        } else {
            el_length.removeClass('invalid').addClass('valid');
            el_length.status = true
        }

        //validate number
        if ( pswd.match(/\d/) ) {
            el_number.removeClass('invalid').addClass('valid');
            el_number.status = true
        } else {
            el_number.removeClass('valid').addClass('invalid');
            el_number.status = false
        }

        //validate capital letter
        // if ( pswd.match(/[A-Z]/) ) {
        //     el_capital.removeClass('invalid').addClass('valid');
        //     el_capital.status = true
        // } else {
        //     el_capital.removeClass('valid').addClass('invalid');
        //     el_capital.status = false
        // }

        //validate letter
        if ( pswd.match(/[a-z]/) ) {
            el_letter.removeClass('invalid').addClass('valid');
            el_letter.status = true
        } else {
            el_letter.removeClass('valid').addClass('invalid');
            el_letter.status = false
        }

        //validate mismatch
        if ( pswd2 == pswd ) {
            el_mismatch.removeClass('invalid').addClass('valid');
            el_mismatch.status = true
        } else {
            el_mismatch.removeClass('valid').addClass('invalid');
            el_mismatch.status = false
        }

        // show or hide results
        if (el_length.status && el_letter.status && el_number.status && el_mismatch.status) {
            pswd_info.fadeOut();
            return true;
        } else {
            if (password1.is(":focus") || password2.is(":focus") || form) {
                pswd_info.show();
                if (form) {
                    password1.focus();
                };
            } else if (form) {
                    pswd_info.show();
                    password1.focus();
            } else {
                pswd_info.hide();
            }
            return false;
        }

    }

    password1.keyup(function() {
        passcheck();
    }).focus(function() {
        passcheck();
    }).blur(function() {
        passcheck();
    });

    password2.keyup(function() {
        passcheck();
    }).focus(function() {
        passcheck();
    }).blur(function() {
        passcheck();
    });

    $('a[href=#region]').fancybox({
        autoResize: true,
        afterLoad: save_cookie,
        beforeClose: function(){
            if(!$.cookie('location'))
                $.cookie('location', 'None');
        }
    });

    $('.filter select').wrap('<div class="select_wraper"></div>');

    $('.comment_add_button').click(function(event){
        event.preventDefault();
        $.ajax({
            url: $('#comment-form').attr('action'),
            type: 'POST',
            data: $('#comment-form').serializeArray(),
            success: function(data){
                if (data.success === true) {
                    var comment = '<div class="user_comment"><div class="user_image"><img src="' + data.image + '"></div>\
                                   <div class="comment_info"><p class="us_info">' + data.user + ', ' + data.date + '</p><p>' + bbcode_to_html(data.text) + '</p></div><a href="#" class="quote">Цитировать</a><div class="clear"></div></div>'
                    $('.user_comments').append(comment);
                }
            }
        }).always(function() {
            $('#comment-form #id_text').val( '' );
        });
    });


    $('.quote').click(function(event){
        event.preventDefault();
        var object = $(this).parents('.user_comment').clone();
        object.find('.author').remove();
        object.find('.quote_text').remove();
        var quote_author = '[a]' + object.find('.us_info').text() + '[/a]';
        var quote_text = '[t]' + $.trim( object.find('.comment_body').text() ) + '[/t]';

        $('#comment-form #id_text').val(quote_author + '\n' + quote_text + '\n');
        $('html, body').animate({
            scrollTop: $("#comment-form").offset().top
        }, 500);
    });

    try{
        $('#id_date_start, #id_date_end, .add_event_form #id_date').pickadate({
            format: 'dd/mm/yyyy',
        });

        $('.add_event_form #id_time').pickatime({
            format: 'HH:i',
            interval: 15
        });
    }
    catch(e){
        console.log(e);
    }

    $('.event_filter_form #id_category, .add_event_form #id_category, .event_filter_form #id_region, .event_filter_form #id_city').wrap('<div class="select_wraper">');


    $('.ptomt_delete, .delete-account').click(function (e) {
        e.preventDefault();
        var $this = $(this);
        $.fancybox({
            href: '#warning2',
            closeBtn: false,
            keys: {
                close: false
            },
            helpers: {
                overlay : {
                    closeClick : false
                }
            }
        });

        $('body').on('click', '.modal-warning-close', function(e){
            e.preventDefault();
            $.fancybox.close();
        });
        $('.modal-warning-conf').click(function(e){
            e.preventDefault();
            var url;
            if ($this.hasClass('delete_m')){
                 url = $this.attr('data-url');
            } else {
                url = $this.attr('href');
            }
            window.location.href = url;
            $.fancybox.close();
        });
    });
    //автокомплит при выборе региона в шапке сайта
    $( "#id_geo_city" ).autocomplete({
        source: function(request, response) {
            $.getJSON("/geo/city/autocomplete/", {input: request.term}, function (data) {
                if (data.length == 0) {
                    $('.no_city').html('Такого города не существует или не обслуживается').show();
                } else {
                    $('.no_city').hide();
                }
                response(data);
            });
        },
        minLength: 1,
        focus: function(event, ui) {
            event.preventDefault();
            $(this).attr('data_id', ui.item.value);
            $(this).val(ui.item.label);
        },
        select: function (event, ui) {
            event.preventDefault();
            console.log($(this), ui.item.value);
            $(this).attr('data_id', ui.item.value);
            $(this).val(ui.item.label);
        }
    });
    $('#id_region').change(function () {
        if ($(this).val() !== '') {
            $('#id_city').removeAttr('disabled');
        } else {
            $('#id_city').attr('disabled', true);
        }
    });
    $('a#like, a#dislike').click(function (e) {
        e.preventDefault();
        var object = $(this);
        $.getJSON(object.attr('href'), function(data){
            if (data.valid) {
                var parent = object.parent();
                console.log(data);
                parent.find('.count-like').hide().html(data.likes).fadeIn(1000);
                parent.find('.count-dislike').hide().html(data.dislike).fadeIn(1000);
            }
            //object.siblings('.fa-check').show();
            //object.siblings('.fa-check').fadeOut(1000);
        });
    });
});
