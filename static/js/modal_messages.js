$(document).ready(function(){
    // $.fancybox.open('#warning');
    $.fancybox({
            href: '#warning',
            closeBtn: false,
            keys: {
                close: false
            },
            helpers: {
                overlay : {
                    closeClick : false
                }
            }
        });

        $('.modal-warning-close').click(function(e){
            e.preventDefault();
            $.fancybox.close();
        });
});