ymaps.ready(function(){
    var myMap, myPlacemark;

    if ( $('#id_lat').val() != "" && $('#id_lng').val() != "" ){
        var lat = $('#id_lat').val();
        var lng = $('#id_lng').val();

        myMap = new ymaps.Map('map', {
            center: [lat, lng],
            zoom: 12,
            behaviors: ["default", "scrollZoom"]
        });

        myPlacemark = new ymaps.Placemark([lat, lng]);
        myMap.geoObjects.add(myPlacemark);

        myMap.events.add('click', function (e) {
            if(myPlacemark)
                myMap.geoObjects.remove(myPlacemark);

            var position = e.get('coordPosition');

            var lat = position[0];
            var lng = position[1];

            $('#id_lat').val(lat);
            $('#id_lng').val(lng);

            myPlacemark = new ymaps.Placemark([lat, lng], { 
                hintContent: 'Москва!', 
                balloonContent: 'Столица России' 
            });
            myMap.geoObjects.add(myPlacemark);
        });
    }

    else{

        if( $('#id_geo_init').val() ){
            ymaps.geocode($('#id_geo_init').val()).then(function (res) {
                myMap = new ymaps.Map('map', {
                    center: [53.9, 27.56667],
                    zoom : 12,
                    behaviors: ["default", "scrollZoom"]
                });
                myMap.controls.add('zoomControl');
                
                myMap.events.add('click', function (e) {
                    if(myPlacemark)
                        myMap.geoObjects.remove(myPlacemark);

                    var position = e.get('coordPosition');

                    var lat = position[0];
                    var lng = position[1];

                    $('#id_lat').val(lat);
                    $('#id_lng').val(lng);

                    myPlacemark = new ymaps.Placemark([lat, lng]);
                    myMap.geoObjects.add(myPlacemark);
                });
            });
        }

    }
});

$(document).ready(function () {
    $('#add fieldset select').wrap('<div class="select_wraper"></div>')
});