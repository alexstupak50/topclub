// добавлнеие к ивентам и обьявлениям изображений
$(function () {
    $('.modal-warning-close').click(function (e) {
        e.preventDefault();
        // .close() почемуто не работает, такой плохой хак приходиться делтьа
        $('.fancybox-overlay, .fancybox-wrap').hide();
        $('html').removeClass('fancybox-margin').removeClass('fancybox-lock');
        return false;
    });
});
function image_del(selector){
    var obj;
    $(selector).on('click', function(e){
        e.preventDefault();
        obj = $(this);
        $.ajax({
            url: obj.attr('href'),
            type: 'GET',
            dataType: 'json',
            success: function(data){
                if(data["status"] == 'success')
                    obj.parent().remove();
            }
        })
            .done(function() {
                input_status('#id_image', '.preview');
            });
    });
}

function set_main(selector){
    $(selector).click(function(e){
        e.preventDefault();
        $('.preview').removeClass('active');
        var obj = $(this);
        $.ajax({
            url: obj.attr('href'),
            type: 'GET',
            dataType: 'json',
            success: function(data){
                if(data["status"] == 'success')
                    obj.parent().addClass('active');
            }
        });
    });
}

function input_status(input, selector){
    if ($(selector).length >= 8 ) {
        $(input).prop( "disabled", true );
    } else if ($(selector).length < 8) {
        $(input).prop( "disabled", false );
    }
}

function img_upload(param) {
    param = typeof param !== 'undefined' ? param : {};
    param.max_img = typeof param.max_img !== 'undefined' ? param.max_img : 8;
    param.max_img_text = typeof param.max_img_text !== 'undefined' ? param.max_img_text : 'Максимальное количество изображений для обьявления - 8 шт.';
    param.main_img_text = typeof param.main_img_text !== 'undefined' ? param.main_img_text : 'Сделать обложкой';

    var files, upl_img;
    var input = $('#id_image');
    input.on('change', function(event){
        $('.img_errors').html('')
        input.prop( "disabled", true );
        count = $('.preview').length;
        files = event.target.files;
        upl_img = files.length;
        if ( count + upl_img > param.max_img){
            $('#warning p').html(param.max_img_text)
            $('#warning').fancybox().click();
            input.val('');
            input_status('#id_image', '.preview');
        } else {
            var csrf = $('input[name=csrfmiddlewaretoken]').val();
            $('.img_previews').prepend($('<div class="loader"><img src="/static/images/ajax-loader2.gif"></div>'));
        }

        $.each(files, function(key, value){
            //Проверка есть ли входной файл картинкой
            if( !value.type.indexOf('image/') ){
                var data = new FormData();
                data.append('csrfmiddlewaretoken', csrf);
                data.append('image', value);
                $.ajax({
                    url: input.attr('data-post-url'),
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                    processData: false,
                    contentType: false,
                    success: function(data){
                        if(data['status'] == 'success'){
                            input.val('');
                            $('.img_previews').append($('<div class="preview"><img src="' + data['answer'].thumbnail_url + '"><a href="' + data['answer'].delete_url + '" class="preview_del"></a><a href="' + data['answer'].cover_url + '" class="cover_url">' + param.main_img_text + '</a></div>'));
                            image_del('.preview a.preview_del');
                            set_main('.cover_url');
                        } else {
                            input.val('');
                            $('.img_errors').html(data['answer']);
                        }
                    },
                    error: function () {
                        $('.loader').remove();
                    }
                })
                    .done(function() {
                        if ( key == upl_img - 1)
                            $('.loader').remove();
                        input_status('#id_image', '.preview');
                    });
            }
        });
    });

    input_status('#id_image', '.preview');
    image_del('.preview a.preview_del');
    set_main('.cover_url');


    $('#id_img_from_url').on('keypress', function(event){
        var key = event.which;
        if (key == 13) {
            if ( $('.preview').length >= param.max_img){
                $('#warning p').html(param.max_img_text)
                $('#warning').fancybox().click();
                $('#id_img_from_url').val('');
            } else {
                $.ajax({
                    type: 'POST',
                    data: $('#id_img_from_url, [name=csrfmiddlewaretoken]').serialize(),
                    url: param.url,

                    beforeSend: function(){
                        $('.img_previews').prepend($('<div class="loader"><img src="/static/images/ajax-loader2.gif"></div>'));
                    },
                    success: function(data) {
                        data=jQuery.parseJSON(data);
                        if(data.status == 'success'){
                            $('.img_previews .loader').hide();
                            $('.img_previews').append($('<div class="preview"><img src="' + data['answer'].thumbnail_url + '"><a href="' + data['answer'].delete_url + '" class="preview_del"></a><a href="' + data['answer'].cover_url + '" class="cover_url">Сделать обложкой</a></div>'));
                            image_del('.preview a.preview_del');
                            set_main('.cover_url');
                        } else {
                            $('.img_previews .loader').hide();
                            alert('Не верная ссылка');
                            $('.img_errors').html(data['answer']);
                        }
                    },

                    complete: function(data){
                        $('#id_img_from_url').val('');
                    }
                });
            }
            return false;
        }

    });
}