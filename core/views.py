# -*- coding:utf-8 -*-
from django.shortcuts import render, get_object_or_404, redirect
from django.conf import settings
from django.utils.functional import SimpleLazyObject

from django.core.exceptions import ObjectDoesNotExist
from django.core.cache import cache
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.decorators import login_required
from django import http


from django_geoip.base import storage_class
from django_geoip.utils import get_class
from django_geoip.models import IpRange
from django_geoip.base import Locator

from catalog.models import (Catalog,
                            Category,
                            Advert,
                            )
from core.models import LocRegion, LocCity
from event.models import Like, TYPE_LIKE, Event
from .utils import JsonResponse
from random import sample
import json


def get_regions(request):
    if request.is_ajax():
        regions = list(LocRegion.objects.values('title_ru',
                                               'pk').order_by('title_ru'))
        return http.HttpResponse(json.dumps(regions),
                                 mimetype='application/json')
    raise http.Http404


def city_autocomplete(request):
    if request.is_ajax():
        data = {}
        if request.GET.get('input'):
            cities = LocCity.objects.filter(title_ru__icontains=request.GET.get('input')).values('title_ru', 'pk', 'region__title_ru').order_by('title_ru')
            data = [{'label': '%s (%s)' % (city['title_ru'], city['region__title_ru']), 'value': city['pk']} for city in cities]
        return http.HttpResponse(json.dumps(data), mimetype='application/json')
    raise http.Http404


def get_cities(request, region_pk):
    if request.is_ajax():
        cities = list(
            LocCity.objects.filter(region_id=region_pk).values(
                'title_ru',
                'pk').order_by('title_ru'))
        return http.HttpResponse(json.dumps(cities),
                                 mimetype='application/json')
    raise http.Http404


def get_choice_val(val):
    for x in TYPE_LIKE:
        if x[1] == val:
            return x[0]
    return

@login_required
def event_make_like(request, pk, kind):
    if request.is_ajax():
        kind_val = get_choice_val(kind)
        if not kind_val:
            return JsonResponse({'valid': False})
        like, created = Like.objects.get_or_create(
            user=request.user, event_id=pk, defaults={'type': kind_val})
        if not created:
            if like.type == kind_val:
                like.delete()
            else:
                like.type = kind_val
                like.save()
        return JsonResponse({
            'valid': True,
            'kind': kind,
            'likes': Like.objects.filter(event_id=pk, type=1).count(),
            'dislike': Like.objects.filter(event_id=pk, type=2).count(),
            })
    return redirect('/')

