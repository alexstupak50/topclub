# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'WordsFilter'
        db.create_table(u'core_wordsfilter', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('words', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'core', ['WordsFilter'])


    def backwards(self, orm):
        # Deleting model 'WordsFilter'
        db.delete_table(u'core_wordsfilter')


    models = {
        u'core.wordsfilter': {
            'Meta': {'object_name': 'WordsFilter'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'words': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['core']