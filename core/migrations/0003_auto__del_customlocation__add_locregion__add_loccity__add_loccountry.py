# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'CustomLocation'
        db.delete_table(u'core_customlocation')

        # Adding model 'LocRegion'
        db.create_table(u'core_locregion', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.LocCountry'])),
            ('title_ru', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('title_en', self.gf('django.db.models.fields.CharField')(max_length=30)),
        ))
        db.send_create_signal(u'core', ['LocRegion'])

        # Adding model 'LocCity'
        db.create_table(u'core_loccity', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('region', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.LocRegion'])),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.LocCountry'])),
            ('title_ru', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('title_en', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('important', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('default', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('lat', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('lng', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'core', ['LocCity'])

        # Adding model 'LocCountry'
        db.create_table(u'core_loccountry', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title_ru', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('title_en', self.gf('django.db.models.fields.CharField')(max_length=30)),
        ))
        db.send_create_signal(u'core', ['LocCountry'])


    def backwards(self, orm):
        # Adding model 'CustomLocation'
        db.create_table(u'core_customlocation', (
            ('is_default', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('city', self.gf('django.db.models.fields.related.OneToOneField')(related_name='custom_location', unique=True, to=orm['django_geoip.City'])),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'core', ['CustomLocation'])

        # Deleting model 'LocRegion'
        db.delete_table(u'core_locregion')

        # Deleting model 'LocCity'
        db.delete_table(u'core_loccity')

        # Deleting model 'LocCountry'
        db.delete_table(u'core_loccountry')


    models = {
        u'core.loccity': {
            'Meta': {'object_name': 'LocCity'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.LocCountry']"}),
            'default': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'important': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'lat': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'lng': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.LocRegion']"}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'core.loccountry': {
            'Meta': {'object_name': 'LocCountry'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'core.locregion': {
            'Meta': {'object_name': 'LocRegion'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.LocCountry']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'core.wordsfilter': {
            'Meta': {'object_name': 'WordsFilter'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'words': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['core']