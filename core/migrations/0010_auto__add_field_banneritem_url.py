# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'BannerItem.url'
        db.add_column(u'core_banneritem', 'url',
                      self.gf('django.db.models.fields.SlugField')(default=1, max_length=200),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'BannerItem.url'
        db.delete_column(u'core_banneritem', 'url')


    models = {
        u'core.banner': {
            'Meta': {'object_name': 'Banner'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        },
        u'core.banneritem': {
            'Meta': {'object_name': 'BannerItem'},
            'banner': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Banner']"}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'url': ('django.db.models.fields.SlugField', [], {'max_length': '200'})
        },
        u'core.loccity': {
            'Meta': {'object_name': 'LocCity'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.LocCountry']"}),
            'default': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'important': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'lat': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'lng': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.LocRegion']"}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'core.loccountry': {
            'Meta': {'object_name': 'LocCountry'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'core.locregion': {
            'Meta': {'object_name': 'LocRegion'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.LocCountry']"}),
            'default_city': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.LocCity']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'core.settings': {
            'Meta': {'object_name': 'Settings'},
            'audio_play': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'audio_volume': ('django.db.models.fields.PositiveIntegerField', [], {'default': '60'}),
            'chat_delete_delta': ('django.db.models.fields.PositiveIntegerField', [], {'default': '10'}),
            'chat_max_length': ('django.db.models.fields.PositiveIntegerField', [], {'default': '50'}),
            'event_guest_delete_delta': ('django.db.models.fields.PositiveIntegerField', [], {'default': '14'}),
            'event_reg_delete_delta': ('django.db.models.fields.PositiveIntegerField', [], {'default': '30'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message_delete_delta': ('django.db.models.fields.PositiveIntegerField', [], {'default': '10'})
        },
        u'core.wordsfilter': {
            'Meta': {'object_name': 'WordsFilter'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'words': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['core']