# -*- encoding: utf-8 -*-
from celery import shared_task
from event.models import Event
from catalog.models import Advert


@shared_task
def clean_blank():
    Event.objects.filter(title__isnull=True).delete()
    Advert.objects.filter(blank=True).delete()