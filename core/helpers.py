# -*- encoding: utf-8 -*-


from django.conf import settings as base_settings
from core.models import Settings


class DBSettings(object):

    def __getattr__(self, item):
        if hasattr(base_settings, item):
            return getattr(base_settings, item)
        try:
            return getattr(Settings.objects.first(), item.lower())
        except (AttributeError, Settings.DoesNotExist):
            raise AttributeError

site_settings = DBSettings()
