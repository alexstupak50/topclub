# -*- coding:utf-8 -*-

import logging

from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from django_geoip.models import GeoLocationFacade, City
from solo.models import SingletonModel

from core.utils import get_coordinates


TARGET_BANNER = (
    ('_blank', _(u'Загружает страницу в новое окно браузера.')),
    ('_self', _(u'Загружает страницу в текущее окно.'))
)


class WordsFilter(SingletonModel):
    words = models.TextField(u'слова', help_text=u'Через запятую.')

    class Meta:
        verbose_name = verbose_name_plural = u"фильтр слов"

    def __unicode__(self):
        return u'список плохих слов'


class LocCountry(models.Model):
    title_ru = models.CharField(u'название (RU)', max_length=30)
    title_en = models.CharField(u'название (EN)', max_length=30)

    class Meta:
        verbose_name = u'страна'
        verbose_name_plural = u'страны'

    def __unicode__(self):
        return u'{0}, {1}'.format(self.title_ru, self.title_en)


class ActiveManager(models.Manager):
    def get_queryset(self):
        return super(ActiveManager, self).get_queryset().filter(is_active=True)


class LocRegion(models.Model):
    country = models.ForeignKey(LocCountry, verbose_name=u'страна')
    title_ru = models.CharField(u'название (RU)', max_length=30)
    title_en = models.CharField(u'название (EN)', max_length=30)
    default_city = models.ForeignKey('LocCity', verbose_name=u'по умолчанию', null=True, blank=True)
    is_active = models.BooleanField(u'Отображать в фильтрах', default=True)
    objects = ActiveManager()
    admin_objects = models.Manager()

    class Meta:
        verbose_name = u'регион'
        verbose_name_plural = u'регионы'

    def __unicode__(self):
        return u'{0}'.format(self.title_ru)

    @staticmethod
    def autocomplete_search_fields():
        return ("id__iexact", "title_ru__icontains", "title_en__icontains",)


class LocCity(GeoLocationFacade):
    # iprange = models.ForeignKey(City, verbose_name=u'привязка к geo', related_name='loc_city')
    region = models.ForeignKey(LocRegion, verbose_name=u'регион')
    country = models.ForeignKey(LocCountry, verbose_name=u'страна')
    title_ru = models.CharField(u'название (RU)', max_length=30)
    title_en = models.CharField(u'название (EN)', max_length=30)
    important = models.BooleanField(u'важный', default=False)
    default = models.BooleanField(u'по умолчанию', default=False)
    is_active = models.BooleanField(u'Отображать в фильтрах', default=True)
    # координаты
    lat = models.FloatField(
        verbose_name=u'широта', null=True, blank=True, help_text=u'Десятичное число. Например: 50.254444')
    lng = models.FloatField(
        verbose_name=u'долгота', null=True, blank=True, help_text=u'Десятичное число. Например: 28.657778')

    objects = ActiveManager()
    admin_objects = models.Manager()

    class Meta:
        verbose_name = u'город'
        verbose_name_plural = u'города'

    def __unicode__(self):
        return u'{0}'.format(self.title_ru)

    def get_lnglat(self):
        return u'{0}, {1}'.format(self.lng, self.lat)

    @classmethod
    def get_by_ip_range(cls, ip_range):
        if ip_range:
            # return ip_range.loc_city
            return cls.get_default_location()
        else:
            raise ObjectDoesNotExist

    @classmethod
    def get_default_location(cls):
        return LocCity.objects.filter(default=True).first()

    @classmethod
    def get_available_locations(cls):
        return City.objects.all()

    @classmethod
    def get_loc(cls, pk):
        return LocCity.objects.get(pk=pk)

    # def __repr__(self):
    #     return 'LocCity(id={0}, city={1})'.format(self.pk, self.title_ru)

    @staticmethod
    def autocomplete_search_fields():
        return ("id__iexact", "title_ru__icontains",  "title_en__icontains",)


@receiver(models.signals.pre_save, sender=LocCity)
def loccity_get_coordinates(sender, instance, **kwargs):
    if not all([instance.lat, instance.lng]):
        instance.__dict__.update(get_coordinates(logging.getLogger(__name__), LocCountry.objects.get(pk=instance.country_id).title_ru, instance.title_ru))


class Logo(models.Model):
    logo = models.ImageField(
        _(u'Файл'), upload_to='logo/',
        help_text=_(u'Только изображения'))
    url = models.CharField(max_length=200, verbose_name=_(u'URL'), blank=True, null=True)
    target = models.CharField(
        max_length=255, verbose_name=_(u'Назначение'), default='_self', choices=TARGET_BANNER)

    class Meta:
        verbose_name = verbose_name_plural = _(u'Логотип')

    def __unicode__(self):
        return u'Логотип'


class Banner(models.Model):
    title = models.CharField(u'Название', max_length=250)

    class Meta:
        verbose_name = verbose_name_plural = _(u'банер')

    def __unicode__(self):
        return self.title


class BannerItem(models.Model):
    banner = models.ForeignKey(Banner, verbose_name=_(u'Банер'))
    file = models.FileField(_(u'Файл'), upload_to='banner/')
    url = models.CharField(max_length=200, verbose_name=_(u'URL'), blank=True, null=True)
    target = models.CharField(
        max_length=255, verbose_name=_(u'Назначение'), default='_self', choices=TARGET_BANNER)

    class Meta:
        verbose_name = verbose_name_plural = _(u' ')

    def __unicode__(self):
        return u'Банер'

    def is_swf(self):
        if self.file.name.endswith('.swf'):
            return True
        return False


class Settings(models.Model):
    event_reg_on_feed = models.PositiveIntegerField(
        _(u'Время пребывания ивента в ленте до попадания в архив'),
        default=14, help_text=_(u'в днях'))
    event_reg_delete_delta = models.PositiveIntegerField(
        _(u'Время перебывания ивента в архиве до полного удаления'),
        default=30, help_text=_(u'в днях'))
    event_guest_delete_delta = models.PositiveIntegerField(
        _(u'Промежуток чистки ивентов гостей'),
        default=14, help_text=_(u'в днях'))
    audio_play = models.BooleanField(
        _(u'Воспроизводить аудио файлы при открытии страницы'), default=True)
    audio_volume = models.PositiveIntegerField(
        _(u'Громкость аудио по умолчанию'), default=60,
        help_text=_(u'от 0 до 100'))
    chat_delete_delta = models.PositiveIntegerField(
        _(u'Промежуток чистки чата'), default=10, help_text=_(u'в минутах'))
    chat_max_length = models.PositiveIntegerField(
        _(u'Максимальное количество символов в чате'), default=50)
    message_delete_delta = models.PositiveIntegerField(
        _(u'Промежуток чистки личной переписки'),
        default=10, help_text=_(u'в днях'))
    time_slider = models.PositiveIntegerField(
        _(u'Время смены картинки в слайдере'), help_text="1сек. == 1000,2сек. == 2000", default=4000)

    class Meta:
        verbose_name = _(u'Настройки')
        verbose_name_plural = _(u'Настройки')

    def __unicode__(self):
        return u'Настройки'
