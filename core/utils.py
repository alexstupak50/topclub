# -*- coding:utf-8 -*-
import json
import os
import sys
import traceback
import requests
import logging
import string
from datetime import date, timedelta
from decimal import Decimal
from hashlib import md5
from pprint import PrettyPrinter
from functools import partial
from xml.etree import ElementTree as ET
from base64 import b64encode, b64decode
from Crypto.Cipher import AES
from django.http import HttpResponse
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from django.conf import settings
from django.utils import timezone
from django.utils.html import format_html
from django.template.defaultfilters import slugify
from django.forms import MediaDefiningClass
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.cache import cache
from django.contrib import admin
from django.http import HttpResponseBadRequest

from sorl.thumbnail import delete

logger = logging.getLogger(getattr(settings, 'DEFAULT_LOGGER_NAME', __name__))

# source:
# http://stackoverflow.com/questions/6473340/link-to-foreignkey-in-admin-causes-attributeerror-when-debug-is-false/
# http://www.techques.com/question/1-6473340/Link-To-Foreignkey-in-Admin-Causes-AttributeError-When-Debug-Is-False/


class ForeignKeyLinksMetaclass(admin.ModelAdmin.__class__):
    '''
    Metaclass for ModelAdmin.
    Using:
    0. Imagine we have following models with foreign_key relation:

        class Reporter(models.Model):
            first_name = models.CharField(max_length=30)
            last_name = models.CharField(max_length=30)

        class Article(models.Model):
            ...
            reporter = models.ForeignKey(Reporter)

    1. Add metaclass to your ModelAdmin class, like:

        class ArticleAdmin(admin.ModelAdmin):
            __metaclass__ = ForeignKeyLinksMetaclass

    2. Add to list_display field, which name must consist of 'link_to_' and
    field name related to object.
    For example to get display foreign key link in ArticleAdmin to
    related Reporter you must add to list_display field which
    name wil be 'link_to_reporter'self.

        class ArticleAdmin(admin.ModelAdmin):
            __metaclass__ = ForeignKeyLinksMetaclass

            list_display = (... 'link_to_reporter', ...)

    3. You can define the 'admin_list_display' method in related model and it
    will be used to format text placed in the link.

        class Reporter(models.Model):
            first_name = models.CharField(max_length=30)
            last_name = models.CharField(max_length=30)

            def admin_list_display(self):
                return u'{0} {1}'.format(self.first_name, self.last_name)

    If not defined, default unicode will be used.

    4. Also you can change default short_description. Define the
    'list_display_titles' which must be a dict instance with link_to_ field
    name as keys and short_description name as value. For example:

        class ArticleAdmin(admin.ModelAdmin):
            __metaclass__ = ForeignKeyLinksMetaclass

            list_display = (... 'link_to_reporter', ...)
            list_display_titles = {'link_to_reporter': u'Link to reporter')}

    By default field name capitalize used.
    '''
    def __new__(cls, name, bases, attrs):
        new_class = super(ForeignKeyLinksMetaclass, cls).__new__(cls, name, bases, attrs)

        def foreign_key_link(instance, field):
            target = getattr(instance, field)
            if target is None:
                return
            url = reverse('admin:{0}_{1}_change'.format(
                target._meta.app_label,
                target._meta.module_name),
                args=(target.id,))
            if hasattr(target, 'admin_list_display'):
                text = target.admin_list_display()
            else:
                text = unicode(target)
            return format_html('<a href="%s">%s</a>' % (url, text))

        def _add_method(name):
            if name is None:
                return
            if isinstance(name, basestring) and name[:8] == 'link_to_':
                try:
                    method = partial(foreign_key_link, field=name[8:])
                    if hasattr(new_class, 'list_display_titles') and \
                                    name in new_class.list_display_titles:
                        method.__name__ = new_class.list_display_titles[name]
                    else:
                        method.__name__ = name[8:]
                    method.allow_tags = True
                    setattr(new_class, name, method)
                except Exception, ex:
                    logger.debug("_add_method(%s) failed: %s" % (name, ex))

        if all([hasattr(new_class, "list_display"),
                new_class.list_display is not None]):
            for name in new_class.list_display:
                _add_method(name)
        if all([hasattr(new_class, "readonly_fields"),
                new_class.readonly_fields is not None]):
            for name in new_class.readonly_fields:
                _add_method(name)
        if all([hasattr(new_class, "fields"),
                new_class.fields is not None]):
            for name in new_class.fields:
                _add_method(name)

        return new_class


class CashedCountPaginator(Paginator):
    """
    Paginator with items count caching.

    Need a query related 'cache_key' passed in to kwargs
    """
    def __init__(self, *args, **kwargs):
        cache_key = getattr(settings, 'PAGINATOR_COUNT_KEY')
        self.cache_key = cache_key.format(kwargs.pop('cache_key'))
        super(CashedCountPaginator, self).__init__(*args, **kwargs)

    def _get_count(self):
        if self._count is None:
            self._count = self.get_or_set_cache()
        return self._count
    count = property(_get_count)

    def get_or_set_cache(self):
        if self.cache_key:
            self._count = cache.get(self.cache_key)
        if self._count is None:
            self._count = super(CashedCountPaginator, self)._get_count()
            # 60 sec * 30 min
            cache_time = 60*30
            cache.set(self.cache_key, self._count, int(cache_time))
        return self._count


def paginate_object(obj_query, items_number, cache_key=None, page=1):
    ''' Cached paginator'''
    # paginate
    obj_pages = CashedCountPaginator(obj_query, items_number, cache_key=cache_key)

    # paginate object
    try:
        result = obj_pages.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        result = obj_pages.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        result = obj_pages.page(obj_pages.num_pages)
    return result


def paginate_objects(obj_query, items_number, page=1):
    '''None cached paginator'''
    # paginate
    obj_pages = Paginator(obj_query, items_number)

    # paginate object
    try:
        result = obj_pages.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        result = obj_pages.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        result = obj_pages.page(obj_pages.num_pages)
    return result


def path(instance, filename, path='images'):
    """
    Returns a file path where to save a photo/image
    """
    ext = filename.rsplit('.')[-1].lower()
    hash = md5(slugify(filename) + '%s' % timezone.now())
    new_filename = '%s.%s' % (hash.hexdigest()[3:10], ext)
    return u'files/%s/%s/%s/%s/%s/%s' % (instance._meta.app_label,
                                         path, date.today().year,
                                         date.today().month,
                                         date.today().day,
                                         new_filename)


def delete_image_on_delete(sender, instance, **kwargs):
    """
    Deletes file from filesystem
    when corresponding `Mdb` object is deleted.
    """
    if instance.image:
        delete(instance.image)
        if os.path.isfile(instance.image.path):
            os.remove(instance.image.path)


def delete_image_on_change(sender, instance, **kwargs):
    """
    Deletes file from filesystem
    when corresponding object is changed.
    """
    if not instance.pk:
        return False
    try:
        old_file = sender.objects.get(pk=instance.pk).image
    except sender.DoesNotExist:
        return False
    new_file = instance.image
    if old_file and not old_file == new_file:
        delete(old_file)
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)


def get_coordinates(_logger, country, city):
    """
    Get location name and returns coordinates in dict
    """
    url = getattr(settings, 'YANDEX_API_URL', 'http://geocode-maps.yandex.ru/1.x/')
    params = {'geocode': '%s, %s' % (country, city), 'format': 'json', 'results': 1}
    try:
        r = requests.get(url, params=params)
    except Exception, e:
        msg = 'Error:\n {0}\n{1}\n'.format(e, " ".join(traceback.format_exception(*sys.exc_info())))
        _logger.info(msg)
        return {}
    if r.status_code == requests.codes.ok:
        try:
            return dict(zip(['lng', 'lat'], r.json()['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos'].split(' ')))
        except Exception, e:
            msg = 'Error:\n {0}\n{1}\n'.format(e, " ".join(traceback.format_exception(*sys.exc_info())))
            _logger.info(msg)
    return {}


def get_location_data(_logger, lat, lng):
    """
    Get coordinates and returns location data in dict
    """
    url = getattr(settings, 'YANDEX_API_URL', 'http://geocode-maps.yandex.ru/1.x/')
    params = {'geocode': '%s, %s' % (lat, lng), 'format': 'json', 'results': 1, 'sco': 'latlong'}
    try:
        r = requests.get(url, params=params)
    except Exception, e:
        msg = 'Error:\n {0}\n{1}\n'.format(e, " ".join(traceback.format_exception(*sys.exc_info())))
        _logger.info(msg)
        return {}
    if r.status_code == requests.codes.ok:
        try:
            return r.json()['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']
        except Exception, e:
            msg = 'Error:\n {0}\n{1}\n'.format(e, " ".join(traceback.format_exception(*sys.exc_info())))
            _logger.info(msg)
    return {}


class MyPrettyPrinter(PrettyPrinter):
    def format(self, *args, **kwargs):
        repr, readable, recursive = PrettyPrinter.format(self, *args, **kwargs)
        if repr:
            if repr[0] in ('"', "'"):
                repr = repr.decode('string_escape')
            elif repr[0:2] in ("u'", 'u"'):
                repr = repr.decode('unicode_escape').encode(sys.stdout.encoding)
        return repr, readable, recursive


def cprint(obj, stream=None, indent=1, width=80, depth=None):
    printer = MyPrettyPrinter(stream=stream, indent=indent, width=width, depth=depth)
    printer.pprint(obj)


class GetCourse(object):
    """
    Using:
    GetCourse().convert(currency_id, amount)

    Will get courses from cache or nbrb.by
    and return amount counted to main currency.
    """
    # USD, EUR, RUB
    codes = {3: 145,
             4: 19,
             2: 190}
    default_data = {3: Decimal(10300),
                    4: Decimal(13830),
                    2: Decimal(291)}
    main_currency = getattr(settings, 'MAIN_CURRENCY', 3)
    # 60 sec * 60 min * 2 hours
    cache_time = 60*60*2
    cache_key = getattr(settings, 'COURSE_CACHE_KEY', 'course_data_{0}')
    key = """./Currency[@Id='{0}']/Rate"""

    def __init__(self, *args, **kwargs):
        self.url = kwargs.get('url', 'http://www.nbrb.by/Services/XmlExRates.aspx')
        self.get_data()

    def __str__(self):
        return str(self.data)

    def retreive_data(self):
        try:
            r = requests.get(self.url)
            if r.status_code == requests.codes.ok:
                return ET.fromstring(r.text.encode(r.encoding))
        except Exception, e:
            logger.info(e)
        return None

    def extract_data(self):
        xml = self.retreive_data()
        return dict((k, Decimal(xml.find(self.key.format(v)).text)) for k, v in self.codes.iteritems())

    def get_and_cache(self):
        cur_cache_key = self.cache_key.format(timezone.now().hour)
        self.data = self.extract_data()
        if self.data:
            cache.set(cur_cache_key, self.data, int(self.cache_time))
        else:
            self.data = self.default_data

    def get_from_cache(self):
        cur_cache_key = self.cache_key.format(timezone.now().hour)
        self.data = cache.get(cur_cache_key)

    def get_data(self):
        self.get_from_cache()
        if self.data is None:
            self.get_and_cache()

    def convert(self, currency, amount):
        amount = Decimal(amount)
        try:
            if currency == self.main_currency:
                return amount
            elif currency in self.data.keys():
                return amount * (self.data[currency] / self.data[self.main_currency])
            else:
                return amount * (1 / self.data[self.main_currency])
        except KeyError, e:
            logger.info(e)


class UCrypto(object):
    '''
    Based on https://pypi.python.org/pypi/django-crypto
    Settings must be set:

    CRYPTO_SECRET = 'your strong key here'
    CRYPTO_BLOCK_SIZE = 32
    CRYPTO_PADDING_CHAR = '{'

    Using:
    UCrypto.encode('string')
    UCrypto.decode('string')

    '''
    cipher = AES.new(getattr(settings, 'CRYPTO_SECRET'))
    cbs = getattr(settings, 'CRYPTO_BLOCK_SIZE', 32)
    cpc = getattr(settings, 'CRYPTO_PADDING_CHAR', '{')

    def __init__(self, *args):
        self.s = string.join(map(str, args), ',')

    def _pad(self):
        '''
        Adds padding characters to the string, so that the len of the string
        is divisible by the block size.
        '''
        return self.s + (self.cbs - len(self.s) % self.cbs) * self.cpc

    def encode(self):
        '''
        Actually encode a string using the cipher.
        '''
        return b64encode(self.cipher.encrypt(self._pad()))

    def decode(self):
        '''
        Decode a string using the cipher.
        '''
        return self.cipher.decrypt(b64decode(self.s)).rstrip(self.cpc)


def ajax_required(f):
    """
    src: https://djangosnippets.org/snippets/771/
    AJAX request required decorator
    use it in your views:

    @ajax_required
    def my_view(request):
        ....

    """
    def wrap(request, *args, **kwargs):
        if not request.is_ajax():
            return HttpResponseBadRequest()
        return f(request, *args, **kwargs)
    wrap.__doc__ = f.__doc__
    wrap.__name__ = f.__name__
    return wrap


class JsonResponse(HttpResponse):
    """
    JSON response
    """
    def __init__(self, content, mimetype='application/json', status=None, content_type=None):
        super(JsonResponse, self).__init__(
            content=json.dumps(content),
            mimetype=mimetype,
            status=status,
            content_type=content_type,
        )


def mat_validator(val):
    from core.matfilter import matfilter
    if val and matfilter(val):
        raise ValidationError(_(u'Пожалуйста удалите бранные слова.'))
