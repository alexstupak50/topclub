# -*- coding: utf-8 -*-
from django.core.management.base import NoArgsCommand

from core.models import LocCity, LocCountry, LocRegion


class Command(NoArgsCommand):
    def handle_noargs(self, **options):
        region_exclude = {
            1: u'Брецкая область',
            5: u'Минская область',
            }
        city_exclude = {
            '1701': u'Малорита, Бретская область',
            '22813': u'Минск, Минская область',
            '16135': u'Молодечко, Минская область'
        }
        LocRegion.objects.exclude(id__in=region_exclude.keys()).update(is_active=False)
        # Брецкой обл оставить только Малорита
        LocCity.objects.exclude(id__in=city_exclude.keys()).update(is_active=False)

