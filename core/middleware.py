# -*- encoding: utf-8 -*-

import pytz

from django.utils import timezone
from django.conf import settings


class TimezoneMiddleware(object):

    def process_request(self, request):
        timezone.activate(pytz.timezone(settings.TIME_ZONE))
        request.session.save()