# -*- encoding: utf-8 -*-

from core.helpers import site_settings as settings
from django.core.urlresolvers import resolve


def site_settings(request):
    return {
        'site_settings': settings
    }


def appname(request):
    return {
        'appname': resolve(request.path).app_name
    }


def test_server(request):
    return {
        'test_server': getattr(settings, 'TEST_SERVER', False)
    }

