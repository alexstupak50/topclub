# -*- coding: utf-8 -*-
from django.contrib import admin

from solo.admin import SingletonModelAdmin

from core.models import WordsFilter, LocCountry, LocRegion, LocCity, Banner, \
    Settings, BannerItem, Logo
from django_geoip.models import Country, Region, City, IpRange


class SettingsAdmin(admin.ModelAdmin):

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def changelist_view(self, request, extra_context=None):
        if not Settings.objects.exists():
            Settings.objects.create()

        return self.change_view(
            request, object_id='1', extra_context=extra_context)


class IpRangeAdmin(admin.ModelAdmin):
    list_filter = ('country', 'region', 'city')


class BaseAdminManger(object):

    def get_queryset(self, request):
        qs = self.model.admin_objects.get_queryset()
        ordering = self.get_ordering(request)
        if ordering:
            qs = qs.order_by(*ordering)
        return qs


class LocCityAdmin(BaseAdminManger, admin.ModelAdmin):
    list_filter = ('default', 'region', 'country', 'important')
    list_display = ('title_ru', 'region', 'country', 'is_active')
    search_fields = ['title_ru']


class LocRegionAdmin(BaseAdminManger, admin.ModelAdmin):
    raw_id_fields = ('default_city',)
    list_display = ('title_ru', 'is_active')
    autocomplete_lookup_fields = {
        'fk': ['default_city'],
    }


class BannerInlineAdmin(admin.TabularInline):
    model = BannerItem
    extra = 3
    max_num = 3


class BannerAdmin(admin.ModelAdmin):
    inlines = [
        BannerInlineAdmin,
    ]

    # def has_add_permission(self, request):
    #     return request.get_full_path() == '/admin/core/banner/'

    def has_delete_permission(self, request, obj=None):
        return False

    # def changelist_view(self, request, extra_context=None):
    #     if Banner.objects.exists():
    #         return self.change_view(
    #             request, object_id='1', extra_context=extra_context)
    #     return self.add_view(request, form_url='', extra_context=extra_context)


class LogoAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        return request.get_full_path() == '/admin/core/logo/'

    def has_delete_permission(self, request, obj=None):
        return False

    def changelist_view(self, request, extra_context=None):
        if Logo.objects.exists():
            return self.change_view(
                request, object_id='1', extra_context=extra_context)
        return self.add_view(request, form_url='', extra_context=extra_context)


admin.site.register(LocCountry)
admin.site.register(Logo, LogoAdmin)
admin.site.register(LocRegion, LocRegionAdmin)
admin.site.register(LocCity, LocCityAdmin)
admin.site.register(Banner, BannerAdmin)
admin.site.register(Settings, SettingsAdmin)
admin.site.register(WordsFilter, SingletonModelAdmin)
# GEOIP  - убрал, так как не используеться
# admin.site.register(Country)
# admin.site.register(Region)
# admin.site.register(City)
# admin.site.register(IpRange, IpRangeAdmin)