# -*- coding: UTF-8 -*-
from random import sample
import datetime
from django import template
from django.db.models import Q
from django.core.cache import cache
from django.template.defaultfilters import stringfilter
from django.conf import settings
from django.utils import timezone


from catalog.models import Catalog, Category, Advert
from core.models import LocRegion, LocCity, Banner, Logo
from core.forms import GeoForm
from cabinet.models import Message, UserProfile
from comments.models import Comment
from event.models import Event

register = template.Library()
adv_filter = dict(blank=False, hidden=False)


@register.inclusion_tag('pagination/pagination.html', takes_context=True)
def get_paginator(context, variable, adjacent_pages=4, page_key='page'):
    '''
    If 'tab' in request.GET and tab_val='delete' it will be removed or changed:
                ?page=1&tab=blablabla   ->   ?page=1
        or:     ?page=1&tab=blablabla    ->  ?page=1&tab=new_value

    'page_key' is a key value to store page number in GET params
        if page_key='page' params will be:
                                                ?page=...
        if page_key='pop_page' params will be:
                                                ?pop_page=...

    'variable' - must be the string name of the context listing variable
    '''
    # получаем GET параметры
    GET_params = template.Variable('request').resolve(context).GET.copy()
    # удаляем параметр страницы
    if page_key in GET_params:
        del GET_params[page_key]
    GET_params = '?%s&' % GET_params.urlencode() if GET_params else '?'

    # обработка пагинации
    page_obj = context[variable]
    if 'number' not in page_obj:
        current_page = page_obj.number
        number_of_pages = page_obj.paginator.num_pages

    startPage = current_page - adjacent_pages
    if startPage <= 2:
        startPage = 1

    endPage = current_page + adjacent_pages
    if endPage >= number_of_pages - 1:
        endPage = number_of_pages

    page_numbers = [n for n in range(startPage, endPage + 1) if 0 < n <= number_of_pages]

    return {'get_params': GET_params,
            'page_key': page_key,
            'page_obj': page_obj,
            'page_numbers': page_numbers,
            'show_first': startPage > 1,
            'show_last': endPage < number_of_pages
            }


@register.filter
@stringfilter
def bring_space(value, delimiter=None):
    return ', '.join(value.split(delimiter))
bring_space.is_safe = True


@register.inclusion_tag('templatetags/cat_menu.html')
def get_cat_menu():
    cache_key = getattr(settings, 'MAIN_MENU_CACHE_KEY')
    # 60 sec * 60 min * 6 hours
    # cache_time = 60*60*6
    cache_time = 10
    c = cache.get(cache_key)
    if c is None and Catalog.objects.exists():
        catalogs = Catalog.objects.filter(
            hidden=False).values('pk', 'title', 'slug')
        catalogs_set, catagories = set(x['pk'] for x in catalogs), {}
        for category in Category.objects.filter(
                hidden=False, catalog_id__in=catalogs_set).values('pk', 'title', 'slug', 'catalog_id'):
            catagories.setdefault(category['catalog_id'], []).append(category)
        for catalog in catalogs:
            if catalog['pk'] in catagories:
                count = str(Advert.objects.filter(
                    public=True,
                    category_id__in=set(x['pk'] for x in catagories[catalog['pk']]),
                    **adv_filter).count())
                # orm_count = str(Advert.objects.filter(
                #     public=True,
                #     category_id__in=set(x['pk'] for x in catagories[catalog['pk']]),
                #     **adv_filter).count())
                # count = ''
                # count_len = len(orm_count)
                # if 1 < count_len <= 3:
                #     count = orm_count[:-1] + '0+'
                # elif 3 < count_len <= 5:
                #     count = orm_count[:-2] + '00+'
                # elif count_len > 5:
                #     count = orm_count[:-3] + '000+'
                catalog['count'] = count
                catalog['categories'] = catagories[catalog['pk']]
        c = catalogs
        cache.set(cache_key, catalogs if catalogs else None, int(cache_time))

    return {'cat_menu': c}


@register.inclusion_tag('templatetags/cabinet_menu.html', takes_context=True)
def get_cabinet_menu(context, user_id):
    c = {}
    request = context['request']
    user = context.get('user')
    c['drafts'] = Advert.objects.filter(public=False, user_id=user_id,
                                        **adv_filter).count()
    c['adverts'] = Advert.objects.filter(public=True, user_id=user_id,
                                         **adv_filter).count()
    c['comments'] = Comment.objects.filter(user_id=user_id,
                                           hidden=False).count()
    c['events'] = Event.objects.get_actual().filter(user_id=user_id).count()
    c['archive'] = Event.objects.get_archive().filter(user_id=user_id).count()
    c['messages'] = Message.objects.get_messages(user).count()
    count = user.count_new_message()
    c['messages_new'] = '+%s' % count if count else ''
    return {'quantity': c}


@register.filter()
def get_advert_type_display(value):
    try:
        return dict(Advert.type_choices)[value]
    except KeyError:
        return None


@register.filter()
def get_advert_currency_display(value):
    try:
        return dict(Advert.currency_choices)[value]
    except KeyError:
        return None


@register.inclusion_tag('templatetags/geo_menu.html', takes_context=False)
def get_geo_menu():
    cache_key = getattr(settings, 'GEO_MENU_CACHE_KEY')
    # 60 sec * 60 min * 24 hours
    cache_time = 60*60*24

    c = cache.get(cache_key)
    if c is None:
        c = {}
        c['regions'] = LocRegion.objects.none()
        c['cities'] = LocCity.objects.none()
        cache.set(cache_key, c if c else None, int(cache_time))
    return {'form': GeoForm()}


@register.inclusion_tag('templatetags/latest.html', takes_context=False)
def get_latest():
    cache_key = getattr(settings, 'LATEST_CACHE_KEY')
    # 60 sec * 60 min * 24 hours
    cache_time = 60*60*24
    c = cache.get(cache_key)
    if c is None:
        c = list(Advert.objects.filter(public=True, hidden=False).values(
            'pk', 'title', 'price', 'currency', 'type', 'cover__image'
            ).order_by('-public_date')[:10])
        cache.set(cache_key, c if c else None, int(cache_time))

    try:
        adverts = sample(c, 5)
    except (ValueError, ):
        adverts = None

    return {'adverts': adverts}


# @register.inclusion_tag('templatetags/lastnews.html')
# def get_lastnews():
#     c = cache.get(NewsArticle.cache_key)
#     if c is None and NewsArticle.objects.exists():
#         c = NewsArticle.objects.all()[:9]
#         cache.set(NewsArticle.cache_key, c if c else None, int(NewsArticle.cache_time))
#         c = cache.get(NewsArticle.cache_key)

#     return {'news': c}


@register.inclusion_tag('templatetags/banner.html', takes_context=True)
def show_banner(context):
    banner = None
    if Banner.objects.exists():
        if 'catalog' == context['appname']:
            banner = Banner.objects.get(id=2).banneritem_set.all()
        else:
            banner = Banner.objects.get(id=1).banneritem_set.all()
    return {
        'banner': banner
    }

@register.inclusion_tag('logo.html')
def logo():
    logo = None
    if Logo.objects.exists():
        logo = Logo.objects.get(id=1)
    return {
        'logo': logo
    }

# @register.inclusion_tag('templatetags/banner.html')
# def show_banner():
#     banner = cache.get(Banner.cache_key)
#     if banner is None and Banner.objects.exists():
#         banner = Banner.objects.filter(hidden=False)
#         cache.set(Banner.cache_key, banner if banner else None, int(Banner.cache_time))
#         banner = cache.get(Banner.cache_key)
#     if banner and banner.count() > 1:
#         banner = random.choice(banner)
#     elif banner:
#         banner = banner[0]
#     return {'banner': banner} if banner else {}


@register.simple_tag(takes_context=True)
def get_params(context, new=False, **kwargs):
    """
    Для добавления гет параметров до уже существующих
    """
    data = dict()
    if not new:
        data = {k: v for k, v in context['request'].GET.iteritems() if v}
    for key, value in kwargs.iteritems():
        data[key] = value

    if not data:
        return ''

    params = '?'
    for key, value in data.iteritems():
        params = '%s%s=%s&' % (params, key, value)
    return params[:-1]


@register.inclusion_tag('metric.html', takes_context=True)
def metrica(context):
    filters = {}
    if 'catalog' == context['appname']:
        model = Advert
        filters['blank'] = False
    else:
        model = Event
    now = datetime.datetime.now()
    all_time = model.objects.filter(**filters)
    year = all_time.filter(created__year=now.year)
    where_month = '%(month)s = MONTH(created)' % {'month': now.month}
    month = year.extra(where=[where_month])
    start_week = now - datetime.timedelta(now.weekday())
    end_week = start_week + datetime.timedelta(7)
    week = month.filter(created__range=[start_week, end_week])
    where_day = '%(day)s = DAY(created)' % {'day': timezone.now().day}
    day = month.extra(where=[where_day])
    return {
        'all_time': all_time.count(),
        'year': year.count(),
        'month': month.count(),
        'week': week.count(),
        'day': day.count(),
        'users': UserProfile.objects.all().count(),
        'type': 'Объявлений' if 'catalog' == context['appname'] else 'Ивентов'
    }
