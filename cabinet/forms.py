# -*- coding:utf-8 -*-
# import logging

from django import forms
# from django.forms.widgets import flatatt, CheckboxInput
# from django.contrib.admin.forms import AdminAuthenticationForm
# from django.contrib.auth.forms import PasswordResetForm, SetPasswordForm
# from django.contrib.sites.models import Site
# from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
# from django.utils.html import format_html,\
#                                 conditional_escape
# # from django.utils.encoding import force_text
from sorl.thumbnail.admin.current import AdminImageWidget

from registration.forms import PasswordCheckMixin
from cabinet.models import UserProfile as User
from cabinet.models import Message

# logger = logging.getLogger(__name__)


class ProfileEditForm(forms.ModelForm, PasswordCheckMixin):
    error_messages = {
        'invalid_password': u'Старый пароль введен неверно',
                    }

    password1 = forms.CharField(widget=forms.PasswordInput(attrs={
                                'render_value': 'False', }),
                                label=u'Новый пароль',
                                required=False)
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={
                                'render_value': 'False', }),
                                label=u'Подтверждение пароля',
                                required=False)
    password3 = forms.CharField(widget=forms.PasswordInput(attrs={
                                'render_value': 'False', }),
                                label=u'Введите пароль',
                                required=False)

    class Meta:
        model = User
        readonly_fields = ('username', 'email')
        fields = ('username', 'email', 'first_name', 'last_name', 'phone',
                  'avatar', 'by_line',
                  'password1', 'password2', 'password3')
        widgets = {
            'avatar': AdminImageWidget
        }

    def __init__(self, *args, **kwargs):
        super(ProfileEditForm, self).__init__(*args, **kwargs)
        if self.Meta.readonly_fields:
            for field in self.Meta.readonly_fields:
                self.fields[field].widget.attrs['readonly'] = True
        self.fields['phone'].widget.attrs.update(
            {'placeholder': '+375 (xx) xxxxxxx'})

    def clean_password1(self):
        return self.check_password_safety()

    def clean_password2(self):
        print self.cleaned_data
        return self.check_password_mismatch()

    def clean_password3(self):
        pwd_new1 = self.cleaned_data.get('password1')
        pwd_new2 = self.cleaned_data.get('password2')
        pwd_old = self.cleaned_data.get('password3')
        if all([pwd_new1, pwd_new2]):
            if pwd_old and self.instance.check_password(pwd_old):
                return pwd_old
            else:
                del self.cleaned_data['password1']
                del self.cleaned_data['password2']
                raise forms.ValidationError(
                    self.error_messages['invalid_password'])
        return ''

    def clean(self):
        if self.Meta.readonly_fields:
            for field in self.Meta.readonly_fields:
                del self.cleaned_data[field]
                # = self.fields[field].initial
        return self.cleaned_data


class MessageCreateForm(forms.ModelForm):
    to = forms.CharField(label=_(u'Кому(никнейм)'), max_length=50)
    subject = forms.CharField(label=_(u"Тема"), max_length=200, required=False)

    class Meta:
        model = Message
        fields = ('to', 'subject', 'body')

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(MessageCreateForm, self).__init__(*args, **kwargs)

    def clean_to(self):
        value = self.cleaned_data['to']
        if self.user.username == value:
            raise forms.ValidationError(
                _(u'Невозможно отправить письмо самому себе'))
        if not User.objects.filter(username=value).exists():
            raise forms.ValidationError(
                _(u'Нет пользователя с никнеймом %s') % value)

        return value

    def clean(self):
        cd = super(MessageCreateForm, self).clean()
        subject = cd.get('subject')
        body = cd['body']
        if not subject and body:
            cd['subject'] = " ".join(body.split(' ')[:3])
        return cd

    def save(self, commit=True):
        cd = self.cleaned_data
        obj = super(MessageCreateForm, self).save(commit=False)

        obj.sender = self.user
        obj.recipient = User.objects.get(username=cd['to'])

        if commit:
            obj.save()

        return obj

# class ProfileEditSettingsForm(forms.ModelForm, PasswordCheckMixin):

#     class Meta:
#         model = User
#         fields = ('settings_new_action', 'settings_new_action_sms',
#          'settings_novelty', 'settings_novelty_sms',
#          'settings_invitation', 'settings_invitation_sms',
#          'settings_friend_accept', 'settings_friend_accept_sms',
#          'settings_action_complete', 'settings_action_complete_sms')








# class PasswordCheckMixin(object):
#     MIN_LENGTH = 6

#     _error_messages = {
#         'password_mismatch': _("Введенные пароли не совпадают."),
#         'light_password': u"Пароль недостаточно сложный. Обязательно наличие\
#                             букв верхнего и нижнего регистра, а так-же цифр.\
#                             Минимальная длина %d символов" % MIN_LENGTH,
#         # 'low_length': u"Пароль должен состоять хотя бы из %d символов." % MIN_LENGTH
#     }

#     def check_password_safety(self, field='password1'):
#         password1 = self.cleaned_data.get(field)

#         if password1:
#             password = unicode(password1)
#             if len(password) < self.MIN_LENGTH:
#                 raise forms.ValidationError(self._error_messages['light_password'])
#             digit = alpha = lower = upper = extra = False
#             for x in password:
#                 if x.isdigit():
#                     digit = True
#                 elif x.isalpha():
#                     alpha = True
#                 if x.islower():
#                     lower = True
#                 elif x.isupper():
#                     upper = True
#                 if x in u'\'"/<>‘':
#                     extra = True
#             if not (digit and lower and upper and alpha) or extra:
#                 raise forms.ValidationError(self._error_messages['light_password'])
#         return password1

#     def check_password_mismatch(self, field1='password1', field2='password2'):
#         password1 = self.cleaned_data.get(field1)
#         password2 = self.cleaned_data.get(field2)
#         if password1 and password2:
#             if password1 != password2:
#                 raise forms.ValidationError(
#                     self._error_messages['password_mismatch'],
#                     code='password_mismatch',
#                 )
#         return password2


# class EmailCheckMixin(object):
#     email_error_messages = {
#         'used_email': u'Этот адрес уже используется. Пожалуйста введите другой\
#                              или воспользуйтесь формой восстановления пароля', }

#     def clean_email(self):
#         """
#         Validate that the supplied email address is unique for the
#         site.
#         """
#         if User.objects.filter(email__iexact=self.cleaned_data['email']):
#             raise forms.ValidationError(self.email_error_messages['used_email'])
#         return self.cleaned_data['email']


# class CustomRegistrationForm(forms.ModelForm,
#                              PasswordCheckMixin,
#                              EmailCheckMixin):
#     error_messages = {
#         'check_tos': u'Для регистрации, вы должны быть согласны с правилами',
#     }

#     password1 = forms.CharField(widget=forms.PasswordInput(attrs={
#                                 'render_value': 'False', }),
#                                 label=u'Пароль', regex=r'^(?=.*\d+)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z\.-_]{6,10}$')
#     password2 = forms.CharField(widget=forms.PasswordInput(attrs={
#                                 'render_value': 'False', }),
#                                 label=u'Подтверждение пароля', regex=r'^(?=.*\d+)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z\.-_]{6,10}$')
#     tos = forms.BooleanField(widget=forms.CheckboxInput(),
#                              help_text=mark_safe(u' С условиями пользовательского <a href="/register/tos/">соглашения</a> согласен'),
#                              label='')
#     phone = forms.RegexField(regex=r'^\+[0-9]{11}$',
#                              max_length=20,
#                              label=u'Телефон',
#                              required=False,
#                              error_messages={'invalid': u"Номер должен быть\
#                                                     +79262424201 формата"})

#     class Meta:
#         model = User
#         fields = ('first_name', 'last_name', 'email', 'phone', 'password1', 'password2', 'email', 'tos')

#     def __init__(self, *args, **kwargs):
#         super(CustomRegistrationForm, self).__init__(*args, **kwargs)
#         self.fields['tos'].widget.attrs.update({'class': 'tooltip'})

#     def clean_password1(self):
#         return self.check_password_safety()

#     def clean_password2(self):
#         return self.check_password_mismatch()

#     def clean_tos(self):
#         """
#         Validate that the user accepted the Terms of Service.
#         """
#         if self.cleaned_data.get('tos', False):
#             return self.cleaned_data['tos']
#         raise forms.ValidationError(self.error_messages['check_tos'])


# class CustomAdminAuthenticationForm(AdminAuthenticationForm):
#     def __init__(self, *args, **kwargs):
#         super(CustomAdminAuthenticationForm, self).__init__(*args, **kwargs)
#         self.fields['username'].label = u'Логин (email)'


# class CustomPasswordResetForm(PasswordResetForm):
#     error_messages = {
#         'invalid_login': u'К сожалению такого логина у нас не зарегистрировано,\
#                            если вы уверены в правильном написании, свяжитесь с\
#                            нашей тех. поддержкой.',
#         'not_activated_error': u'К сожалению Ваш аккаунт еще не активирован.\
#                               Для активации свяжитесь с нашей тех. поддержкой.',
#         'not_activated': u'К сожалению Ваш аккаунт еще не активирован. На Вашу\
#                            почту было выслано письмо. Для подтверждения\
#                            регистрации нужно перейти по ссылке из письма.',
#                     }

#     def clean(self):
#         """
#         Validate that the supplied email address has registereg on the site.
#         """
#         if 'email' in self.cleaned_data:
#             try:
#                 self.user = User.objects.get(email__iexact=self.cleaned_data['email'])
#             except User.DoesNotExist:
#                 raise forms.ValidationError(self.error_messages['invalid_login'])
#             if not self.user.is_active:
#                 try:
#                     registrationprofile = RegistrationProfile.objects.get(user=self.user)
#                 except RegistrationProfile.DoesNotExist:
#                     raise forms.ValidationError(self.error_messages['not_activated_error'])
#                 try:
#                     site = Site.objects.get_current()
#                     registrationprofile.send_activation_email(site)
#                     raise forms.ValidationError(self.error_messages['not_activated'])
#                 except:
#                     raise forms.ValidationError(self.error_messages['not_activated_error'])
#         return self.cleaned_data


# class CustomSetPasswordForm(SetPasswordForm, PasswordCheckMixin):
#     def clean_new_password1(self):
#         return self.check_password_safety(field='new_password1')

#     def clean_new_password2(self):
#         return self.check_password_mismatch(field1='new_password1',
#                                             field2='new_password2')


# class CabinetImageWidget(forms.ClearableFileInput):
#     """
#     An ImageField Widget for settings in user cabinet.
#     """
#     clear_checkbox_label = u'Удалить фото'
#     default_image_url = '/static/images/load.png'
#     help_text = u'<p>Условия загрузки: <br>Размер 100x100 пикселей, <br> jpg,\
#                         jpeg, png. Максимальный<br>размер файла: 100 Кб.</p>'
#     template_with_initial = '%(input)s %(help_text)s %(initial)s\
#                      %(clear_template)s'
#     template_with_clear = u'%(clear)s <label style="width:auto" for="%(clear_checkbox_id)s">%(clear_checkbox_label)s</label>'

#     def render(self, name, value, attrs=None):

#         substitutions = {
#             'initial_text': self.initial_text,
#             'input_text': self.input_text,
#             'clear_template': '',
#             'clear_checkbox_label': self.clear_checkbox_label,
#         }
#         template = '%(input)s %(help_text)s %(initial)s'
#         substitutions['input'] = self.render_super(name, attrs=attrs)
#         substitutions['help_text'] = self.help_text
#         substitutions['initial'] = self.render_image(name, value,
#                                                                 attrs=attrs)
#         if value and hasattr(value, "url"):
#             template = self.template_with_initial
#             if not self.is_required:
#                 checkbox_name = self.clear_checkbox_name(name)
#                 checkbox_id = self.clear_checkbox_id(checkbox_name)
#                 substitutions['clear_checkbox_name'] = conditional_escape(checkbox_name)
#                 substitutions['clear_checkbox_id'] = conditional_escape(checkbox_id)
#                 substitutions['clear'] = CheckboxInput().render(checkbox_name, False, attrs={'id': checkbox_id})
#                 substitutions['clear_template'] = self.template_with_clear % substitutions

#         return mark_safe(template % substitutions)

#     def render_super(self, name, attrs=None):
#         final_attrs = self.build_attrs(attrs, type=self.input_type, name=name)
#         return format_html('<input{0} />', flatatt(final_attrs))

#     def render_image(self, name, value, attrs=None):
#         if value and hasattr(value, 'url'):
#             try:
#                 mini = get_thumbnail(value, '100x100', upscale=False)
#             except Exception as e:
#                 logger.warn("Unable to get the thumbnail", exc_info=e)
#             else:
#                 output = format_html('<img class="load_foto_1" src="{0}">',
#                                                                     mini.url)
#         if not 'output' in locals():
#             output = format_html('<img class="load_foto_1" src="{0}">',
#                                                          self.default_image_url)
#         return output


# class ProfileEditForm(forms.ModelForm, PasswordCheckMixin):
#     error_messages = {
#         'invalid_password': u'Старый пароль введен неверно',
#                     }

#     password1 = forms.CharField(widget=forms.PasswordInput(attrs={
#                                 'render_value': 'False', }),
#                                 label=u'Новый пароль',
#                                 required=False)
#     password2 = forms.CharField(widget=forms.PasswordInput(attrs={
#                                 'render_value': 'False', }),
#                                 label=u'Подтверждение пароля',
#                                 required=False)
#     avatar = forms.ImageField(label=u'Аватар', required=False,
#                               help_text=u'картинка будет сжата до размеров\
#                                                                      160х160',
#                               widget=CabinetImageWidget(attrs={
#                                                         'class': 'from_vk'}))
#     password3 = forms.CharField(widget=forms.PasswordInput(attrs={
#                                 'render_value': 'False', }),
#                                 label=u'Введите пароль',
#                                 required=False)
#     phone = forms.RegexField(regex=r'^\+[0-9]{11}$',\
#                              max_length=20,
#                              label=u'Телефон',
#                              required=False,
#                              error_messages={'invalid': u"Номер должен быть\
#                                                     +79262424201 формата"})

#     class Meta:
#         model = User
#         fields = ('first_name', 'last_name', 'email', 'phone', 'avatar',
#             'password1', 'password2', 'password3')

#     def __init__(self, *args, **kwargs):
#         self.user = kwargs.get('instance')
#         super(ProfileEditForm, self).__init__(*args, **kwargs)

#     def clean_password1(self):
#         return self.check_password_safety()

#     def clean_password2(self):
#         return self.check_password_mismatch()

#     def clean_password3(self):
#         if ('password1' and 'password2' and 'password3') in self.cleaned_data and\
#             self.cleaned_data.get('password1') != '' and\
#             not self.user.check_password(self.cleaned_data.get('password3')):
#             raise forms.ValidationError(
#                                     self.error_messages['invalid_password'])
#         return self.cleaned_data.get('password3')


# class ProfileEditSettingsForm(forms.ModelForm, PasswordCheckMixin):

#     class Meta:
#         model = User
#         fields = ('settings_new_action', 'settings_new_action_sms',
#          'settings_novelty', 'settings_novelty_sms',
#          'settings_invitation', 'settings_invitation_sms',
#          'settings_friend_accept', 'settings_friend_accept_sms',
#          'settings_action_complete', 'settings_action_complete_sms')
