# -*- coding:utf-8 -*-
import urllib2
from urlparse import urlparse
from sorl.thumbnail import get_thumbnail

from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, UserManager
from django.core import validators
from django.core.urlresolvers import reverse
from django.core.mail import send_mail
from django.utils.translation import ugettext_lazy as _
from django.utils.html import format_html
from django.utils import timezone
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile

from core.utils import path
from core.models import LocCity, LocRegion
# from random import choice
# import string


# def _rand_key(size=20):
#     return ''.join([choice(string.letters + string.digits) for i in range(size)])


def avatar_path(*args):
    """
    Returns a file path where to save a photo/image
    Default args are: instance, filename
    Also u can provide kwarg 'path' to path. Only string.
    """
    return path(*args, path='avatar')


class UserProfile(AbstractBaseUser, PermissionsMixin):
    is_staff = models.BooleanField(_('staff status'), default=False,
        help_text=_('Designates whether the user can log into this admin '
                    'site.'))
    is_active = models.BooleanField(_('active'), default=True,
        help_text=_('Designates whether this user should be treated as '
                    'active. Unselect this instead of deleting accounts.'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    # наши данные
    username = models.CharField(_('username'), max_length=30, unique=True,
        help_text=_(u'Обязательное поле. Не более 30 символов. Только буквы, '
                    u'цифры и символы -._'),
        validators=[
            validators.RegexValidator(r'^[\w.-]{3,30}$', _('Enter a valid username.'), 'invalid')
        ])
    email = models.EmailField(u'E-mail', unique=True)
    first_name = models.CharField(u'имя', max_length=20)
    last_name = models.CharField(u'фамилия', max_length=20)
    phone = models.CharField(u"Телефон", max_length=255, default='', blank=True, null=True)
    avatar = models.ImageField(u'аватар', upload_to=avatar_path,
                                max_length=250, blank=True, null=True)
    by_line = models.TextField(u"Подпись", default='', blank=True, null=True)

    admin_region = models.ForeignKey(
        LocRegion, verbose_name=u'регион', null=True, blank=True, related_name='admin_region')
    # geoip
    city = models.ForeignKey(LocCity, verbose_name=u'город', null=True)
    region = models.ForeignKey(LocRegion, verbose_name=u'регион', null=True)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = u'пользователь'
        verbose_name_plural = u'пользователи'

    def __unicode__(self):
        return '%s (%s)' % (self.email, self.get_full_name())

    def admin_list_display(self):
        return format_html(u'{0}<br>{1}'.format(self.email, self.get_full_name()))

    def get_short_name(self):
        return self.username if not self.social_auth.exists() \
            else self.get_full_name()

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def email_user(self, subject, message, from_email=None):
        send_mail(subject, message, from_email, [self.email])

    def get_absolute_url(self):
        return reverse('cabinet:view', args=(self.pk,))

    def get_chat_avatar(self):
        if self.avatar:
            return get_thumbnail(self.avatar, '25x25', crop='center', quality=99).url
        return '/static/images/avatar25x25.gif'

    @staticmethod
    def autocomplete_search_fields():
        return ("id__iexact", "username__icontains", "email__icontains",)

    def is_vk(self):
        return self.social_auth.exists()

    def get_vk_uid(self):
        if not self.is_vk():
            return None
        return self.social_auth.all()[0].uid

    def count_new_message(self):
        return self.received_messages.filter(read_at__isnull=True).count()


def set_vk_avatar(sender, instance, created, **kwargs):
    if not instance.avatar and instance.social_auth.exists():
        try:
            url = instance.social_auth.all()[0].extra_data['photo_200']
            name = urlparse(url).path.split('/')[-1]
            img_temp = NamedTemporaryFile(delete=True)
            img_temp.write(urllib2.urlopen(url).read())
            img_temp.flush()
            instance.avatar.save(name, File(img_temp))
        except AttributeError:
            pass

models.signals.post_save.connect(set_vk_avatar, sender=UserProfile)


class MessageManager(models.Manager):

    def get_messages(self, user):
         return self.filter(
             models.Q(sender=user, sender_deleted_at__isnull=True) |
             models.Q(recipient=user, recipient_deleted_at__isnull=True)
         )

    def get_dialog(self, user, user2):
        return self.filter(
            (models.Q(sender=user, sender_deleted_at__isnull=True) & models.Q(recipient=user2)) |
            (models.Q(sender=user2) & models.Q(recipient=user, recipient_deleted_at__isnull=True))
        )

    def trash_for(self, user):
        """
        Returns all messages that were either received or sent by the given
        user and are marked as deleted.
        """
        return self.filter(
            recipient=user,
            recipient_deleted_at__isnull=False,
        ) | self.filter(
            sender=user,
            sender_deleted_at__isnull=False,
        )

    def count_unread(self, user):
        return self.filter(recipient=user, read_at__isnull=True).count()


class Message(models.Model):
    """
    A private message from user to user
    """
    subject = models.CharField(_(u"Тема"), max_length=120)
    body = models.TextField(_(u"Текст"))
    sender = models.ForeignKey('UserProfile', related_name='sent_messages', verbose_name=_(u"Отправитель"))
    recipient = models.ForeignKey(
        'UserProfile', related_name='received_messages', verbose_name=_(u"Получатель"))
    parent_msg = models.ForeignKey(
        'self', related_name='next_messages', null=True, blank=True, verbose_name=_(u"Parent message"))
    sent_at = models.DateTimeField(_(u"Отправленно"), auto_now_add=True)
    read_at = models.DateTimeField(_(u"read at"), null=True, blank=True)
    replied_at = models.DateTimeField(_(u"replied at"), null=True, blank=True)
    sender_deleted_at = models.DateTimeField(_(u"Sender deleted at"), null=True, blank=True)
    recipient_deleted_at = models.DateTimeField(_(u"Recipient deleted at"), null=True, blank=True)

    objects = MessageManager()

    def new(self):
        """returns whether the recipient has read the message or not"""
        if self.read_at is not None:
            return False
        return True

    def replied(self):
        """returns whether the recipient has written a reply to this message"""
        if self.replied_at is not None:
            return True
        return False

    def __unicode__(self):
        return self.subject

    class Meta:
        ordering = ['-sent_at']
        verbose_name = _(u"Личное сообщение")
        verbose_name_plural = _(u"Личные сообщения")


def inbox_count_for(user):
    """
    returns the number of unread messages for the given user but does not
    mark them seen
    """
    return Message.objects.filter(recipient=user, read_at__isnull=True, recipient_deleted_at__isnull=True).count()