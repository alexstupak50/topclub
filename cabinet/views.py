# -*- coding:utf-8 -*-
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.shortcuts import redirect
from django.views.generic import ListView, FormView, View
from django.core.urlresolvers import reverse, reverse_lazy
from itertools import groupby

from cabinet.forms import ProfileEditForm, MessageCreateForm
from cabinet.models import Message, UserProfile
from catalog.models import Advert
from catalog.views import advert_add, get_adverts, get_adv_params
from core.utils import paginate_object
from comments.utils import paginate_objects as paginate_comments
from event.mixins import LoginRequiredMixin
from event.models import Event

@login_required
def view(request):
    return render(request, "cabinet.html", {})


def pre_adverts_view(request, template_name, public=False):
    params = {'user': request.user, 'public': public}
    adverts = get_adverts(params)

    adverts = adverts.select_related(
        'cover', 'city', 'region'
        ).order_by('-public_date')

    adverts = paginate_object(adverts, 1, cache_key=counted_hash, page=page)
    #TODO здесь кто-то удалил лишнее, нужно вернуть

@login_required
def drafts(request):
    """
    Returns list of unpublished drafts
    """
    page = request.GET.get('page')
    count_cache_key = 'drafts_%d' % request.user.id
    params = {'user': request.user, 'public': False}

    adverts = get_adverts(params)
    adverts = adverts.select_related(
        'cover', 'city', 'region'
        ).order_by('-changed_last')

    adverts = paginate_object(adverts, 20, cache_key=count_cache_key, page=page)
    return render(request, "drafts.html", {'adverts': adverts})


@login_required
def adverts(request):
    """
    Returns list of published adverts
    """
    page = request.GET.get('page')
    count_cache_key = 'adverts_%d' % request.user.id
    params = {'user': request.user, 'public': True}

    adverts = get_adverts(params)
    adverts = adverts.select_related(
        'cover', 'city', 'region'
        ).order_by('-public_date')

    adverts = paginate_object(adverts, 20, cache_key=count_cache_key, page=page)
    return render(request, "adverts.html", {'adverts': adverts})


@login_required
def draft_edit(request, pk):
    return advert_add(request, pk, blank=False)


@login_required
def draft_delete(request, pk):
    advert = get_object_or_404(Advert,
                               blank=False,
                               public=False,
                               **get_adv_params(request, pk))
    advert.delete()
    messages.success(request, _(u'Запись успешно удалена.'))
    # return redirect(reverse('cabinet:drafts'))
    return drafts(request)


@login_required
def profile_edit(request):
    """
    Provide profile edit form
    """
    form = ProfileEditForm(request.POST or None, request.FILES or None, instance=request.user)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            messages.success(request, _(u'Данные успешно обновлены.'))
            return redirect(reverse('cabinet:view'))

    return render(request, "profile_edit.html", {'form': form})


@login_required
def comments_list(request):
    """
    Returns list of published comments
    """
    page = request.GET.get('page')
    comments = paginate_comments(request.user.comment_set.all().select_related('content_object'),
                                 20,
                                 page=page)

    # cache generic objects
    related_obj_cts = dict()
    for comment in comments:
        related_obj_cts.setdefault(comment.content_type_id,
                                   set()).add(comment.object_id)
    related_objects = dict()
    for ct, ids in related_obj_cts.iteritems():
        related_objects[ct] = ContentType.objects.get_for_id(
            ct).model_class().objects.in_bulk(ids)
    for comment in comments:
        try:
            comment.related_object = related_objects[comment.content_type_id][comment.object_id]
        except KeyError:
            pass
    return render(request, "profile_comments.html", {'comments': comments})

# def get_profile(request, pk):
#     if request.user.is_authenticated() and request.user.pk == int(pk):
#         return redirect('userprofile:get_self')
#     else:
#         return render(request, 'profile_view.html',
#                                 {'user': get_object_or_404(UserProfile, pk=pk)})


@login_required
def events(request):
    """
    Returns list of published events
    """
    page = request.GET.get('page')
    count_cache_key = 'event_%d' % request.user.id

    qs = Event.objects.get_actual().filter(user=request.user)
    ctx = {
        'events': paginate_object(qs, 20, cache_key=count_cache_key, page=page)
    }

    return render(request, "my_events.html", ctx)


@login_required
def events_archive(request):
    """
    Returns list of published events
    """
    page = request.GET.get('page')
    count_cache_key = 'event_%d' % request.user.id
    qs = Event.objects.get_archive().filter(user=request.user)
    ctx = {
        'events': paginate_object(qs, 20, cache_key=count_cache_key, page=page)
    }

    return render(request, "my_archive.html", ctx)


class MessageListView(LoginRequiredMixin, ListView):
    model = Message
    template_name = 'my_messages.html'
    paginate_by = 20
    context_object_name = 'messages_list'

    def get_queryset(self):
        user_messages = Message.objects.get_messages(self.request.user)
        queryset = []
        keys = []

        def get_key(message):
            if message.sender == self.request.user:
                return message.recipient
            else:
                return message.sender

        for key, group in groupby(user_messages, get_key):
            if key not in keys:
                temp = sorted([x for x in group], key=lambda m: m.sent_at)
                queryset.append({
                    'is_new': any([x.new() for x in temp if x.recipient == self.request.user]),
                    'user': key,
                    'last_message': temp[-1] if temp else None
                })
                keys.append(key)
        return queryset


class MessageDialogListView(LoginRequiredMixin, ListView):
    model = Message
    template_name = 'message_detail.html'
    paginate_by = 20
    context_object_name = 'messages_list'

    def get_queryset(self):
        self.user2 = get_object_or_404(UserProfile, username=self.kwargs.get('username'))
        return Message.objects.get_dialog(self.request.user, self.user2)

    def get_context_data(self, **kwargs):
        ctx = super(MessageDialogListView, self).get_context_data(**kwargs)
        object_list = ctx['object_list']
        ctx['is_new_list'] = is_new_list = [x.id for x in object_list if not x.read_at]
        ctx['user2'] = self.user2
        ctx['form'] = MessageCreateForm(
            self.request.POST or None, user=self.request.user, initial={'to': self.user2.username})
        self.model.objects.filter(
            id__in=is_new_list, recipient=self.request.user).update(read_at=timezone.now())
        return ctx


class MessageCreateView(LoginRequiredMixin, FormView):
    form_class = MessageCreateForm
    template_name = 'message_new.html'
    success_url = reverse_lazy('cabinet:messages')

    def get_context_data(self, **kwargs):
        ctx = super(MessageCreateView, self).get_context_data(**kwargs)
        try:
            ctx['recipient'] = UserProfile.objects.get(username=self.request.GET.get('to'))
        except UserProfile.DoesNotExist:
            pass
        return ctx

    def get_form_kwargs(self):
        kwargs = super(MessageCreateView, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        form.save()
        if 'dialog' in self.request.META.get('HTTP_REFERER', ''):
            self.success_url = self.request.META.get('HTTP_REFERER')
        messages.success(self.request, _(u'Сообщение успешно отправлено'))
        return super(MessageCreateView, self).form_valid(form)

    def get_initial(self):
        initial = super(MessageCreateView, self).get_initial()
        if self.request.GET.get('to'):
            initial['to'] = self.request.GET.get('to')
        return initial


class DeleteDialogView(LoginRequiredMixin, View):
    model = Message
    success_url = reverse_lazy('cabinet:messages')

    def get(self, request, *args, **kwargs):
        user2 = get_object_or_404(UserProfile, username=self.kwargs.get('username'))
        dialog = Message.objects.get_dialog(self.request.user, user2)
        for message in dialog:
            if message.recipient == self.request.user:
                message.recipient_deleted_at = timezone.now()
            else:
                message.sender_deleted_at = timezone.now()
            message.save()
        messages.success(self.request, _(u'Диалог успешно удалено'))
        return redirect(self.success_url)


class DeleteMessageView(LoginRequiredMixin, View):
    model = Message

    def get(self, request, *args, **kwargs):
        message = get_object_or_404(Message, id=self.kwargs.get('pk'))
        if message.recipient == self.request.user:
            message.recipient_deleted_at = timezone.now()
        else:
            message.sender_deleted_at = timezone.now()
        message.save()
        messages.success(self.request, _(u'Сообщение успешно удалено'))
        return redirect(self.request.META.get('HTTP_REFERER', reverse_lazy('cabinet:messages')))


class DeleteAccount(LoginRequiredMixin, View):

     def get(self, request, *args, **kwargs):
        self.request.user.delete()
        messages.success(self.request, _(u'Профиль успешно удален'))
        return redirect('/')