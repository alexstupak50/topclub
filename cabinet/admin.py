# -*- coding: utf-8 -*-
from django.contrib import admin
from cabinet.models import Message
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import ugettext_lazy as _
from .models import UserProfile


class MessageAdmin(admin.ModelAdmin):
    list_display = ('sender', 'recipient', 'subject', 'sent_at')
    date_hierarchy = 'sent_at'
    readonly_fields = ('sender', 'recipient', 'sent_at')
    exclude = ('parent_msg', )
    fieldsets = (
        (None,
            {'fields': (('sender', 'recipient'), 'sent_at')
             }),
        (None,
            {'fields': ('subject', 'body')}),
    )


class UserAdmin(BaseUserAdmin):
    list_display = ('username', 'email', 'first_name', 'last_name', 'is_staff', 'admin_region')
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions', 'admin_region')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )

    # inlines = [TransportInline, NotificationInline, RouteInline, PaymentsInline]
    # form = UserAdminForm
    # add_form = UserAdminCreationForm
    # filter_horizontal = ('categories',)

    # fieldsets = (
    #     (None, {'fields': ('email', 'username', 'password')}),
    #     (_(u'Персональные данные'), {'fields': (
    #         'first_name', 'last_name', 'phone', 'phone_confirmed', 'cash',
    #         'currency')}),
    #     (_(u'Права'), {'fields': (
    #         'is_active', 'is_staff', 'is_superuser', 'groups',
    #         'user_permissions')}),
    #     (_(u'Важные даты'), {'fields': ('last_login', 'date_joined')}),
    #     (_(u'Информация заказчика'), {'fields': ('is_customer',)}),
    #     (_(u'Информация перевозчика'), {'fields': (
    #         'is_carrier', 'is_pro', 'pro_end', 'rating', 'review_pos_count',
    #         'review_neg_count', 'type_carrier', 'country',
    #         'region', 'restrict_orders', 'information', 'categories')}),
    #     (_(u'Прочее'), {'fields': ('is_slider',)}),
    # )
    # readonly_fields = ('rating', 'review_pos_count', 'review_neg_count',)
    # add_fieldsets = (
    #     (None, {
    #         'classes': ('wide',),
    #         'fields': ('email', 'password1', 'password2'),
    #     }),
    # )

    # list_display = ('email', 'first_name', 'is_customer', 'is_carrier')
    # list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups',
    #                'is_customer', 'is_carrier', 'is_pro', 'is_slider')
    # search_fields = ('email', 'first_name', 'last_name')
    # ordering = ('email',)


admin.site.unregister(UserProfile)
admin.site.register(UserProfile, UserAdmin)
admin.site.register(Message, MessageAdmin)