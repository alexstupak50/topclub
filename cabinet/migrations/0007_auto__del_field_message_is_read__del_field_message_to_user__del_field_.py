# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Message.is_read'
        db.delete_column(u'cabinet_message', 'is_read')

        # Deleting field 'Message.to_user'
        db.delete_column(u'cabinet_message', 'to_user_id')

        # Deleting field 'Message.user'
        db.delete_column(u'cabinet_message', 'user_id')

        # Deleting field 'Message.created'
        db.delete_column(u'cabinet_message', 'created')

        # Deleting field 'Message.text'
        db.delete_column(u'cabinet_message', 'text')

        # Adding field 'Message.body'
        db.add_column(u'cabinet_message', 'body',
                      self.gf('django.db.models.fields.TextField')(default=1),
                      keep_default=False)

        # Adding field 'Message.sender'
        db.add_column(u'cabinet_message', 'sender',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, related_name='sent_messages', to=orm['cabinet.UserProfile']),
                      keep_default=False)

        # Adding field 'Message.recipient'
        db.add_column(u'cabinet_message', 'recipient',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, related_name='received_messages', to=orm['cabinet.UserProfile']),
                      keep_default=False)

        # Adding field 'Message.parent_msg'
        db.add_column(u'cabinet_message', 'parent_msg',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='next_messages', null=True, to=orm['cabinet.Message']),
                      keep_default=False)

        # Adding field 'Message.sent_at'
        db.add_column(u'cabinet_message', 'sent_at',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, default=datetime.datetime(2015, 3, 27, 0, 0), blank=True),
                      keep_default=False)

        # Adding field 'Message.read_at'
        db.add_column(u'cabinet_message', 'read_at',
                      self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Message.replied_at'
        db.add_column(u'cabinet_message', 'replied_at',
                      self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Message.sender_deleted_at'
        db.add_column(u'cabinet_message', 'sender_deleted_at',
                      self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Message.recipient_deleted_at'
        db.add_column(u'cabinet_message', 'recipient_deleted_at',
                      self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True),
                      keep_default=False)


        # Changing field 'Message.subject'
        db.alter_column(u'cabinet_message', 'subject', self.gf('django.db.models.fields.CharField')(max_length=120))

    def backwards(self, orm):
        # Adding field 'Message.is_read'
        db.add_column(u'cabinet_message', 'is_read',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Message.to_user'
        db.add_column(u'cabinet_message', 'to_user',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, related_name='messages_to', to=orm['cabinet.UserProfile']),
                      keep_default=False)

        # Adding field 'Message.user'
        db.add_column(u'cabinet_message', 'user',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, related_name='messages', to=orm['cabinet.UserProfile']),
                      keep_default=False)

        # Adding field 'Message.created'
        db.add_column(u'cabinet_message', 'created',
                      self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now),
                      keep_default=False)

        # Adding field 'Message.text'
        db.add_column(u'cabinet_message', 'text',
                      self.gf('django.db.models.fields.TextField')(default=1),
                      keep_default=False)

        # Deleting field 'Message.body'
        db.delete_column(u'cabinet_message', 'body')

        # Deleting field 'Message.sender'
        db.delete_column(u'cabinet_message', 'sender_id')

        # Deleting field 'Message.recipient'
        db.delete_column(u'cabinet_message', 'recipient_id')

        # Deleting field 'Message.parent_msg'
        db.delete_column(u'cabinet_message', 'parent_msg_id')

        # Deleting field 'Message.sent_at'
        db.delete_column(u'cabinet_message', 'sent_at')

        # Deleting field 'Message.read_at'
        db.delete_column(u'cabinet_message', 'read_at')

        # Deleting field 'Message.replied_at'
        db.delete_column(u'cabinet_message', 'replied_at')

        # Deleting field 'Message.sender_deleted_at'
        db.delete_column(u'cabinet_message', 'sender_deleted_at')

        # Deleting field 'Message.recipient_deleted_at'
        db.delete_column(u'cabinet_message', 'recipient_deleted_at')


        # Changing field 'Message.subject'
        db.alter_column(u'cabinet_message', 'subject', self.gf('django.db.models.fields.CharField')(max_length=255))

    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'cabinet.message': {
            'Meta': {'ordering': "['-sent_at']", 'object_name': 'Message'},
            'body': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'parent_msg': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'next_messages'", 'null': 'True', 'to': u"orm['cabinet.Message']"}),
            'read_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'recipient': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'received_messages'", 'to': u"orm['cabinet.UserProfile']"}),
            'recipient_deleted_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'replied_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'sender': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'sent_messages'", 'to': u"orm['cabinet.UserProfile']"}),
            'sender_deleted_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'sent_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        u'cabinet.userprofile': {
            'Meta': {'object_name': 'UserProfile'},
            'avatar': ('django.db.models.fields.files.ImageField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'by_line': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True', 'blank': 'True'}),
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.LocCity']", 'null': 'True'}),
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'phone': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.LocRegion']", 'null': 'True'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'core.loccity': {
            'Meta': {'object_name': 'LocCity'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.LocCountry']"}),
            'default': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'important': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'lat': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'lng': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.LocRegion']"}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'core.loccountry': {
            'Meta': {'object_name': 'LocCountry'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'core.locregion': {
            'Meta': {'object_name': 'LocRegion'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.LocCountry']"}),
            'default_city': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.LocCity']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        }
    }

    complete_apps = ['cabinet']