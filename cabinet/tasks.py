# -*- encoding: utf-8 -*-

from django.utils import timezone
from celery import shared_task
from dateutils import relativedelta
from core.helpers import site_settings


@shared_task
def clean_messages():
    from cabinet.models import Message
    x_date = timezone.now() - relativedelta(
        days=site_settings.MESSAGE_DELETE_DELTA)
    return Message.objects.filter(sent_at__lt=x_date).delete()
