# -*- coding:utf-8 -*-
from django.conf.urls import patterns, url
from .views import MessageListView, MessageCreateView, \
    MessageDialogListView, DeleteDialogView, DeleteMessageView, DeleteAccount

urlpatterns = patterns('cabinet.views',
    url(r'^$', 'view', name="view"),
    url(r'^edit/$', 'profile_edit', name='edit'),
    url(r'^drafts/$', 'drafts', name="drafts"),
    url(r'^draft/(?P<pk>\d+)/$', 'draft_edit', name='draft_edit'),
    url(r'^draft/(?P<pk>\d+)/delete/$', 'draft_delete', name='draft_delete'),
    url(r'^adverts/$', 'adverts', name="adverts"),
    url(r'^events/$', 'events', name='events'),
    url(r'^archive/$', 'events_archive', name='archive'),
    url(r'^comments/$', 'comments_list', name='comments'),
    url(r'^messages/$', MessageListView.as_view(), name='messages'),
    url(r'^messages/dialog/(?P<username>[\w-]+)/$', MessageDialogListView.as_view(), name='dialog'),
    url(r'^messages/dialog/(?P<username>[\w-]+)/delete/$', DeleteDialogView.as_view(), name='dialog_delete'),
    url(r'^messages/(?P<pk>\d+)/delete/$', DeleteMessageView.as_view(), name='message_delete'),
    url(r'^message/new/$', MessageCreateView.as_view(), name='message_new'),
    url(r'^delete/$', DeleteAccount.as_view(), name='delete_account'),

    # url(r'^draft/(?P<pk>\d+)/$', 'draft_view', name="draft_view"),
    # url(r'^(?P<pk>\d+)/$', 'get_profile', name='view'),
)
