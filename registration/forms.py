# -*- coding:utf-8 -*-
from django import forms
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import AuthenticationForm, PasswordResetForm
from django.core.validators import validate_email
from django.conf import settings
from django.core.mail import send_mail
from django.contrib.sites.models import Site
from django.utils.translation import ugettext_lazy as _


from captcha.fields import CaptchaField
from cabinet.models import UserProfile as User

# from hashlib import md5


class PasswordCheckMixin(object):
    MIN_LENGTH = 6

    _error_messages = {
        'password_mismatch': _("Введенные пароли не совпадают."),
        'light_password': u"Пароль недостаточно сложный. Обязательно наличие\
                            букв верхнего и нижнего регистра, а так-же цифр.\
                            Минимальная длина %d символов" % MIN_LENGTH,
    }

    def check_password_safety(self, field='password1'):
        password1 = self.cleaned_data.get(field)

        if password1:
            password = unicode(password1)
            if len(password) < self.MIN_LENGTH:
                raise forms.ValidationError(self._error_messages['light_password'])
            digit = alpha = lower = upper = extra = False
            for x in password:
                if x.isdigit():
                    digit = True
                elif x.isalpha():
                    alpha = True
                if x.islower():
                    lower = True
                elif x.isupper():
                    upper = True
                if x in u'\'"/<>‘':
                    extra = True
            if not (digit and lower and alpha) or extra:  # and upper
                raise forms.ValidationError(self._error_messages['light_password'])
        return password1

    def check_password_mismatch(self, field1='password1', field2='password2'):
        password1 = self.cleaned_data.get(field1)
        password2 = self.cleaned_data.get(field2)
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self._error_messages['password_mismatch'],
                    code='password_mismatch',
                )
        return password2


class EmailCheckMixin(object):
    email_error_messages = {
        'used_email': u'Этот адрес уже используется. Пожалуйста введите другой\
                             или воспользуйтесь формой восстановления пароля', }

    def clean_email(self):
        """
        Validate that the supplied email address is unique for the
        site.
        """
        if User.objects.filter(email__iexact=self.cleaned_data['email']):
            raise forms.ValidationError(self.email_error_messages['used_email'])
        return self.cleaned_data['email']


class RegistrationForm(forms.Form, PasswordCheckMixin, EmailCheckMixin):
    MIN_LENGTH = 6

    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
        'light_password': u"Пароль недостаточно сложный. Обязательно наличие\
                            верхнего и нижнего регистра, наличие цифр и букв.\
                            Минимально %d символов" % MIN_LENGTH,
    }

    error_css_class = 'error'
    required_css_class = 'required'

    username = forms.RegexField(regex=r'^[\w.-]+$',
                                max_length=30,
                                label='',
                                error_messages={'invalid': _(u'Это значение может содержать только буквы, цифры и символы: -._ ')},
                                widget=forms.TextInput(attrs={'placeholder': _(u'Логин'), 'pattern': '^[\w.-]{3,30}$'}))
    email = forms.EmailField(label='', widget=forms.TextInput(attrs={'placeholder': _(u'E-mail')}))
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': _(u'Пароль')}), label='')
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': _(u'Повторите пароль')}), label='')
    captcha = CaptchaField(label=_(u'Введите цифры и буквы'))

    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({'required': ''})
        # self.fields['password1'].widget.attrs.update({'placeholder': _(u'Пароль'), 'type': 'password'})

    def clean_username(self):
        existing = User.objects.filter(username__iexact=self.cleaned_data['username'])
        if existing.exists():
            raise forms.ValidationError(u'Пользователь с таким логином уже существует')
        else:
            return self.cleaned_data['username']

    def clean_password1(self):
        return self.check_password_safety()

    def clean_password2(self):
        return self.check_password_mismatch()

    def create_user(self):
        try:
            username = self.cleaned_data['username']
            email = self.cleaned_data['email']
            password = self.cleaned_data['password1']
            new_user = User.objects.create_user(username, email, password)
            new_user.save()

            current_site = Site.objects.get(pk=settings.SITE_ID)
            send_mail(u'Регистрация на сайте %s' % current_site.name,
                      u'Здравствуйте. Вы успешно зарегистрировались на сайте %s.\nВаш логин: %s\n\nУдачного дня' % (current_site.domain, username),
                      settings.DEFAULT_FROM_EMAIL,
                      [email])
        except:
            pass


class UserAuthenticationForm(AuthenticationForm):
    required_css_class = 'required'
    message_incorrect_password = u'Пожалуйста введите корректный логин и пароль.'
    message_inactive = u'Этот аккаунт не активирован.'

    username = forms.CharField(label='', max_length=254, widget=forms.TextInput(attrs={'placeholder': u'Логин или e-mail'}))
    password = forms.CharField(label='', widget=forms.PasswordInput(attrs={'placeholder': u'Пароль'}))

    def __init__(self, request=None, *args, **kwargs):
        super(UserAuthenticationForm, self).__init__(request, *args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({'required': ''})

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            self.user_cache = authenticate(username=username, password=password)
            if self.user_cache is None:
                raise forms.ValidationError(self.message_incorrect_password)
            if self.user_cache is not None and not self.user_cache.is_active:
                raise forms.ValidationError(self.message_inactive)
            self.check_for_test_cookie()
            login(self.request, self.user_cache)
        return self.cleaned_data


class PasswordRecoveryForm(PasswordResetForm):
    required_css_class = 'required'

    email = forms.EmailField(label='', max_length=254)

    def __init__(self, *args, **kwargs):
        super(PasswordRecoveryForm, self).__init__(*args, **kwargs)
        self.fields['email'].widget.attrs.update({'placeholder': u'Ваш e-mail', 'required': ''})

    def clean_email(self):
        validate_email(self.cleaned_data['email'])
        try:
            User._default_manager.get(email__iexact=self.cleaned_data['email'], is_active=True)
        except User.DoesNotExist:
            raise forms.ValidationError(u'Указанный e-mail не зарегистрирован либо деактивирован.')
        return self.cleaned_data['email']


class RequireUsernameAndEmailForm(forms.Form):
    username = forms.CharField(label=_(u'логин/username'), max_length=30)
    email = forms.EmailField(label=_(u'email'))

    def clean_username(self):
        value = self.cleaned_data['username']
        if User.objects.filter(username=value).exists():
            raise forms.ValidationError(_(u'Этот логин занят'))
        return value

    def clean_email(self):
        value = self.cleaned_data['email']
        if User.objects.filter(email=value).exists():
            raise forms.ValidationError(_(u'Этот email занят'))
        return value
