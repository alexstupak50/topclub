# -*- coding: utf-8 -*-
import json
from django.http import Http404, HttpResponse
from django.utils.translation import ugettext as _
from django.contrib.auth import (REDIRECT_FIELD_NAME,
                                 logout as auth_logout)
from django.contrib.sites.models import get_current_site
from django.http import HttpResponseRedirect
from django.utils.http import is_safe_url
from django.shortcuts import resolve_url, redirect
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.views.generic import TemplateView, FormView

from .forms import RegistrationForm, UserAuthenticationForm, \
    PasswordRecoveryForm, RequireUsernameAndEmailForm

from captcha.models import CaptchaStore
from captcha.helpers import captcha_image_url


def registration(request):
    if request.is_ajax and request.POST:
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.create_user()
            data = json.dumps({'success': True})
        else:
            to_json_responce = {}
            to_json_responce['form_errors'] = form.errors
            to_json_responce['new_cptch_key'] = CaptchaStore.generate_key()
            to_json_responce['new_cptch_image'] = captcha_image_url(
                to_json_responce['new_cptch_key'])
            data = json.dumps(to_json_responce)
        return HttpResponse(data, mimetype='application/json')

    raise Http404


def authentication(request):
    if request.is_ajax and request.POST:
        form = UserAuthenticationForm(request, data=request.POST)
        if form.is_valid():
            data = json.dumps({'success': True,
                               'username': form.data['username']})
        else:
            to_json_responce = {}
            to_json_responce['form_errors'] = form.errors
            data = json.dumps(to_json_responce)
        return HttpResponse(data, mimetype='application/json')

    raise Http404


def password_reset(request):
    if request.is_ajax and request.POST:
        form = PasswordRecoveryForm(request.POST)
        if form.is_valid():
            form.save()
            data = json.dumps({'success': True})
        else:
            to_json_responce = {}
            to_json_responce['form_errors'] = form.errors
            data = json.dumps(to_json_responce)
        return HttpResponse(data, mimetype='application/json')

    raise Http404


def logout(request, next_page=None,
           template_name='registration/logged_out.html',
           redirect_field_name=REDIRECT_FIELD_NAME,
           current_app=None, extra_context=None):
    """
    Logs out the user and displays 'You are logged out' message.
    """
    auth_logout(request)

    if next_page is not None:
        next_page = resolve_url(next_page)

    if (redirect_field_name in request.POST or
            redirect_field_name in request.GET):
        next_page = request.POST.get(redirect_field_name,
                                     request.GET.get(redirect_field_name))
        # Security check -- don't allow redirection to a different host.
        if not is_safe_url(url=next_page, host=request.get_host()):
            next_page = request.path

    if next_page:
        # Redirect to this page until the session has been cleared.
        return HttpResponseRedirect(next_page)

    current_site = get_current_site(request)
    context = {
        'site': current_site,
        'site_name': current_site.name,
        'title': _('Logged out')
    }
    if extra_context is not None:
        context.update(extra_context)
    return HttpResponseRedirect('/')


def required_username_and_email(request):
    form = RequireUsernameAndEmailForm(request.POST or None)
    ctx = {}
    template_name = 'require_username_and_email.html'
    if request.session.get('vk_redirect'):
        return redirect('/')
    if form.is_valid():
        cd = form.cleaned_data
        try:
            backend = request.session['social_auth_last_login_backend']
        except KeyError:
            backend = 'vk-oauth2'
        if backend == 'facebook':
            request.user.username = cd['username']
            request.user.email = cd['email']
            request.user.save()
            return redirect('/')
        ctx['backend'] = backend
        ctx['form'] = form
        ctx['is_valid'] = True
        ctx.update(cd)
        request.session['vk_redirect'] = True
        request.session.modified = True
        return render_to_response(template_name, ctx, RequestContext(request))
    return render_to_response(template_name, {'form': form}, RequestContext(request))

# class RequireUsernameAndEmailView(FormView):
#     template_name = 'require_username_and_email.html'
#     form_class = RequireUsernameAndEmailForm
#
#     def get_context_data(self, **kwargs):
#         ctx = super(
#             RequireUsernameAndEmailView, self).get_context_data(**kwargs)
#         try:
#             # import ipdb; ipdb.set_trace()
#             ctx['backend'] = self.request.session['social_auth_last_login_backend']
#         except KeyError:
#             ctx['backend'] = 'vk-oauth2'
#         print ctx['backend']
#         return ctx
#
#     def form_valid(self, form):
#         ctx = self.get_context_data(form=form, is_valid=True)
#         return render_to_response(self.template_name, ctx,
#                                   RequestContext(self.request))