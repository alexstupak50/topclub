from django import template

from registration.forms import RegistrationForm, UserAuthenticationForm,\
                               PasswordRecoveryForm

register = template.Library()


@register.assignment_tag
def registration_form():
    return RegistrationForm()


@register.assignment_tag
def authentication_form():
    return UserAuthenticationForm()


@register.assignment_tag
def password_recovery_form():
    return PasswordRecoveryForm()
