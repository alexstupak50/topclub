# -*- coding:utf-8 -*-
from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import (UserChangeForm,
                                       UserCreationForm)
from django.template.loader import render_to_string

from sorl.thumbnail.admin.current import AdminImageWidget

from registration.forms import PasswordCheckMixin,\
                              EmailCheckMixin
from cabinet.models import UserProfile as User


class AdminUserCreationForm(UserCreationForm,
                            PasswordCheckMixin,
                            EmailCheckMixin):
    """
    A form that creates a user, with no privileges, from the given e-mail,
                                                                 and password.
    """
    email_error_messages = {
        'used_email': u'Этот адрес уже используется. Пожалуйста введите другой.',
    }

    class Meta:
        model = User
        fields = ('username', 'email')

    def clean_username(self):
        '''
        We redefine this to avoid django lookup users in standart auth model
        '''
        # Since User.username is unique, this check is redundant,
        # but it sets a nicer error message than the ORM. See #13147.
        username = self.cleaned_data["username"]
        try:
            User._default_manager.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(
            self.error_messages['duplicate_username'],
            code='duplicate_username',
        )

    def clean_password1(self):
        return self.check_password_safety()

    def clean_password2(self):
        return self.check_password_mismatch()


class AdminUserChangeForm(UserChangeForm):
    avatar = forms.ImageField(widget=AdminImageWidget, required=False)

    class Meta:
        model = User
        fields = '__all__'


class CustomUserAdmin(UserAdmin):
    # add_form_template = 'admin/admin_user_add_form.html'
    form = AdminUserChangeForm
    add_form = AdminUserCreationForm
    list_display = ('__unicode__', 'thumb',  'is_staff')
    readonly_fields = ('last_login', 'date_joined', )
    ordering = ('-date_joined',)
    filter_horizontal = ('groups', 'user_permissions', )
    fieldsets = (
        (None, {'fields': ('username', 'email', 'password')}),
        (u'Личные данные', {'fields': ('first_name', 'last_name', 'phone')}),
        (None, {'fields': ('avatar', 'by_line',)}),
        (u'Права доступа', {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (u'Важные даты', {'fields': (('date_joined', 'last_login'), )}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'email', 'password1', 'password2'),
        }),
    )

    def thumb(self, obj):
        return render_to_string('admin/thumb.html', {
            'image': obj.avatar, 'size': '100x100'
            })
    thumb.short_description = (u'аватар')
    thumb.allow_tags = True

admin.site.register(User, CustomUserAdmin)
