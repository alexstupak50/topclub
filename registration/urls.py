from django.conf.urls import patterns, include, url
from django.contrib.auth import views

# from registration.views import RequireUsernameAndEmailView

urlpatterns = patterns('',
    url(r'^password_change/$', views.password_change, name='password_change'),
    url(r'^password_change/done/$', views.password_change_done, name='password_change_done'),
    # url(r'^password_reset/$', views.password_reset, name='password_reset'),
    url(r'^password_reset/done/$', views.password_reset_done, name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.password_reset_confirm, name='password_reset_confirm'),
    url(r'^reset/done/$', views.password_reset_complete, name='password_reset_complete'),

    # (r'', include('django.contrib.auth.urls')),
    url(r'^registration/$', 'registration.views.registration', name='registration'),
    url(r'^authentication/$', 'registration.views.authentication', name='authentication'),
    url(r'^password-reset/$', 'registration.views.password_reset', name='password_reset'),
    url(r'^logout/$', 'registration.views.logout', name='logout'),
    # url(r'^registration/social/$', RequireUsernameAndEmailView.as_view(), name='require_username_and_email'),
    url(r'^registration/social/$', 'registration.views.required_username_and_email', name='require_username_and_email'),
)
