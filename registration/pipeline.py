# -*- encoding: utf-8 -*-
from django.shortcuts import redirect
from requests import request, HTTPError
from django.core.files.base import ContentFile
from social.backends.facebook import FacebookOAuth2

from social.pipeline.partial import partial


# @partial
def require_username_and_email(strategy, details, user=None, is_new=False, *args, **kwargs):
    if kwargs.get('ajax') or (user and user.email and user.username):
        return
    elif is_new and (not details.get('email') or not details.get('username')):
        username = strategy.request_data().get('username')
        email = strategy.request_data().get('email')
        if email and username:
            details['email'] = email
            details['username'] = username
        else:
            return redirect('require_username_and_email')

@partial
def save_profile_picture(strategy, user, response, details, is_new=False, *args, **kwargs):

    if is_new:
        try:
            url = 'http://graph.facebook.com/{0}/picture'.format(response['id'])

            try:
                response = request('GET', url, params={'type': 'large'})
                response.raise_for_status()
            except HTTPError:
                pass
            else:
                profile = user
                profile.avatar.save('{0}_social.jpg'.format(user.username),
                                    ContentFile(response.content))
                profile.save()
        except (KeyError, AttributeError):
            pass