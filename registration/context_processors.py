# -*- encoding: utf-8 -*-

from django.conf import settings


def social(request):
    return {
        'VK_API_ID': getattr(settings, 'SOCIAL_AUTH_VK_OAUTH2_KEY', None)
    }

