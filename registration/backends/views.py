# -*- coding:utf-8 -*-
import hashlib
import random

from django.conf import settings
from django.contrib.sites.models import RequestSite, get_current_site
from django.contrib.sites.models import Site
# from django.contrib.auth import REDIRECT_FIELD_NAME, login as auth_login
from django.contrib.auth import (REDIRECT_FIELD_NAME,
                                 logout as auth_logout)
# from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.tokens import default_token_generator
from django.core.urlresolvers import reverse
from django.shortcuts import resolve_url, redirect
from django.views.decorators.csrf import csrf_protect
# from django.views.decorators.debug import sensitive_post_parameters
# from django.views.decorators.cache import never_cache
from django.template.response import TemplateResponse
from django.http import HttpResponseRedirect
from django.utils.http import is_safe_url
from django.utils.translation import ugettext as _

# from registration import signals
# from registration.views import RegistrationView as BaseRegistrationView
# from registration.backends.default.views import ActivationView as BaseActivationView
# from registration.models import RegistrationProfile

from cabinet.models import UserProfile as User
# from cabinet.forms import CustomPasswordResetForm


def logout(request, next_page=None,
           template_name='registration/logged_out.html',
           redirect_field_name=REDIRECT_FIELD_NAME,
           current_app=None, extra_context=None):
    """
    Logs out the user and displays 'You are logged out' message.
    """
    auth_logout(request)

    if next_page is not None:
        next_page = resolve_url(next_page)

    if (redirect_field_name in request.POST or
            redirect_field_name in request.GET):
        next_page = request.POST.get(redirect_field_name,
                                     request.GET.get(redirect_field_name))
        # Security check -- don't allow redirection to a different host.
        if not is_safe_url(url=next_page, host=request.get_host()):
            next_page = request.path

    if next_page:
        # Redirect to this page until the session has been cleared.
        return HttpResponseRedirect(next_page)

    current_site = get_current_site(request)
    context = {
        'site': current_site,
        'site_name': current_site.name,
        'title': _('Logged out')
    }
    if extra_context is not None:
        context.update(extra_context)
    return HttpResponseRedirect('/')
    # return TemplateResponse(request, template_name, context,
    #     current_app=current_app)


# @csrf_protect
# def password_reset(request, is_admin_site=False,
#                    template_name='registration/password_reset_form_default.html',
#                    ajax_template_name='registration/password_reset_ajax_form_default.html',
#                    email_template_name='registration/password_reset_email.html',
#                    subject_template_name='registration/password_reset_subject.txt',
#                    password_reset_form=CustomPasswordResetForm,
#                    token_generator=default_token_generator,
#                    post_reset_redirect=None,
#                    from_email=None,
#                    current_app=None,
#                    extra_context=None,
#                    html_email_template_name=None):
#     if post_reset_redirect is None:
#         post_reset_redirect = reverse('password_reset_done')
#     else:
#         post_reset_redirect = resolve_url(post_reset_redirect)
#     if request.method == "POST":
#         form = password_reset_form(request.POST)
#         if form.is_valid():
#             opts = {
#                 'use_https': request.is_secure(),
#                 'token_generator': token_generator,
#                 'from_email': from_email,
#                 'email_template_name': email_template_name,
#                 'subject_template_name': subject_template_name,
#                 'request': request,
#             }
#             if is_admin_site:
#                 opts = dict(opts, domain_override=request.get_host())
#             form.save(**opts)
#             return HttpResponseRedirect(post_reset_redirect)
#     else:
#         form = password_reset_form()
#     context = {
#         'form': form,
#         'title': _('Password reset'),
#     }
#     if extra_context is not None:
#         context.update(extra_context)
#     return custom_return(request, context, current_app,
#                                     ajax_template_name=ajax_template_name,
#                                     template_name=template_name)


# def custom_return(request, context, current_app,
#                 redirect_url=None,
#                 template_name=None,
#                 ajax_template_name='registration/redirect_ajax_succes.html'):
#     if request.is_ajax():
#         if redirect_url is not None:
#             context.update({'redirect_url': redirect_url})
#         return TemplateResponse(request, ajax_template_name, context,
#                                                         current_app=current_app)
#     else:
#         if redirect_url is not None:
#             return HttpResponseRedirect(redirect_url)
#         return TemplateResponse(request, template_name, context,
#                                                         current_app=current_app)


# class RegistrationView(BaseRegistrationView):
#     """
#     A registration backend which follows a simple workflow:

#     1. User signs up, inactive account is created.

#     2. Email is sent to user with activation link.

#     3. User clicks activation link, account is now active.

#     Using this backend requires that

#     * ``registration`` be listed in the ``INSTALLED_APPS`` setting
#       (since this backend makes use of models defined in this
#       application).

#     * The setting ``ACCOUNT_ACTIVATION_DAYS`` be supplied, specifying
#       (as an integer) the number of days from registration during
#       which a user may activate their account (after that period
#       expires, activation will be disallowed).

#     * The creation of the templates
#       ``registration/activation_email_subject.txt`` and
#       ``registration/activation_email.txt``, which will be used for
#       the activation email. See the notes for this backends
#       ``register`` method for details regarding these templates.

#     Additionally, registration can be temporarily closed by adding the
#     setting ``REGISTRATION_OPEN`` and setting it to
#     ``False``. Omitting this setting, or setting it to ``True``, will
#     be interpreted as meaning that registration is currently open and
#     permitted.

#     Internally, this is accomplished via storing an activation key in
#     an instance of ``registration.models.RegistrationProfile``. See
#     that model and its custom manager for full documentation of its
#     fields and supported operations.
#     """

#     def dispatch(self, request, *args, **kwargs):
#         """
#         Check that user signup is allowed before even bothering to
#         dispatch or do other processing.
#         """
#         if 'next' in request.GET:
#             self.redirect_on_activate = request.GET.get('next')
#         else:
#             self.redirect_on_activate = False
#         return super(RegistrationView, self).dispatch(request, *args, **kwargs)

#     def register(self, request, **cleaned_data):
#         """
#         Given a username, email address and password, register a new
#         user account, which will initially be inactive.

#         Along with the new ``User`` object, a new
#         ``registration.models.RegistrationProfile`` will be created,
#         tied to that ``User``, containing the activation key which
#         will be used for this account.

#         An email will be sent to the supplied email address; this
#         email should contain an activation link. The email will be
#         rendered using two templates. See the documentation for
#         ``RegistrationProfile.send_activation_email()`` for
#         information about these templates and the contexts provided to
#         them.

#         After the ``User`` and ``RegistrationProfile`` are created and
#         the activation email is sent, the signal
#         ``registration.signals.user_registered`` will be sent, with
#         the new ``User`` as the keyword argument ``user`` and the
#         class of this backend as the sender.

#         """
#         email = cleaned_data['email']
#         password = cleaned_data['password1']
#         first_name = cleaned_data['first_name']
#         last_name = cleaned_data['last_name']
#         phone = None
#         if 'phone' in cleaned_data:
#             phone = cleaned_data['phone']

#         if Site._meta.installed:
#             site = Site.objects.get_current()
#         else:
#             site = RequestSite(request)
#         new_user = self.create_inactive_user(email, password,
#                                              first_name, last_name,
#                                              phone, site)
#         signals.user_registered.send(sender=self.__class__,
#                                      user=new_user,
#                                      request=request)
#         return new_user

#     def registration_allowed(self, request):
#         """
#         Indicate whether account registration is currently permitted,
#         based on the value of the setting ``REGISTRATION_OPEN``. This
#         is determined as follows:

#         * If ``REGISTRATION_OPEN`` is not specified in settings, or is
#           set to ``True``, registration is permitted.

#         * If ``REGISTRATION_OPEN`` is both specified and set to
#           ``False``, registration is not permitted.
#         """
#         return getattr(settings, 'REGISTRATION_OPEN', True)

#     def get_success_url(self, request, user):
#         """
#         Return the name of the URL to redirect to after successful
#         user registration.
#         """
#         return ('registration_complete', (), {})

#     def create_inactive_user(self, email, password, first_name,
#                 last_name, phone, site, send_email=True):
#         """
#         Create a new, inactive ``User``, generate a
#         ``RegistrationProfile`` and email its activation key to the
#         ``User``, returning the new ``User``.

#         By default, an activation email will be sent to the new
#         user. To disable this, pass ``send_email=False``.
#         """
#         new_user = User.objects.create_user(email, password)
#         new_user.is_active = False
#         new_user.first_name, new_user.last_name = first_name, last_name
#         new_user.phone = phone
#         new_user.save()

#         registration_profile = self.create_profile(new_user)

#         if send_email:
#             registration_profile.send_activation_email(site)

#         return new_user

#     def create_profile(self, user):
#         """
#         Create a ``RegistrationProfile`` for a given
#         ``User``, and return the ``RegistrationProfile``.
#         The activation key for the ``RegistrationProfile`` will be a
#         SHA1 hash, generated from a combination of the ``User``'s
#         username and a random salt.
#         """
#         salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
#         email = user.email
#         if isinstance(email, unicode):
#             email = email.encode('utf-8')
#         activation_key = hashlib.sha1(salt + email).hexdigest()
#         return RegistrationProfile.objects.create(user=user,
#                            activation_key=activation_key)

#     def render_to_response(self, context, **response_kwargs):
#         """
#         Using custom render_to_response to set our redirect cookies
#         """
#         if self.redirect_on_activate:
#             response = super(RegistrationView, self).render_to_response(
#                                                     context, **response_kwargs)
#             response.set_cookie('redirect_on_activate',
#                                 self.redirect_on_activate,
#                                 max_age=4 * 24 * 60 * 60)
#             return response
#         return super(RegistrationView, self).render_to_response(
#                                                     context, **response_kwargs)


# class ActivationView(BaseActivationView):
#     def activate(self, request, activation_key):
#         activated_user = RegistrationProfile.objects.activate_user(activation_key)
#         return activated_user

#     def get_success_url(self, request, user):
#         if 'redirect_on_activate' in request.COOKIES:
#             return request.COOKIES.get('redirect_on_activate')
#         return ('registration_activation_complete', (), {})

#     def get(self, request, *args, **kwargs):
#         activated_user = self.activate(request, *args, **kwargs)
#         if activated_user:
#             signals.user_activated.send(sender=self.__class__,
#                                         user=activated_user,
#                                         request=request)
#             success_url = self.get_success_url(request, activated_user)
#             try:
#                 to, args, kwargs = success_url
#                 response = redirect(to, *args, **kwargs)
#             except ValueError:
#                 response = redirect(success_url)
#             if 'redirect_on_activate' in request.COOKIES:
#                 response.delete_cookie('redirect_on_activate')
#             return response
#         return super(ActivationView, self).get(request, *args, **kwargs)
